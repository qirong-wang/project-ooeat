package f2i.eatery.spring.rs.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class OneEateryDto {
	@XmlElement
	private Long id;
	@XmlElement
	private String name;
	@XmlElement
	private String cookingStyle;
	@XmlElement
	private String street;
	@XmlElement
	private String postCode;
	@XmlElement
	private String city;
	@XmlElement
	private String region;
	@XmlElement
	private String description;
	@XmlElement
	private Long reviews;
	@XmlElement
	private Double rating;
//	@XmlElement
//	private Map<String,String> tags;
	@XmlElement
	private List<String> tags;
	@XmlElement
	private String hoursOp1;
	@XmlElement
	private String hoursOp2;
	@XmlElement
	private String payments;
	@XmlElement
	private String price;
	@XmlElement
	private String gettingThere;
	@XmlElement
	private String parking;
	@XmlElement
	private List<Long> listPhoto;
	public OneEateryDto(Long id, String name, String street, String postCode, String city, String region,
			String description, Long reviews, Double rating, List<String> tags, String hoursOp1, String hoursOp2,
			String payments, String price, String gettingThere, String parking, List<Long> listPhoto) {
		super();
		this.id = id;
		this.name = name;
		this.street = street;
		this.postCode = postCode;
		this.city = city;
		this.region = region;
		this.description = description;
		this.reviews = reviews;
		this.rating = rating;
		this.tags = tags;
		this.hoursOp1 = hoursOp1;
		this.hoursOp2 = hoursOp2;
		this.payments = payments;
		this.price = price;
		this.gettingThere = gettingThere;
		this.parking = parking;
		this.listPhoto = listPhoto;
	}
	public OneEateryDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "OneEateryDto [id=" + id + ", name=" + name + ", cookingStyle=" + cookingStyle + ", street=" + street
				+ ", postCode=" + postCode + ", city=" + city + ", region=" + region + ", description=" + description
				+ ", reviews=" + reviews + ", rating=" + rating + ", tags=" + tags + ", hoursOp1=" + hoursOp1
				+ ", hoursOp2=" + hoursOp2 + ", payments=" + payments + ", price=" + price + ", gettingThere="
				+ gettingThere + ", parking=" + parking + ", listPhoto=" + listPhoto + "]";
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getReviews() {
		return reviews;
	}
	public void setReviews(Long reviews) {
		this.reviews = reviews;
	}
	public Double getRating() {
		return rating;
	}
	public void setRating(Double rating) {
		this.rating = rating;
	}
//	public Map<String, String> getTags() {
//		return tags;
//	}
//	public void setTags(Map<String, String> tags) {
//		this.tags = tags;
//	}
	
	public String getHoursOp1() {
		return hoursOp1;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	public void setHoursOp1(String hoursOp1) {
		this.hoursOp1 = hoursOp1;
	}
	public String getHoursOp2() {
		return hoursOp2;
	}
	public void setHoursOp2(String hoursOp2) {
		this.hoursOp2 = hoursOp2;
	}
	public String getPayments() {
		return payments;
	}
	public void setPayments(String payments) {
		this.payments = payments;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getGettingThere() {
		return gettingThere;
	}
	public void setGettingThere(String gettingThere) {
		this.gettingThere = gettingThere;
	}
	public String getParking() {
		return parking;
	}
	public void setParking(String parking) {
		this.parking = parking;
	}
	public List<Long> getListPhoto() {
		return listPhoto;
	}
	public void setListPhoto(List<Long> listPhoto) {
		this.listPhoto = listPhoto;
	}
	public String getCookingStyle() {
		return cookingStyle;
	}
	public void setCookingStyle(String cookingStyle) {
		this.cookingStyle = cookingStyle;
	}
	
	
}
