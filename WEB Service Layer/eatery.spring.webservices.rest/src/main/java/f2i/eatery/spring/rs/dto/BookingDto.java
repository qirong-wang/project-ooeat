package f2i.eatery.spring.rs.dto;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonFormat;



@XmlRootElement
public class BookingDto {
	
		@XmlElement
		private Long id;
		@XmlElement
//		@JsonSerialize(using = LocalDateTimeSerialize.class)
//		@JsonDeserialize(using = LocalDateTimeDeserialize.class)
//		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd E HH:mm:ss.SSS")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern=" EEE 'le' dd/MM/yyyy 'à' HH:mm")
		private LocalDateTime dateTime;
		@XmlElement
		private Long nb_people;
		@XmlElement
		private String customerId;
		@XmlElement
		private Long eateryId;
		@XmlElement
		private String eateryName;
		@XmlElement
		private Boolean modifyEnable;
		public BookingDto() {
			super();
			// TODO Auto-generated constructor stub
		}
//		public BookingDto(Long id, LocalDateTime dateTime, Long nb_people) {
//			super();
//			this.id = id;
//			this.dateTime = dateTime;
//			this.nb_people = nb_people;
//		}
		
		public BookingDto(Long id, LocalDateTime dateTime, Long nb_people, String eateryName) {
			super();
			this.id = id;
			this.dateTime = dateTime;
			this.nb_people = nb_people;
			this.eateryName = eateryName;
		}
		
		
public Boolean getModifyEnable() {
			return modifyEnable;
		}

		public void setModifyEnable(Boolean modifyEnable) {
			this.modifyEnable = modifyEnable;
		}

public BookingDto(Long id, LocalDateTime dateTime, Long nb_people, Long eateryId, String eateryName) {
	super();
	this.id = id;
	this.dateTime = dateTime;
	this.nb_people = nb_people;
	this.eateryId = eateryId;
	this.eateryName = eateryName;
}
//		public BookingDto(Long id2, LocalDateTime date_time, Long nb_people2, String name) {
//			// TODO Auto-generated constructor stub
//		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public LocalDateTime getDateTime() {
			return dateTime;
		}
		public void setDateTime(LocalDateTime dateTime) {
			this.dateTime = dateTime;
		}
		public Long getNb_people() {
			return nb_people;
		}
		public void setNb_people(Long nb_people) {
			this.nb_people = nb_people;
		}
		
		public String getCustomerId() {
			return customerId;
		}
		public void setCustomerId(String customerId) {
			this.customerId = customerId;
		}
		public Long getEateryId() {
			return eateryId;
		}
		public void setEateryId(Long eateryId) {
			this.eateryId = eateryId;
		}
		@Override
		public String toString() {
			return "BookingDto [id=" + id + ", dateTime=" + dateTime + ", nb_people=" + nb_people + ", customerId="
					+ customerId + ", eateryId=" + eateryId + ", eateryName=" + eateryName + ", modifyEnable="
					+ modifyEnable + "]";
		}

		public String getEateryName() {
			return eateryName;
		}

		public void setEateryName(String eateryName) {
			this.eateryName = eateryName;
		}
		
		
}
