package f2i.eatery.spring.rs.mapper;

import data.entities.Review;
import f2i.eatery.spring.rs.dto.ReviewDto;

public interface ReviewMapper {
public static ReviewDto ReviewToReviewDto (Review review) {
	return new ReviewDto (review.getId(), review.getRating(), review.getComment());
}
public static Review ReviewDtoToReview (ReviewDto reviewDto) {
	return new Review(reviewDto.getId(), reviewDto.getRating(), reviewDto.getComment());
}
}
