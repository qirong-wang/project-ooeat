package f2i.eatery.spring.rs.dto;

import data.entities.ImageData;

public interface ImageDataMapper {

	public static ImageDataDto  ImageDataToDto(ImageData  imageData) {
		
		return new ImageDataDto(imageData.getId(),imageData.getContent());
		
	}
	
public static ImageData ImageDataDtoToImageData( ImageDataDto   imageDataDto) {
		
		return new ImageData(imageDataDto.getId(),imageDataDto.getContent(),null,null,null);
		
	}
	
	
}
