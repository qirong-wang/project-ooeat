package f2i.eatery.spring.rs.mapper;

import data.entities.City;
import f2i.eatery.spring.rs.dto.CityDto;

public interface CityMapper {
	static CityDto CityToCityDto(City city) {
		return new CityDto(city.getId(), city.getName());
	}
	
static City CityDtoToCity(CityDto cityDto)	{
	return new City (cityDto.getId(), cityDto.getName(), null, null);
}

}
