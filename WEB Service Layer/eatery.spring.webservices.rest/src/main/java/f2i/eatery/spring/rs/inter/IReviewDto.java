package f2i.eatery.spring.rs.inter;



import f2i.eatery.spring.rs.dto.ReviewDto;

public interface IReviewDto {
	ReviewDto saveReview(Long bookingId, Long rating, String comment);
}
