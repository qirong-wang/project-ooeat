package f2i.eatery.spring.rs.inter;

import java.util.List;

import f2i.eatery.spring.rs.dto.CityDto;
import f2i.eatery.spring.rs.dto.RegionDto;

public interface IRegionCityDto {
public List<CityDto> getAllCity();

public List<CityDto> getAllCityHaveEateryOrderByName();

public List<RegionDto> getAllRegion();

public List<CityDto> getAllCityHaveEateryOrderByNameByRegionID(Long regionID);

}
