package f2i.eatery.spring.rs.dto;




import javax.xml.bind.annotation.XmlElement;



import data.entities.Address;


public class EateryDto {
	@XmlElement
	private Long id;
	@XmlElement
	private String name;
	@XmlElement
	private String description;
	@XmlElement
	private Address address;
	public EateryDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public EateryDto(Long id, String name, String description, Address address) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.address = address;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "EateryDto [id=" + id + ", name=" + name + ", description=" + description + ", address=" + address + "]";
	}
	
	
	
}
