package f2i.eatery.spring.rs.controller;



import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import f2i.eatery.spring.rs.dto.BookingDto;
import f2i.eatery.spring.rs.inter.IBookingDto;

@RestController
@Transactional
public class BookingController {
	public static final Logger log=LogManager.getLogger(BookingController.class);
@Autowired
IBookingDto serviceBookingDto;
@RequestMapping(path="/addbook" ,method= RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public ResponseEntity <BookingDto> SaveBooking (@RequestBody BookingDto booking){
//	System.out.println("-------------------"+booking.getCustomerId());
	booking =serviceBookingDto.getBooking(booking.getCustomerId(), booking.getEateryId(), booking.getNb_people(), booking.getDateTime());
	return new ResponseEntity<BookingDto>(booking, HttpStatus.OK);
	
}
@RequestMapping(path="/updatebook" ,method= RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public ResponseEntity <BookingDto> updateBooking (@RequestBody BookingDto booking){
//	System.out.println("-------------------"+booking.getCustomerId());
	booking =serviceBookingDto.update(booking.getId(), booking.getCustomerId(), booking.getDateTime(), booking.getNb_people());
	return new ResponseEntity<BookingDto>(booking, HttpStatus.OK);
	
}
@RequestMapping (path="/bookingfutur/{username}", produces= MediaType.APPLICATION_JSON_UTF8_VALUE, method= {RequestMethod.POST, RequestMethod.GET})
public ResponseEntity<List<BookingDto>> futurbooking(@PathVariable("username") String username){
	List<BookingDto> listBf= serviceBookingDto.getFuturBooking(username);
//	listBf.forEach(e-> log.info(e));
	return new ResponseEntity <List<BookingDto>>(listBf, HttpStatus.OK) ;
	
}
@RequestMapping (path="/bookingprevious/{username}", produces= MediaType.APPLICATION_JSON_UTF8_VALUE, method= {RequestMethod.POST, RequestMethod.GET})
public ResponseEntity<List<BookingDto>> previousbooking(@PathVariable("username") String username){
//	System.out.println("previousbooking...");
	List<BookingDto> listBf= serviceBookingDto.getPreviousBooking(username);
	return new ResponseEntity <List<BookingDto>>(listBf, HttpStatus.OK) ;
	
}


}