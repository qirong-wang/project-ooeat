package f2i.eatery.spring.rs.inter;

import java.util.List;

import f2i.eatery.spring.rs.dto.CookingStyleDto;

public interface IEateryDto {

	List<CookingStyleDto> getListCookingStyle(Long cityID);
}
