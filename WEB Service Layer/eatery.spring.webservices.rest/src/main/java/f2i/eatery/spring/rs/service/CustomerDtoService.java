package f2i.eatery.spring.rs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.Customer;
import data.interfaces.ICustomer;
import f2i.eatery.spring.rs.dto.CustomerDto;
import f2i.eatery.spring.rs.inter.ICustomerDto;
import f2i.eatery.spring.rs.mapper.CustomerMapper;
@Service
public class CustomerDtoService implements ICustomerDto {
	@Autowired
ICustomer service;
	
	@Override
	public CustomerDto modifyCustomerDto(Customer customer) {
		return  CustomerMapper.CustomerToCustomerDto(service.updateCustomer(customer));

	}
	
	@Override
    public CustomerDto modifyCustomer(CustomerDto customerDto) {
		Customer customer = service.findByUsername(customerDto.getUsername());
		customer.setFirstName(customerDto.getFisrtName());
		customer.setLastName(customerDto.getLastName());
		customer.setPasswordEncry(customerDto.getPassword());
		customer.setPhone(customerDto.getPhone());
		customer.setTitle(customerDto.getTitle());
		Customer updateCustomer = service.updateCustomer(customer);
		CustomerDto customerToCustomerDto = CustomerMapper.CustomerToCustomerDto(updateCustomer);
        return customerToCustomerDto;
    }
//	@Override
//	public CustomerDto modifyCustomer(CustomerDto customerDto) {
//		Customer customer = service.findByUsername(customerDto.getUsername());
//		customer.setFirstName(customerDto.getFisrtName());
//		customer.setLastName(customerDto.getLastName());
//		customer.setPassword(customerDto.getPassword());
//		customer.setPhone(customerDto.getPhone());
//		customer.setTitle(customerDto.getTitle());
//		Customer updateCustomer = service.updateCustomer(customer);
//		CustomerDto customerToCustomerDto = CustomerMapper.CustomerToCustomerDto(updateCustomer);
//		return customerToCustomerDto;
//	}
	

}
