package f2i.eatery.spring.rs.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class LoginDto {
	@XmlElement
	private String username;
	@XmlElement
	private String oldPassword;
	
	@XmlElement
	private String newPassword;
	public LoginDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return oldPassword;
	}
	public void setPassword(String password) {
		this.oldPassword = password;
	}
	public String getNewpassword() {
		return newPassword;
	}
	public void setNewpassword(String newpassword) {
		this.newPassword = newpassword;
	}
	@Override
	public String toString() {
		return "LoginDto [username=" + username + ", password=" + oldPassword + ", newpassword=" + newPassword + "]";
	}
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
}
