package f2i.eatery.spring.rs.inter;


import f2i.eatery.spring.rs.dto.ImageDataDto;

public interface IImageDataDto {

  public ImageDataDto getOneImageByID(Long imageId);
	
}
