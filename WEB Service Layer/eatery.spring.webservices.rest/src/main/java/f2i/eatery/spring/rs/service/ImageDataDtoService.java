package f2i.eatery.spring.rs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.interfaces.IImageData;
import f2i.eatery.spring.rs.dto.ImageDataDto;
import f2i.eatery.spring.rs.dto.ImageDataMapper;
import f2i.eatery.spring.rs.inter.IImageDataDto;


@Service
public class ImageDataDtoService implements IImageDataDto {

	@Autowired
	IImageData imageDataService;

	@Override
	public ImageDataDto getOneImageByID(Long imageId) {
		return ImageDataMapper.ImageDataToDto(imageDataService.getOneImageByID(imageId))   ;
	}
	
	
}
