package f2i.eatery.spring.rs.dto;

public class CookingStyleDto {
	private Long id;
	private String name;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public CookingStyleDto(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public CookingStyleDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "CookingStyleDto [id=" + id + ", name=" + name + "]";
	}
	
	
	
}
