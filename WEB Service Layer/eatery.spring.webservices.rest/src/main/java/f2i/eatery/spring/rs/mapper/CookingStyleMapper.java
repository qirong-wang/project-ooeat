package f2i.eatery.spring.rs.mapper;

import data.entities.CookingStyle;
import f2i.eatery.spring.rs.dto.CookingStyleDto;

public class CookingStyleMapper {
	public static CookingStyleDto cookingStyleToCookingStyleDto(CookingStyle cookingStyle) {
	return new CookingStyleDto(cookingStyle.getId(),cookingStyle.getName());
}
}
