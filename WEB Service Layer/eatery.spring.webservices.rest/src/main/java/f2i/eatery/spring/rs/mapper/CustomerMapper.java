package f2i.eatery.spring.rs.mapper;

import data.entities.Customer;
import f2i.eatery.spring.rs.dto.CustomerDto;

public interface CustomerMapper {

	public static CustomerDto CustomerToCustomerDto(Customer customer) {
		return new CustomerDto(customer.getUsername(), customer.getPasswordEncry(), customer.getFirstName(),
				customer.getLastName(), customer.getTitle(), customer.getPhone());
	}

	public static Customer CustomerDtoToCustomer(CustomerDto customerDto) {
		System.out.println("++++"+customerDto);
//		new Customer(username, password, fisrtName, lastName, title2, phone2)
		return new Customer(customerDto.getUsername(), customerDto.getPassword(), customerDto.getFisrtName(),
				customerDto.getLastName(), customerDto.getTitle(), customerDto.getPhone());
//		return new Customer(customerDto.getUsername(), customerDto.getPassword(), customerDto.getFisrtName(),
//				customerDto.getLastName(), customerDto.getTitle(), customerDto.getPhone());

	}
}
