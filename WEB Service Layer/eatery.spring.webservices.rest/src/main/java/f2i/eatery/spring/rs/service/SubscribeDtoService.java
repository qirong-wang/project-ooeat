package f2i.eatery.spring.rs.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.Customer;
import data.exceptions.UserNameAlreadyExistsException;
import data.interfaces.ICustomer;
import f2i.eatery.spring.rs.dto.CustomerDto;
import f2i.eatery.spring.rs.inter.ISubscribeDto;
import f2i.eatery.spring.rs.mapper.CustomerMapper;

@Service
public class SubscribeDtoService implements ISubscribeDto {

	@Autowired
	private ICustomer service;

	private static final Logger log = LogManager.getLogger(SubscribeDtoService.class);

	@Override
	public Boolean subscribe(CustomerDto custumerDto) throws UserNameAlreadyExistsException {
		Customer customer = CustomerMapper.CustomerDtoToCustomer(custumerDto);
		Boolean subscribe = service.subscribe(customer);

		return subscribe;
	}

	@Override
	public CustomerDto findByUsername(String username) {

		return CustomerMapper.CustomerToCustomerDto(service.findByUsername(username));
	}
	@Override
    public CustomerDto subscribeA(String username, String password, String firstname, String lastname, String title, String phone) throws UserNameAlreadyExistsException {
       log.info("subscribe  call..");
		CustomerDto customer = CustomerMapper.CustomerToCustomerDto(service.subscribeA(username, password, firstname, lastname, title, phone));

        return customer;
    }
}
