package f2i.eatery.spring.rs.inter;

import java.time.LocalDateTime;
import java.util.List;

import data.entities.Booking;
import f2i.eatery.spring.rs.dto.BookingDto;
import f2i.eatery.spring.rs.dto.BookingDtoModify;
import f2i.eatery.spring.rs.dto.BookingPreviousDto;

public interface IBookingDto {
	BookingDto getBooking(String customerId, Long eateryId, Long np, LocalDateTime time);

	void deleteBooking(Long id);

	List<BookingDto> getFuturBooking(String username);

	List<BookingDto> getPreviousBooking(String username);

	BookingDto update(Long id, String username, LocalDateTime date, Long np);

	BookingDto saveBooking(Booking booking);

	BookingDto updateBooking(BookingDtoModify booking);

	List<BookingPreviousDto> getPreviousBookingList(String username);
}
