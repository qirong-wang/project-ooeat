package f2i.eatery.spring.rs.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import data.interfaces.IRegion;
import f2i.eatery.spring.rs.dto.CityDto;
import f2i.eatery.spring.rs.dto.RegionDto;
import f2i.eatery.spring.rs.inter.IRegionCityDto;

@RestController
@Transactional
public class CityController {
	public static final Logger log=LogManager.getLogger(CityController.class);
@Autowired
IRegionCityDto service;

@Autowired 
IRegion serviceRegion;
	@RequestMapping(value="/city", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE} )
	public ResponseEntity <List <CityDto>>  callCity() {
		log.info("list city started");
		List <CityDto> getAllCities =new ArrayList<>();
		try {
		getAllCities= service.getAllCity();
		}
		catch (Exception e) {
			log.info("error ............" + e.getLocalizedMessage());
		}
		return new ResponseEntity<List <CityDto>> (getAllCities, HttpStatus.OK );
		
	}
	@RequestMapping(value="/cityEatery", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE} )
	public ResponseEntity <List <CityDto>> cityEatery() {
		log.info("list city started");
		List <CityDto> getAllCities =new ArrayList<>();
		try {
			getAllCities= service.getAllCityHaveEateryOrderByName();
		}
		catch (Exception e) {
			log.info("error ...getAllCityHaveEateryOrderByName........." + e.getLocalizedMessage());
		}
		return new ResponseEntity<List <CityDto>> (getAllCities, HttpStatus.OK );
		
	}
	@RequestMapping(value="/cityEateryRegion", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE} )
	public ResponseEntity <List <CityDto>> cityEateryRegion(@RequestBody Map<String,String> map) {
		log.info("list cityEateryRegion started");
		List <CityDto> getAllCitiesByRegion =new ArrayList<>();
		try {
			Long regionID = Long.valueOf(map.get("regionID"));
			if(regionID>0)getAllCitiesByRegion= service.getAllCityHaveEateryOrderByNameByRegionID(Long.valueOf(map.get("regionID")));
			else getAllCitiesByRegion=service.getAllCityHaveEateryOrderByName();
		}
		catch (Exception e) {
			log.info("error ...getAllCityHaveEateryOrderByNameByRegionID........." + e.getLocalizedMessage());
		}
		return new ResponseEntity<List <CityDto>> (getAllCitiesByRegion, HttpStatus.OK );
		
	}
	@RequestMapping(value="/allRegion", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE} )
	public ResponseEntity <	List <RegionDto>> allRegion() {
		log.info("list allRegion started");
		List <RegionDto> getAllRegions =new ArrayList<>();
		try {
			getAllRegions= service.getAllRegion();
		}
		catch (Exception e) {
			log.info("error ...getAll allRegion........." + e.getLocalizedMessage());
		}
		return new ResponseEntity<List <RegionDto>> (getAllRegions, HttpStatus.OK );
		
	}
	
}
