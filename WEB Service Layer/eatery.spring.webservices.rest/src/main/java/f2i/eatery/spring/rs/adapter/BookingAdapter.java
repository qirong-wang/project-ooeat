package f2i.eatery.spring.rs.adapter;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.Booking;
import data.interfaces.IBooking;
import data.interfaces.ICustomer;
import data.interfaces.IEatery;
import f2i.eatery.spring.rs.dto.BookingDto;
import f2i.eatery.spring.rs.dto.BookingDtoModify;
import f2i.eatery.spring.rs.dto.BookingPreviousDto;
import f2i.eatery.spring.rs.mapper.BookingMapper;

@Service
@Transactional
public class BookingAdapter {
	@Autowired
	IEatery serviceEatery;
	@Autowired
	ICustomer serviceCustomer;
	@Autowired
	IBooking serviceBooking;
	

	public BookingDto saveNewBooking(BookingDtoModify bookingDtoModify) {
		Booking book=new Booking();
		book.setCustomer(serviceCustomer.findByUsername(bookingDtoModify.getCustomerId()));
		book.setDate_time(bookingDtoModify.getDateTime());
		book.setEatery(serviceEatery.getOneEateryByEateryID(bookingDtoModify.getEateryId()));
		book.setNb_people(bookingDtoModify.getNb_people());
		Booking saveBooking = serviceBooking.saveBooking(book);
		BookingDto bookingToBookingDto = BookingMapper.BookingToBookingDto(saveBooking);
		return bookingToBookingDto;
	}
	public BookingPreviousDto getPreviousBooking(Booking booking) {
		BookingPreviousDto bookingPreviousDto = new BookingPreviousDto();
		bookingPreviousDto.setId(booking.getId());
		bookingPreviousDto.setNb_people(booking.getNb_people());
		bookingPreviousDto.setDateTime(booking.getDate_time());
		bookingPreviousDto.setEateryId(booking.getEatery().getId());
		bookingPreviousDto.setEateryName(booking.getEatery().getName());
		bookingPreviousDto.setCustomerId(booking.getCustomer().getUsername());
		if(booking.getReview()!=null) {
			bookingPreviousDto.setReviewId(booking.getReview().getId());
			bookingPreviousDto.setComment(booking.getReview().getComment());
			bookingPreviousDto.setRating(booking.getReview().getRating());
		}
		return bookingPreviousDto;
	}

}
