package f2i.eatery.spring.rs.inter;


import data.entities.Customer;
import f2i.eatery.spring.rs.dto.CustomerDto;

public interface ICustomerDto {

	CustomerDto modifyCustomerDto(Customer customer);

	CustomerDto modifyCustomer(CustomerDto customerDto);
}
