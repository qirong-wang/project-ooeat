package f2i.eatery.spring.rs.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import data.exceptions.UserNameAlreadyExistsException;
import f2i.eatery.spring.rs.dto.CustomerDto;
import f2i.eatery.spring.rs.inter.ISubscribeDto;

@RestController
@Transactional
public class SuscribleController {
	public static final Logger log = LogManager.getLogger(SuscribleController.class);
	@Autowired
	ISubscribeDto service;


	
	@ResponseBody
    @RequestMapping(value = "/subscribe", method = RequestMethod.PUT, produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> subscribeA(@RequestBody CustomerDto custumerDto) throws UserNameAlreadyExistsException {
        try {
            service.subscribeA(custumerDto.getUsername(), custumerDto.getPassword(), custumerDto.getFisrtName(),
                    custumerDto.getLastName(), custumerDto.getTitle(), custumerDto.getPhone());
            log.info("le custumerDao est bien enregister");
            return new ResponseEntity<CustomerDto>(custumerDto, HttpStatus.OK);
        } catch (UserNameAlreadyExistsException e) {
        	System.out.println(e.getLocalizedMessage());
            log.info(e.getLocalizedMessage());
            return new ResponseEntity<String>(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }
}