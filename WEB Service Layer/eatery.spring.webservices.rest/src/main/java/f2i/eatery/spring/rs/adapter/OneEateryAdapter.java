package f2i.eatery.spring.rs.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.Eatery;
import data.interfaces.IEatery;
import data.interfaces.IImageData;
import f2i.eatery.spring.rs.dto.OneEateryDto;
@Service
@Transactional
public class OneEateryAdapter {
	@Autowired
	IEatery serviceEatery;
	@Autowired
	IImageData serviceImage;
	
public OneEateryDto getOneEateryDto(Map<String,String> map)	{
	Eatery oneEatery = serviceEatery.getOneEateryByEateryID(Long.valueOf(map.get("oneEateryID")));
	OneEateryDto eateryDto=new OneEateryDto();
	eateryDto.setId(oneEatery.getId());
	eateryDto.setDescription(oneEatery.getDescription());
	eateryDto.setName(oneEatery.getName());
	eateryDto.setCookingStyle(oneEatery.getCookingStyle().getName());
	eateryDto.setStreet(oneEatery.getAddress().getStreet());
	eateryDto.setPostCode(oneEatery.getAddress().getPostCode());
	eateryDto.setCity(oneEatery.getCity().getName());
	eateryDto.setRegion(oneEatery.getCity().getRegion().getName());
	eateryDto.setDescription(oneEatery.getDescription());
	eateryDto.setReviews(Long.valueOf(map.get("nbReview")));
	eateryDto.setRating(Double.valueOf(map.get("rating")));
	List<String> listTags=new ArrayList<String>();
	oneEatery.getEateryTags().forEach(t->listTags.add(t.getName()));
	eateryDto.setTags(listTags);
	eateryDto.setListPhoto(serviceImage.getListPhotoIDByEateryID(oneEatery.getId()));
	eateryDto.setHoursOp1(oneEatery.getPracticalInfo().getHoursOp1());
	eateryDto.setHoursOp2(oneEatery.getPracticalInfo().getHoursOp2());
	eateryDto.setPayments(oneEatery.getPracticalInfo().getPaymentOps());
	eateryDto.setPrice(oneEatery.getPracticalInfo().getPrice());
	eateryDto.setGettingThere(oneEatery.getPracticalInfo().getGettingThere());
	return eateryDto;
}

}
