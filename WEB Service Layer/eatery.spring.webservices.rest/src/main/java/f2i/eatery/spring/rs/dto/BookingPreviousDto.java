package f2i.eatery.spring.rs.dto;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonFormat;

@XmlRootElement
public class BookingPreviousDto {
	
		@XmlElement
		private Long id;
		@XmlElement
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern=" EEE 'le' dd/MM/yyyy 'à' HH:mm")
		private LocalDateTime dateTime;
		@XmlElement
		private Long nb_people;
		@XmlElement
		private String customerId;
		@XmlElement
		private Long eateryId;
		@XmlElement
		private String eateryName;
		@XmlElement
		private Long reviewId;
		@XmlElement
		private Long rating;
		@XmlElement
		private String comment;
		public BookingPreviousDto() {
			super();
		}
		
		public BookingPreviousDto(Long id, LocalDateTime dateTime, Long nb_people, String eateryName) {
			super();
			this.id = id;
			this.dateTime = dateTime;
			this.nb_people = nb_people;
			this.eateryName = eateryName;
		}
		

public BookingPreviousDto(Long id, LocalDateTime dateTime, Long nb_people, Long eateryId, String eateryName) {
	super();
	this.id = id;
	this.dateTime = dateTime;
	this.nb_people = nb_people;
	this.eateryId = eateryId;
	this.eateryName = eateryName;
}


		public Long getReviewId() {
	return reviewId;
}

public void setReviewId(Long reviewId) {
	this.reviewId = reviewId;
}

public Long getRating() {
	return rating;
}

public void setRating(Long rating) {
	this.rating = rating;
}


		public String getComment() {
	return comment;
}

public void setComment(String comment) {
	this.comment = comment;
}

		public BookingPreviousDto(Long id, LocalDateTime dateTime, Long nb_people, String customerId, Long eateryId,
		String eateryName, Long reviewId, Long rating, String comment) {
	super();
	this.id = id;
	this.dateTime = dateTime;
	this.nb_people = nb_people;
	this.customerId = customerId;
	this.eateryId = eateryId;
	this.eateryName = eateryName;
	this.reviewId = reviewId;
	this.rating = rating;
	this.comment = comment;
}

		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public LocalDateTime getDateTime() {
			return dateTime;
		}
		public void setDateTime(LocalDateTime dateTime) {
			this.dateTime = dateTime;
		}
		public Long getNb_people() {
			return nb_people;
		}
		public void setNb_people(Long nb_people) {
			this.nb_people = nb_people;
		}
		
		public String getCustomerId() {
			return customerId;
		}
		public void setCustomerId(String customerId) {
			this.customerId = customerId;
		}
		public Long getEateryId() {
			return eateryId;
		}
		public void setEateryId(Long eateryId) {
			this.eateryId = eateryId;
		}

		public String getEateryName() {
			return eateryName;
		}

		public void setEateryName(String eateryName) {
			this.eateryName = eateryName;
		}

		@Override
		public String toString() {
			return "BookingPreviousDto [id=" + id + ", dateTime=" + dateTime + ", nb_people=" + nb_people
					+ ", customerId=" + customerId + ", eateryId=" + eateryId + ", eateryName=" + eateryName
					+ ", reviewId=" + reviewId + ", rating=" + rating + ", comment=" + comment + "]";
		}
		
		
}
