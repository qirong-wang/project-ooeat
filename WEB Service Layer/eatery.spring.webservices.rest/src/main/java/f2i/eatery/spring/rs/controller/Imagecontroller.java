package f2i.eatery.spring.rs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import data.entities.ImageData;
import data.interfaces.IImageData;


@RestController
@CrossOrigin
public class Imagecontroller {
	@Autowired
	IImageData service;

	@RequestMapping(path = "/image/{id}")
	public ResponseEntity<byte[]> getImage(@PathVariable("id") Long id)
	{
		Long oneSmallImageDataIDByEateryID = service.getOneSmallImageDataIDByEateryID(id);
		if(oneSmallImageDataIDByEateryID==null||oneSmallImageDataIDByEateryID==0l) {
			oneSmallImageDataIDByEateryID=2l;
//			return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
		}
		ImageData image = service.getOneImageByID(oneSmallImageDataIDByEateryID);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
		headers.setContentLength(image.getContent().length);

		return new ResponseEntity<byte[]>(image.getContent(), headers, HttpStatus.OK);

	}
	@RequestMapping(path = "/eateryPhoto/{id}")
	public ResponseEntity<byte[]> getImageByEateryID(@PathVariable("id") Long id)
	
	{	
		ImageData image = service.getOneImageByID(id);
		if(image==null) {
			return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
		}
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
		headers.setContentLength(image.getContent().length);
			return new ResponseEntity<byte[]>(image.getContent(), headers, HttpStatus.OK);
			
	}
	
	
	
	
}


	
	
	
	


