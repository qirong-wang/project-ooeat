package f2i.eatery.spring.rs.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.Booking;
import data.interfaces.IBooking;
import f2i.eatery.spring.rs.adapter.BookingAdapter;
import f2i.eatery.spring.rs.dto.BookingDto;
import f2i.eatery.spring.rs.dto.BookingDtoModify;
import f2i.eatery.spring.rs.dto.BookingPreviousDto;
import f2i.eatery.spring.rs.inter.IBookingDto;
import f2i.eatery.spring.rs.mapper.BookingMapper;
import f2i.eatery.spring.rs.tools.ListMapper;
@Service
public class BookingDtoService implements IBookingDto {
@Autowired
IBooking serviceBooking;
@Autowired
BookingAdapter serviceBookingAdapter;
	@Override
	public BookingDto getBooking(String customerId, Long eateryId, Long np, LocalDateTime time) {
		return BookingMapper.BookingToBookingDto(serviceBooking.getBooking(customerId, eateryId, np, time));
	}

	@Override
	public void deleteBooking(Long id) {
		serviceBooking.deleteBooking(id);
	}

	@Override
	public List<BookingDto> getFuturBooking(String username) {
//		List<Booking> futurBooking = serviceBooking.getFuturBooking(username);
//		String name = futurBooking.get(0).getEatery().getName();
		List<BookingDto> bookingListToBDtoList = ListMapper.BookingListToBDtoList(serviceBooking.getFuturBooking(username));
		if(bookingListToBDtoList!=null&&bookingListToBDtoList.size()>0) {
			LocalDateTime modifyEnableTime = LocalDateTime.now().plusDays(1l);
			bookingListToBDtoList.forEach(b->{
				if(modifyEnableTime.isBefore(b.getDateTime())) 
					b.setModifyEnable(true);else b.setModifyEnable(false);
			});
		}
		return bookingListToBDtoList;
	}

	@Override
	public List<BookingDto> getPreviousBooking(String username) {
		return ListMapper.BookingListToBDtoList(serviceBooking.getPreviousBooking(username));
		}
	@Override
	public List<BookingPreviousDto> getPreviousBookingList(String username) {
		List<BookingPreviousDto> listPreviousBookings= new ArrayList<BookingPreviousDto>();
		List<Booking> previousBooking = serviceBooking.getPreviousBooking(username);
		previousBooking.forEach(e->listPreviousBookings.add(serviceBookingAdapter.getPreviousBooking(e)));
		return listPreviousBookings;
		}

	@Override
	public BookingDto update(Long id, String username, LocalDateTime date, Long np) {
		return BookingMapper.BookingToBookingDto(serviceBooking.update(id, username, date, np));
	}
	@Override
	public BookingDto saveBooking(Booking booking) {
		return BookingMapper.BookingToBookingDto(serviceBooking.saveBooking(booking));
	}
	@Override
	public BookingDto updateBooking(BookingDtoModify booking) {
		Booking bookingById = serviceBooking.getBookingById(booking.getId());
	
		bookingById.setDate_time(booking.getDateTime());
		bookingById.setNb_people(booking.getNb_people());
		Booking saveBooking = serviceBooking.saveBooking(bookingById);
		return BookingMapper.BookingToBookingDto(saveBooking);
	}

}
