package f2i.eatery.spring.rs.controller;



import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import data.entities.Customer;
import data.interfaces.IInjection;
import f2i.eatery.spring.rs.adapter.BookingAdapter;
import f2i.eatery.spring.rs.dto.BookingDto;
import f2i.eatery.spring.rs.dto.BookingDtoModify;
import f2i.eatery.spring.rs.dto.BookingPreviousDto;
import f2i.eatery.spring.rs.inter.IBookingDto;

@RestController
@Transactional
public class BookingControllers {
	public static final Logger log=LogManager.getLogger(BookingControllers.class);
@Autowired
IBookingDto serviceBookingDto;
@Autowired
IInjection injectionService;
@Autowired
BookingAdapter serviceBookingAdapter ;
@RequestMapping (path="/previousBooking", produces= MediaType.APPLICATION_JSON_UTF8_VALUE, method= {RequestMethod.POST, RequestMethod.GET})
public ResponseEntity<List<BookingDto>> previousbooking(@RequestBody Map<String, String> user){
	log.info("previousbooking..."+user.get("username"));
	String username = user.get("username");
	List<BookingDto> listBf= serviceBookingDto.getPreviousBooking(username);
	return new ResponseEntity <List<BookingDto>>(listBf, HttpStatus.OK) ;
}
@RequestMapping (path="/previousBookingList", produces= MediaType.APPLICATION_JSON_UTF8_VALUE, method= {RequestMethod.POST, RequestMethod.GET})
public ResponseEntity<List<BookingPreviousDto>> previousBookingList(@RequestBody Map<String, String> user){
	String username = user.get("username");
	List<BookingPreviousDto> previousBookingList = serviceBookingDto.getPreviousBookingList(username);
	log.info("previousBookingList-------------------------------------------------");
	return new ResponseEntity <List<BookingPreviousDto>>(previousBookingList, HttpStatus.OK) ;
}
@RequestMapping (path="/futurBooking", produces= MediaType.APPLICATION_JSON_UTF8_VALUE, method= {RequestMethod.POST, RequestMethod.GET})
public ResponseEntity<List<BookingDto>> futurBooking(@RequestBody Map<String, String> user){
	String username = user.get("username");
	List<BookingDto> listBf= serviceBookingDto.getFuturBooking(username);
//	listBf.forEach(e->System.out.println(e));
	return new ResponseEntity <List<BookingDto>>(listBf, HttpStatus.OK) ;
	
}
@RequestMapping(path="/saveBooking" ,method= RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public ResponseEntity <BookingDto> saveBooking (@RequestBody BookingDtoModify booking){
	return new ResponseEntity<BookingDto>(serviceBookingAdapter.saveNewBooking(booking), HttpStatus.OK);
}
@RequestMapping(path="/updateBooking" ,method= RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public ResponseEntity <BookingDto> updateBooking (@RequestBody BookingDtoModify booking){
	log.info(booking);
	log.info(booking.getDateTime());
	return new ResponseEntity<BookingDto>(serviceBookingDto.updateBooking(booking), HttpStatus.OK);
}

@RequestMapping (path="/bookingDelete", method={RequestMethod.POST }, produces =MediaType.APPLICATION_JSON_UTF8_VALUE)
public ResponseEntity <?> deleteOneBooking(@RequestBody Map<String, String> booking){
	log.info("deleteOneBooking call....");
	try {
		serviceBookingDto.deleteBooking(Long.valueOf(booking.get("bookingID")));
		String  msg="votre réservation a bien été supprimé";
		log.info(msg);
		return new ResponseEntity<String>(msg, HttpStatus.OK);
	}catch (Exception e){
		String msg="Error";
		return new ResponseEntity<String>(msg, HttpStatus.UNAUTHORIZED);
	}
}
@RequestMapping (path="/injection", method={RequestMethod.POST }, produces =MediaType.APPLICATION_JSON_UTF8_VALUE)
public ResponseEntity <?> injection(){
	log.info("injection call....");
	try {
		ClassLoader classLoader = getClass().getClassLoader();
		
		URL xml = classLoader.getResource("Customer.xml");
		URL xsd = classLoader.getResource("Customer.xsd");
		if (xml == null) {
			throw new IllegalArgumentException("file is not found!");
		} else {
			log.info("OK");
		}
		List<Customer> injectionCustomer = injectionService.injectionCustomer2(new File(xml.getFile()), new File(xsd.getFile()));
		injectionService.injectionBMC4j();
		log.info(injectionCustomer.size());
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}catch (Exception e){
		return new ResponseEntity<String>(e.getLocalizedMessage(), HttpStatus.UNAUTHORIZED);
	}
}
@RequestMapping (path="/injection2", method={RequestMethod.POST }, produces =MediaType.APPLICATION_JSON_UTF8_VALUE)
public ResponseEntity <?> injection2(){
	log.info("injection call....");
	try {
		ClassLoader classLoader = getClass().getClassLoader();
		
		URL xml = classLoader.getResource("Customer.xml");
		URL xsd = classLoader.getResource("Customer.xsd");
		if (xml == null) {
			throw new IllegalArgumentException("file is not found!");
		} else {
			log.info("OK");
		}
		List<Customer> injectionCustomer = injectionService.injectionCustomer2(new File(xml.getFile()), new File(xsd.getFile()));
		injectionService.injectionBMC4j();
		log.info(injectionCustomer.size());
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}catch (Exception e){
		return new ResponseEntity<String>(e.getLocalizedMessage(), HttpStatus.UNAUTHORIZED);
	}
}
@RequestMapping (path="/injectionCus", method={RequestMethod.POST }, produces =MediaType.APPLICATION_JSON_UTF8_VALUE)
public ResponseEntity <?> injectionCus(){
	log.info("injectionCus call....");
	try {
		ClassLoader classLoader = getClass().getClassLoader();
		URL xml = classLoader.getResource("Customer.xml");
		URL xsd = classLoader.getResource("Customer.xsd");
		if (xml == null) {
			throw new IllegalArgumentException("file is not found!");
		} else {
			log.info("OK");
		}
		List<Customer> injectionCustomer = injectionService.injectionCustomer2(new File(xml.getFile()), new File(xsd.getFile()));
		injectionService.injectionCust();
		log.info(injectionCustomer.size());
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}catch (Exception e){
		return new ResponseEntity<String>(e.getLocalizedMessage(), HttpStatus.UNAUTHORIZED);
	}
}

//@RequestMapping (path="/bookingdelete/{id}", method={RequestMethod.POST, RequestMethod.GET}, produces =MediaType.APPLICATION_JSON_UTF8_VALUE)
//public ResponseEntity <?> deleteBooking(@PathVariable ("id") Long id){
//	try {
//		serviceBookingDto.deleteBooking(id);
//		String  msg="votre réservation a bien été supprimé";
//		return new ResponseEntity<String>(msg, HttpStatus.OK);
//	}catch (Exception e){
//		String msg="Error";
//		return new ResponseEntity<String>(msg, HttpStatus.UNAUTHORIZED);
//		
//	}
//}
}