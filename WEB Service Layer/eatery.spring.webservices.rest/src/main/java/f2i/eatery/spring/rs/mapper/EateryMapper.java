package f2i.eatery.spring.rs.mapper;

import data.entities.Eatery;
import f2i.eatery.spring.rs.dto.EateryDto;

public class EateryMapper {
static EateryDto EateryToEateryDto(Eatery eatery) {
	return new EateryDto(eatery.getId(), eatery.getName(), eatery.getDescription(), eatery.getAddress());
}
static Eatery EateryDtoToEatery (EateryDto eateryDto) {
	return new Eatery(eateryDto.getId(), eateryDto.getName(), eateryDto.getDescription(), eateryDto.getAddress(), null, null, null);
}
}
