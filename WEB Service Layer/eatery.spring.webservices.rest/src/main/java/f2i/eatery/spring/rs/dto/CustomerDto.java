package f2i.eatery.spring.rs.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CustomerDto {
	@XmlElement
	private String username;
	@XmlElement
	private String password;
	@XmlElement
	private String fisrtName;
	@XmlElement
	private String lastName;
	@XmlElement
	private String title;
	@XmlElement
	private String phone;
	public CustomerDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public CustomerDto(String username, String password, String fisrtName, String lastName, String title,
			String phone) {
		super();
		this.username = username;
		this.password = password;
		this.fisrtName = fisrtName;
		this.lastName = lastName;
		this.title = title;
		this.phone = phone;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFisrtName() {
		return fisrtName;
	}
	public void setFisrtName(String fisrtName) {
		this.fisrtName = fisrtName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "CustomerDto [username=" + username + ", password=" + password + ", fisrtName=" + fisrtName
				+ ", lastName=" + lastName + ", title=" + title + ", phone=" + phone + "]";
	}

	
	
	
}
