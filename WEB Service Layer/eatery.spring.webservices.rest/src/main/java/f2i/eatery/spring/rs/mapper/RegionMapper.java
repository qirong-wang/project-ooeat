package f2i.eatery.spring.rs.mapper;

import data.entities.Region;
import f2i.eatery.spring.rs.dto.RegionDto;

public class RegionMapper {
public static RegionDto RegionToRegionDto(Region region) {
	return new RegionDto(region.getId(), region.getName());
	
}
public static Region RegionDtoToRegion (RegionDto regionDto) {
	return new Region();
}
}
