package f2i.eatery.spring.rs.dto;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ImageDataDto {

	private Long id;
	
	private byte [] content;

	public ImageDataDto(Long id, byte[] content) {
		super();
		this.id = id;
		this.content = content;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "ImageDataDto [id=" + id + ", content=" + Arrays.toString(content) + "]";
	}

	
}
