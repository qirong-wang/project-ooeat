package f2i.eatery.spring.rs.controller;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.Yaml;

import data.entities.Menu;
import data.entities.Review;
import data.interfaces.IEatery;
import data.interfaces.IImageData;
import data.interfaces.IMenu;
import data.interfaces.IReview;
import data.po.EateryInfo;
import data.po.EateryRequest;
import data.po.ReviewInfo;
import f2i.eatery.spring.rs.adapter.OneEateryAdapter;
import f2i.eatery.spring.rs.dto.CookingStyleDto;
import f2i.eatery.spring.rs.dto.OneEateryDto;
import f2i.eatery.spring.rs.dto.ReviewDto;
import f2i.eatery.spring.rs.inter.IEateryDto;
import f2i.eatery.spring.rs.mapper.ReviewMapper;

@RestController
@Transactional
public class EateryController {
	public static final Logger log=LogManager.getLogger(EateryController.class);
@Autowired 
OneEateryAdapter serviceEateryAdapter;
@Autowired
IEatery serviceEatery;
@Autowired
IMenu serviceMenu;
@Autowired
IImageData serviceImage;
@Autowired
IReview serviceReview;
@Autowired
IEateryDto serviceEateryDto;
private EateryRequest eateryRequest;
@RequestMapping(value="/eatery", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE},method = { RequestMethod.POST},consumes={MediaType.APPLICATION_JSON_VALUE})
public ResponseEntity <List<EateryInfo>> callEatery(@RequestBody EateryRequest eateryRequest) {
	log.info("callEatery started...");
	this.eateryRequest=eateryRequest;
	log.info(eateryRequest);
//	public List<EateryInfo> findListEateryByMultiConditions(Long cityID, List<Long> cookingStylesID, List<Long> tagsID, String minPrice, String maxPrice, String minRating, Integer firstPage, Integer maxSize, String restrictionOrder); 
	List<EateryInfo> findListEateryByMultiConditions = serviceEatery.findListEateryByMultiConditions(eateryRequest);
	return new ResponseEntity<List<EateryInfo>> (findListEateryByMultiConditions, HttpStatus.OK );
}
	@RequestMapping(value="/nbEatery", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE} )
	public ResponseEntity <Integer> callNbEatery() {
		log.info("nbEatery   callNbEatery started.........");
		Integer findNbEateryByMultiConditions = serviceEatery.findNbEateryByMultiConditions(eateryRequest);
		return new ResponseEntity<Integer> (findNbEateryByMultiConditions, HttpStatus.OK );
	}
	
	@RequestMapping(value="/listCookingStyle", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE} )
	public ResponseEntity <List<String[]>> callListCookingStyle() {
		log.info("listCookingStylelistCookingStyle   callListCookingStyle started.......");
		List<String[]> nbCookingStyleByMultiConditions = serviceEatery.getNbCookingStyleByMultiConditions();
		return new ResponseEntity<List<String[]>> (nbCookingStyleByMultiConditions, HttpStatus.OK );
	}
	
	@RequestMapping(value="/getOneEatery", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE} )
	public ResponseEntity <OneEateryDto> getOneEatery(@RequestBody Map<String,String> map) {
		log.info("getOneEatery started...");
		OneEateryDto eateryDto = serviceEateryAdapter.getOneEateryDto(map);
		return new ResponseEntity<OneEateryDto> (eateryDto, HttpStatus.OK );
	}
	@RequestMapping(value="/getOneEateryMenu", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE} )
	public ResponseEntity <	 Map<String, Object>> getOneEateryMenu(@RequestBody Map<String,String> map) {
		log.info("getOneEateryMenu started...");
		Menu oneMenuByID = serviceMenu.getOneMenuByID(Long.valueOf(map.get("oneEateryID")));
		String yamlString = oneMenuByID.getContent();
		 if(yamlString.startsWith("---\n")){
	         yamlString = yamlString.replaceAll("---\n","");
	      }
		 Yaml yaml= new Yaml();
		 @SuppressWarnings("unchecked")
		Map<String, Object> mapping = (Map<String, Object>) yaml.load(yamlString);
//		System.out.println(oneMenuByID.getContent());
		return new ResponseEntity<Map<String, Object>> (mapping, HttpStatus.OK );
	}
	@RequestMapping(value="/getOneEateryComment", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE} )
	public ResponseEntity <List<ReviewInfo>> getOneEateryComment(@RequestBody Map<String,String> map) {
		log.info("getOneEateryNote started...");
		log.info(map.get("eateryID")+"--"+map.get("firstPageComment")+"--"+map.get("withComment"));
		List<ReviewInfo> listReviewByEateryID = serviceReview.getListReviewByEateryID(Long.valueOf(map.get("eateryID")), Integer.valueOf(map.get("firstPageComment")), Boolean.valueOf(map.get("withComment")));
		return new ResponseEntity<List<ReviewInfo>> (listReviewByEateryID, HttpStatus.OK );
	}
	@RequestMapping(value="/getNbReviewWithComment", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE} )
	public ResponseEntity <Integer> getNbReviewWithComment(@RequestBody Map<String,String> map) {
		log.info("getNbReviewWithComment started...");
		Integer nbReviewsWithCommentByEateryID = serviceReview.getNbReviewsWithCommentByEateryID(Long.valueOf(map.get("eateryID")));
		return new ResponseEntity<Integer> (nbReviewsWithCommentByEateryID, HttpStatus.OK );
	}
	@RequestMapping(value="/getListCookingStyleIndex", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE} )
	public ResponseEntity <List<CookingStyleDto>> getListCookingStyleIndex(@RequestBody Map<String,String> map) {
		log.info("getListCookingStyleIndex started...");
		Long cityID =map.get("cityID")==null?0l:Long.valueOf(map.get("cityID"));
		List<CookingStyleDto> listCookingStyle = serviceEateryDto.getListCookingStyle(cityID);
		return new ResponseEntity<List<CookingStyleDto>> (listCookingStyle, HttpStatus.OK );
	}
	@RequestMapping(value="/setReview", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE} )
	public ResponseEntity <ReviewDto> setReview(@RequestBody Map<String,String> map) {
		log.info("setReview started...");
		Review saveReview = serviceReview.saveReview(Long.valueOf(map.get("bookingId")), Long.valueOf(map.get("rating")), map.get("comment"));
		return new ResponseEntity<ReviewDto> (ReviewMapper.ReviewToReviewDto(saveReview), HttpStatus.OK );
	}
	
}
