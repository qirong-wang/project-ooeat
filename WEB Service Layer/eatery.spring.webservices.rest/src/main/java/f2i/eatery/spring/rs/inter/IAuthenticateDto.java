package f2i.eatery.spring.rs.inter;

import data.exceptions.NoUserException;
import data.exceptions.PasswordWrongException;
import f2i.eatery.spring.rs.dto.CustomerDto;

public interface IAuthenticateDto {
public CustomerDto Authenticate(String username, String password) throws PasswordWrongException, NoUserException;
public CustomerDto findByUsername(String username);
public void updatePassword(String username, String password, String newPassword) throws PasswordWrongException;
}