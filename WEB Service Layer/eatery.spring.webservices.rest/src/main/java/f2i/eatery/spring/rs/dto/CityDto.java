package f2i.eatery.spring.rs.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class CityDto {
	@XmlElement
	private Long id;
	@XmlElement
	private String name;
	
	
	
	public CityDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public CityDto(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	


	@Override
	public String toString() {
		return "CityDto [id=" + id + ", name=" + name + "]";
	}


}
