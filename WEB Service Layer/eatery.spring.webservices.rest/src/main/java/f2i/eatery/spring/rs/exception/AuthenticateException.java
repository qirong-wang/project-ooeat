package f2i.eatery.spring.rs.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Not existant username or password")
public class AuthenticateException extends RuntimeException
{
	
	private static final long serialVersionUID = 1L;
	public static final Logger log=LogManager.getLogger(AuthenticateException.class);
	

}
