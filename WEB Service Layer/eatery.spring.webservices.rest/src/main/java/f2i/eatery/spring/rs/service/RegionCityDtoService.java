package f2i.eatery.spring.rs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.interfaces.ICity;
import data.interfaces.IRegion;
import f2i.eatery.spring.rs.dto.CityDto;
import f2i.eatery.spring.rs.dto.RegionDto;
import f2i.eatery.spring.rs.inter.IRegionCityDto;
import f2i.eatery.spring.rs.tools.ListMapper;

@Service
public class RegionCityDtoService implements IRegionCityDto{

@Autowired 
ICity serviceCity;
@Autowired 
IRegion serviceRegion;
	@Override
	public List<CityDto> getAllCity() {
		
		return ListMapper.cityListToCityDtoList(serviceCity.getAllCity());
	}
	@Override
	public List<CityDto> getAllCityHaveEateryOrderByName() {
		return ListMapper.cityListToCityDtoList(serviceCity.getAllCityHaveEateryOrderByName());
	}
	@Override
	public List<CityDto> getAllCityHaveEateryOrderByNameByRegionID(Long regionID) {
		return ListMapper.cityListToCityDtoList(serviceCity.getAllCityHaveEateryByRegionIdOrderByName(regionID));
	}
	@Override
	public List<RegionDto> getAllRegion() {
		
		return ListMapper.regionListToRegionDtoList(serviceRegion.getAllRegion());
	}

}
