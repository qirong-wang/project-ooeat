package f2i.eatery.spring.rs.controller;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import data.entities.Customer;
import data.exceptions.NoUserException;
import data.exceptions.PasswordWrongException;
import data.interfaces.ICustomer;
import f2i.eatery.spring.rs.dto.CustomerDto;
import f2i.eatery.spring.rs.dto.LoginDto;
import f2i.eatery.spring.rs.exception.AuthenticateException;
import f2i.eatery.spring.rs.inter.IAuthenticateDto;
import f2i.eatery.spring.rs.inter.ICustomerDto;
import f2i.eatery.spring.rs.mapper.CustomerMapper;

@RestController
@Transactional
public class CustomerController {
	public static final Logger log=LogManager.getLogger(CustomerController.class);
	@Autowired
	ICustomer service;
	@Autowired
	ICustomerDto serviceCustDto;
	@Autowired
	IAuthenticateDto serviceDto;
	
	
	@RequestMapping (path ="/connection",method= RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
	public ResponseEntity <?> connect (@RequestBody Map<String, String> user) throws PasswordWrongException{
		CustomerDto customer;
		try {
			customer= serviceDto.Authenticate(user.get("username"), user.get("password"));
			return new ResponseEntity <CustomerDto>( customer, HttpStatus.OK);
		}
		catch (AuthenticateException e ){
		return new ResponseEntity <String> (e.getLocalizedMessage() , HttpStatus.NOT_FOUND);
		
	} catch (NoUserException e) {
		return new ResponseEntity <String> (e.getLocalizedMessage() , HttpStatus.NOT_FOUND);
		}
	
}
	@RequestMapping (path ="/connect",method= RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
	public ResponseEntity <?> connection (@RequestBody Map<String, String> user) throws PasswordWrongException{
		try {
			Customer authentification = service.authentification(user.get("username"), user.get("password"));
			CustomerDto customerToCustomerDto = CustomerMapper.CustomerToCustomerDto(authentification);
			return new ResponseEntity <CustomerDto>(customerToCustomerDto, HttpStatus.OK);
		}
		catch (PasswordWrongException | NoUserException e ){
			return new ResponseEntity <String> (e.getLocalizedMessage() , HttpStatus.NOT_FOUND);
		}
		
	}
	
	
	
	@RequestMapping (path ="/updatePassword",method= RequestMethod.PUT, consumes={MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity <?> updatePassword (@RequestBody LoginDto login){
		System.out.println(login);
				try {
					serviceDto.updatePassword(login.getUsername(), login.getPassword(), login.getNewpassword());
					return new ResponseEntity <LoginDto> (login , HttpStatus.OK);
				} catch (PasswordWrongException e) {
					return new ResponseEntity <String> (e.getLocalizedMessage() , HttpStatus.NOT_FOUND);
				}
	}
//	@RequestMapping(path="/update", method = RequestMethod.PUT ,consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
//	public ResponseEntity <CustomerDto> update (@RequestBody CustomerDto customer){
//		String username = customer.getUsername();
//		CustomerDto customerDto=serviceDto.findByUsername(username);
//		
//		customerDto= serviceCustDto.modifyCustomerDto(customer);
//		log.info("customerDto");
//		return new ResponseEntity (customerDto, HttpStatus.OK);
//		
//	}
	@RequestMapping(path="/updateCustomer", method = RequestMethod.PUT ,consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity <CustomerDto> update (@RequestBody CustomerDto customerDto){
		CustomerDto modifyCustomer = serviceCustDto.modifyCustomer(customerDto);
//        String username = customerDto.getUsername();
////        Customer c=new Customer();
//        Customer updateCustomer = service.updateCustomer(new Customer(customerDto.getUsername(), customerDto.getPassword(), customerDto.getFisrtName(), customerDto.getLastName(), customerDto.getTitle(), customerDto.getPhone()));
//        CustomerDto customerToCustomerDto = CustomerMapper.CustomerToCustomerDto(updateCustomer);
//        CustomerDto customerUpdated=serviceDto.findByUsername(username);
//        customerUpdated= serviceCustDto.modifyCustomer(customerUpdated);
        log.info(modifyCustomer);
        return new ResponseEntity<CustomerDto> (modifyCustomer, HttpStatus.OK);
        
    }
}
