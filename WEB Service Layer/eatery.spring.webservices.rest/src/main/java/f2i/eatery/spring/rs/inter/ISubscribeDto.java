package f2i.eatery.spring.rs.inter;

import data.exceptions.UserNameAlreadyExistsException;
import f2i.eatery.spring.rs.dto.CustomerDto;

public interface ISubscribeDto {

	public Boolean subscribe(CustomerDto custumerDto) throws UserNameAlreadyExistsException;

	public CustomerDto findByUsername(String username);
	CustomerDto subscribeA(String username, String password, String firstname, String lastname, String title,
            String phone) throws UserNameAlreadyExistsException;
}
