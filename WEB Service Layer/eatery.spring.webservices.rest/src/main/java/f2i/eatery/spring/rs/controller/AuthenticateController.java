package f2i.eatery.spring.rs.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import data.exceptions.NoUserException;
import data.exceptions.PasswordWrongException;
import data.interfaces.ICustomer;
import f2i.eatery.spring.rs.dto.CustomerDto;
import f2i.eatery.spring.rs.dto.LoginDto;
import f2i.eatery.spring.rs.exception.AuthenticateException;
import f2i.eatery.spring.rs.inter.IAuthenticateDto;
import f2i.eatery.spring.rs.inter.ICustomerDto;

@RestController
@Transactional
public class AuthenticateController {
	public static final Logger log = LogManager.getLogger(AuthenticateController.class);
	@Autowired
	ICustomer service;
	@Autowired
	ICustomerDto serviceCustDto;
	@Autowired
	IAuthenticateDto serviceDto;
	
	@RequestMapping (path ="/auth",method= RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
	public ResponseEntity <?> updatepassword (@RequestBody LoginDto login) throws PasswordWrongException, NoUserException{
		System.out.println("-------------------"+login.getUsername());
		System.out.println(login);
		CustomerDto customer;
		try {
			customer = serviceDto.Authenticate(login.getUsername(), login.getPassword());
//			ResponseEntity <CustomerDto> resp =  new ResponseEntity <CustomerDto>( customer, HttpStatus.OK);
			System.out.println(login.getUsername());
			System.out.println(customer.getUsername());
			return new ResponseEntity<CustomerDto>(customer, HttpStatus.OK);
		} catch (AuthenticateException e) {
//				service.authentification(login.getUsername(), login.getPassword());
			return new ResponseEntity<String>(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);

		}
	}

	@RequestMapping(path = "/updPass", method = RequestMethod.PUT, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> authent(@RequestBody LoginDto login) {
		System.out.println(
				"-------------------" + login.getUsername() + " " + login.getPassword() + " " + login.getNewpassword());
		CustomerDto customer;
//		try {
			try {
				serviceDto.updatePassword(login.getUsername(), login.getPassword(), login.getNewpassword());
			} catch (PasswordWrongException e) {
				e.printStackTrace();
			}
//			ResponseEntity <CustomerDto> resp =  new ResponseEntity <CustomerDto>( customer, HttpStatus.OK);
		System.out.println(login.getUsername());
		String user = login.getUsername();
		customer = serviceDto.findByUsername(user);
		log.info("hello............." + customer.getFisrtName());
		return new ResponseEntity<CustomerDto>(customer, HttpStatus.OK);
//		}
//		catch (AuthenticateException e ){
////				service.authentification(login.getUsername(), login.getPassword());
//		return new ResponseEntity <String> (e.getLocalizedMessage() , HttpStatus.NOT_FOUND);
//		
//	}
	}
}



