package f2i.eatery.spring.rs.dto;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReviewDto {
	@XmlElement
	private Long id;
	@XmlElement
	private Long rating;
	@XmlElement
	private String comment;
	public ReviewDto(Long id, Long rating, String comment) {
		super();
		this.id = id;
		this.rating = rating;
		this.comment = comment;
	}
	public ReviewDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRating() {
		return rating;
	}
	public void setRating(Long rating) {
		this.rating = rating;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public String toString() {
		return "ReviewDto [id=" + id + ", rating=" + rating + ", comment=" + comment + "]";
	}
	
}
