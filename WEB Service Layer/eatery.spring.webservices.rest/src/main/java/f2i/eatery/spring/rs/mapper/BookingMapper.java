package f2i.eatery.spring.rs.mapper;

import data.entities.Booking;
import f2i.eatery.spring.rs.dto.BookingDto;
import f2i.eatery.spring.rs.dto.BookingDtoModify;


public class BookingMapper {
	public static BookingDto BookingToBookingDto(Booking booking) {
//		return new BookingDto(booking.getId(), booking.getDate_time(), booking.getNb_people(), booking.getEatery().getName());
		return new BookingDto(booking.getId(), booking.getDate_time(), booking.getNb_people(),booking.getEatery().getId(), booking.getEatery().getName());
	}
	public static Booking BookingDtoToBooking (BookingDto bookingDto) {
		return new Booking(bookingDto.getId(),null, null, bookingDto.getDateTime(), bookingDto.getNb_people(), null, null);
	}
	public static Booking BookingDtoModifyToBooking (BookingDtoModify bookingDto) {
		return new Booking(bookingDto.getId(),null, null, bookingDto.getDateTime(), bookingDto.getNb_people(), null, null);
	}
}
