package f2i.eatery.spring.rs.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RegionDto {
	@XmlElement
private Long id;
	@XmlElement
	private String name ;
//	@XmlElement
//	@JsonIgnore
//	List<City> cities;
	public RegionDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public RegionDto(Long id, String name) {
	super();
	this.id = id;
	this.name = name;
}

	public Long getId() {
		return id;
	}
	
	
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
//	public List<City> getCities() {
//		return cities;
//	}
//	public void setCities(List<City> cities) {
//		this.cities = cities;
//	}
	@Override
	public String toString() {
		return "RegionDto [id=" + id + ", name=" + name + "]";
	}
	
	
}
