package f2i.eatery.spring.rs.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.exceptions.NoUserException;
import data.exceptions.PasswordWrongException;
import data.interfaces.ICustomer;
import f2i.eatery.spring.rs.dto.CustomerDto;
import f2i.eatery.spring.rs.inter.IAuthenticateDto;
import f2i.eatery.spring.rs.mapper.CustomerMapper;

@Service
public class AuthenticateService implements IAuthenticateDto {
	private static final Logger log = LogManager.getLogger(AuthenticateService.class);
@Autowired
ICustomer service;

@Override
public CustomerDto Authenticate(String username, String password) throws PasswordWrongException, NoUserException {
	log.info("Authenticate  call...");
	return CustomerMapper.CustomerToCustomerDto(service.authentification(username, password));

}

@Override
public CustomerDto findByUsername(String username) {
	
	return CustomerMapper.CustomerToCustomerDto(service.findByUsername(username));
}

@Override
public void updatePassword(String username, String password, String newPassword) throws PasswordWrongException {
		service.updatePassword(username, password, newPassword);
	
}
	


}
