package f2i.eatery.spring.rs.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.interfaces.IEatery;
import f2i.eatery.spring.rs.dto.CookingStyleDto;
import f2i.eatery.spring.rs.inter.IEateryDto;
import f2i.eatery.spring.rs.mapper.CookingStyleMapper;

@Service
public class EateryDtoService implements IEateryDto{
@Autowired
IEatery eateryDtoService;
	@Override
	public List<CookingStyleDto> getListCookingStyle(Long cityID) {
		return eateryDtoService.getListCookingStyleByMultiConditions(cityID).stream().map(s->CookingStyleMapper.cookingStyleToCookingStyleDto(s)).collect(Collectors.toList());
	}


}