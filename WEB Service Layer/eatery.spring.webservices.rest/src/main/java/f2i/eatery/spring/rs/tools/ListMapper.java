package f2i.eatery.spring.rs.tools;

import java.util.List;
import java.util.stream.Collectors;

import data.entities.Booking;
import data.entities.City;
import data.entities.Customer;
import data.entities.Region;
import f2i.eatery.spring.rs.dto.BookingDto;
import f2i.eatery.spring.rs.dto.CityDto;
import f2i.eatery.spring.rs.dto.CustomerDto;
import f2i.eatery.spring.rs.dto.RegionDto;
import f2i.eatery.spring.rs.mapper.BookingMapper;
import f2i.eatery.spring.rs.mapper.CityMapper;
import f2i.eatery.spring.rs.mapper.CustomerMapper;
import f2i.eatery.spring.rs.mapper.RegionMapper;




public class ListMapper {
	public static List<RegionDto> regionListToRegionDtoList(List<Region> regionList) {
		return regionList.stream().map(e->RegionMapper.RegionToRegionDto(e)).collect(Collectors.toList());
	}
	public static List<CityDto> cityListToCityDtoList(List<City> cityList) {
		return cityList.stream().map(e->CityMapper.CityToCityDto(e)).collect(Collectors.toList());
	}
	public static CustomerDto customerToCustomerDto(Customer customer) {
		return CustomerMapper.CustomerToCustomerDto(customer);
		
	}

	
	public static List<BookingDto> BookingListToBDtoList(List<Booking> bookingList) {
		return bookingList.stream().map(e->BookingMapper.BookingToBookingDto(e)).collect(Collectors.toList());
	}
}

