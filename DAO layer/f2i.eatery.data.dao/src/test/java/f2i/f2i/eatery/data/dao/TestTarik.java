package f2i.f2i.eatery.data.dao;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import data.entities.Customer;
import data.interfaces.IBooking;
import data.interfaces.ICustomer;
import data.interfaces.IEatery;
import data.interfaces.IReview;
import data.interfaces.IUnsubCustomer;
import data.repos.IbookingRepository;
import data.repos.UnsubscribedRepository;

@Configuration
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@ContextConfiguration(locations = { "classpath*:app-context-service.xml" })

class TestTarik {
	@Autowired
	ICustomer service;
	@Autowired
	IUnsubCustomer serviceUnsub;
	@Autowired
	IBooking serviceBooking;
	@Autowired
	IEatery serviceEatery;
	@Autowired
	IReview serviceReview;
	@Autowired
	UnsubscribedRepository serviceRepo;
	@Autowired
	IbookingRepository bookingRespository;
	private static final Logger log = LogManager.getLogger(TestTarik.class);

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

//	@Test
//	void test() {
//		log.info("Authentication Test start");
//		String username = "user";
//		String password = "1234";
//		Customer customer = service.authentification(username, password);
//		log.info(customer);
//		assertTrue(customer != null, "Username or password are incorrect");
//		log.info("welcome " + customer.getFirstName());
//	}

	@Test
	void testUpdate() {
		log.info("Update Start .......");
		String username = "Qirong";
		String phone = "0989890000";
		String firstName = "Qirong";
		Customer customer = service.findByUsername(username);
		customer.setFirstName(firstName);
		customer.setPhone(phone);
		customer = service.updateCustomer(customer);
		log.info("tset" + customer);

	}

//	@Test
//	void testUpdatePassword() {
//		log.info("Update Start .......");
//		String username = "hakim";
//		Customer customer1 = service.findByUsername(username);
//		String oldpass = customer1.getPassword();
//		String password = "Agen";
//		String pass = "000000000";
//		service.updatePassword(username, password, pass);
//		Customer customer = service.findByUsername(username);
////		service.updateCustomer(customer);
//		String newpass = customer.getPassword();
//		if (newpass != oldpass) {
//			log.info("good...........");
//		} else {
//			log.info("bad...........");
//		}
//		log.info(customer.getPassword());
//	}

	@Transactional
	@Test
	void testUnsubscribe() {
		log.info("Unsubscribe Start .......");
		String username = "Qirong";
//		UnsubscribedCustomer unsub = new UnsubscribedCustomer();
	service.findByUsername(username);
//List<Booking> booking =bookingRespository.(username);
//List<Booking> booking2 =new ArrayList<>(booking);
//log.info("booking2    :"+booking2 );

//		UnsubscribedCustomer unsub = new UnsubscribedCustomer();
//		unsub.setUsername("FrenchMan *Unsub*");
//		unsub.setPassword(customer.getPassword());
//		unsub.setFirstName(customer.getFirstName());
//		unsub.setLastName(customer.getLastName());
//		unsub.setPhone(customer.getPhone());
//		unsub.setTitle(customer.getTitle());
//		List<Booking> bok=customer.getBookings();
//		String x =((Booking) bok).getCustomer().setUsername("Qirong U");
//		log.info("change username" + b);
//		bok.set(2, "FrenchMan *Unsub*");
//		unsub.setBookings(customer.getBookings());
//		log.info(unsub);
//		service.unsubscribeCustomer(username);

//serviceRepo.save(unsub);
	}

	@Test
	void testLastBooking() {
		serviceBooking.getBooking("FrenchMan", 2L, 6L, LocalDateTime.of(2019, Month.DECEMBER, 20, 05, 30));
	}

	@Test
	void testDeleteBooking() {
		serviceBooking.deleteBooking(4L);
	}

	@Test
	void testgellAllfuturBooking() {
		serviceBooking.getFuturBooking("hakim");
		serviceBooking.getPreviousBooking("hakim");
	}

	@Test
	void updateBooking() {
		serviceBooking.update(10L, "Qirong", LocalDateTime.of(2019, Month.DECEMBER, 20, 05, 30), 20L);
	}

	@Test
	void saveReview() {
		String comment = "cooooooooooooooooooooooooooooooooooooooooooool !!!!";
		serviceReview.saveReview(20L, 4L, comment);
	}

	@Test
	void randddom() {
		String c = "ABCDEFGHIJKLMONPQRSTUWVXYZ";
		int length = 5;
		char[] text = new char[length];
		String rdString = "";
		Random rd = new Random();
		for (int i = 0; i < length; i++) {
			text[i] = c.charAt(rd.nextInt(c.length()));
			rdString += text[i];

		}
		for (int i = 0; i < 20; i++) {
			int y = rd.nextInt(11);
			System.out.print(" : " + y);
		}
		log.info(rdString);
		for (int i = 4; i >= 0; i--) {

			for (int j = 0; j <= i; j++) {
				System.out.print("*");
			}
			System.out.println("");
		}
	}
	
	@Test 
	void sybs() {
//		service.subscribeA("tuu9u9u", "rakkaa83", "Tarik", "RAKKAA", "Mr", "0762977955");
	}
//	@Test
//	void TestSusbs() {
//		service.cnx("tariko", "rakkaa83");
//	}
//	@Test 
//	void updateinfoooo(){
//		service.updateInfo("Qirong", "0000", "Qirong", "Wang", "Mr", "9999999");
//	}
}
