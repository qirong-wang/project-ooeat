package f2i.f2i.eatery.data.dao;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import data.entities.Booking;
import data.entities.City;
import data.entities.CookingStyle;
import data.entities.Customer;
import data.entities.Eatery;
import data.entities.ImageData;
import data.entities.Region;
import data.exceptions.NoUserException;
import data.exceptions.PasswordWrongException;
import data.interfaces.IBooking;
import data.interfaces.ICity;
import data.interfaces.ICustomer;
import data.interfaces.IEatery;
import data.interfaces.IImageData;
import data.interfaces.IInjection;
import data.interfaces.IRegion;
import data.interfaces.IReview;
import data.po.EateryInfo;
import data.po.EateryRequest;
@Configuration
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@ContextConfiguration(locations = { "classpath*:app-context-service.xml" })
class TestQirong {
	private static final Logger log = LogManager.getLogger(TestQirong.class);
	@Autowired
	ICity cityService;
	@Autowired
	IRegion regionService;
	@Autowired
	IEatery eateryService;
	@Autowired
	IImageData imageDataService;
	@Autowired
	ICustomer customerService;
	@Autowired
	IBooking bookingService;
	@Autowired
	IInjection injectionService;
	@Autowired
	IReview reviewService;
	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void test() {
		City oneCityByID = cityService.getOneCityByID(2l);
		System.out.println(oneCityByID.getName());
	}
	@Test
	void testFindAllCity() {
//		List<City> allCity = cityService.getAllCity();
		List<City> allCity = cityService.getListCityByNameLike("i");
		allCity.forEach(c->System.out.println(c));
	}
	@Test
	void testFindAllRegion() {
		List<Region> allRegion = regionService.getAllRegion();
		allRegion.forEach(c->System.out.println(c));
	}
	@Test
	void testFindAllCityByRegionID() {
		List<City> listCityByRegionID = cityService.getListCityByRegionID(2l);
		listCityByRegionID.forEach(c->System.out.println(c));
	}
	@Test
	void testEatery() {
		long time1 = System.currentTimeMillis();
//		List<EateryInfo> findListEateryByMultiConditions = eateryService.findListEateryByMultiConditions(null, null, null, null, null);
//		eateryService.findListEateryByMultiConditions(cityID, cookingStylesID, tagsID, rating, firstPage, maxSize, restrictionOrder)
//		List<EateryInfo> findListEateryByMultiConditions = eateryService.findListEateryByMultiConditions(null, null, null, null,null, 20, "count(r.booking) desc");
//		public List<EateryInfo> findListEateryByMultiConditions(Long cityID, List<Long> cookingStylesID, List<Long> tagsID, Integer minPrice, Integer maxPrice, Integer minRating, Integer firstPage, Integer maxSize, String restrictionOrder); 
//		List<EateryInfo> findListEateryByMultiConditions = eateryService.findListEateryByMultiConditions(null, null, null, 1+"",""+12,null,null, null, "count(r.booking) desc");
		List<EateryInfo> findListEateryByMultiConditions = eateryService.findListEateryByMultiConditions(1l, null, null, null,199+"",299+"",null, null, null);
		System.out.println("findListEateryByMultiConditions + OK");
//		findListEateryByMultiConditions.forEach(e->System.out.println(e));
		Integer findNbEateryByMultiConditions = eateryService.findNbEateryByMultiConditions();
		System.out.println("--" + findListEateryByMultiConditions.size());
//		System.out.println(findListEateryByMultiConditions.size());
		if(findListEateryByMultiConditions!=null)	System.out.println(findListEateryByMultiConditions.get(0));
//			System.out.println("findListEateryByMultiConditions.get(0)  + OK");
		System.out.println(findNbEateryByMultiConditions);
			System.out.println(findNbEateryByMultiConditions+"HHHH");
				eateryService.getNbCookingStyleByMultiConditions();
//			Map<Long, String[]> nbCookingStyleByMultiConditions = eateryService.getNbCookingStyleByMultiConditions();
					
					long time2 = System.currentTimeMillis();
					System.out.println(time2-time1 + "mS");
	}
	@Test
	void testEatery2() {
		eateryService.getNbCookingStyleByMultiConditions();
	}
	@Test
	void testEatery5() {
		eateryService.findNbEateryByMultiConditions();
	}
	@Test
	void testEatery6() {
		List<Long> listLong = new ArrayList<Long>();
		listLong.add(2L);
		listLong.add(5L);
		listLong.add(1L);
		System.out.println(listLong);
		System.out.println(listLong.toString().replace('[','(').replace(']', ')'));
		
	}
	@Test
	void testEatery64() {
		long time1 = System.currentTimeMillis();
//		EateryRequestDTO requestDTO=new EateryRequestDTO(cityID, cookingStyles, tags, order, minNote, maxPrice, minPrice, firstPage, maxSize);
		EateryRequest requestDTO=new EateryRequest(null, null, null, null, 166+"", ""+222, null, null, null);
		List<EateryInfo> findListEateryByMultiConditions = eateryService.findListEateryByMultiConditions(requestDTO);
		findListEateryByMultiConditions.forEach(e->System.out.println(e));
		System.out.println(findListEateryByMultiConditions.size()+" size");
		long time2 = System.currentTimeMillis();
		System.out.println(time2-time1 +"-----");
	}
	@Test
	void testEatery665() {
		Eatery oneEateryByEateryID = eateryService.getOneEateryByEateryID(3l);
		System.out.println(oneEateryByEateryID);
	}
	@Test
	void testEatery65() {
//		String[] a=new String[]{"1","3"};
//		System.out.println(Arrays.toString(a));
		long time1 = System.currentTimeMillis();
//		EateryRequestDTO requestDTO=new EateryRequestDTO(cityID, cookingStyles, tags, order, minNote, maxPrice, minPrice, firstPage, maxSize);
//		EateryRequest requestDTO=new EateryRequest(null, null, null, null, 166+"", ""+222, null, null, null);
//		EateryRequest requestDTO=new EateryRequest(null, new String[] {"1","6"}, new String[] {"1"}, null, null, null, null, null, null);
//		EateryRequest requestDTO=new EateryRequest(6l, new String[] {"1","20"}, null, "p.price", null, null, null, null, null);
		EateryRequest requestDTO=new EateryRequest(1l, null, null, null, null, null, null, null, 10);
		EateryRequest requestDTO2=new EateryRequest(1l, null, null, null, null, null, null, 10, 10);
//		EateryRequest requestDTO=new EateryRequest(null, null, new String[] {"1"}, null, null, null, null, null, null);
//		EateryRequest requestDTO=new EateryRequest(null, null, null, null, null, null, null, null, null);
		List<EateryInfo> findListEateryByMultiConditions = eateryService.findListEateryByMultiConditions(requestDTO);
	eateryService.findListEateryByMultiConditions(requestDTO2);
		findListEateryByMultiConditions.forEach(e->System.out.println(e));
		
		System.out.println(findListEateryByMultiConditions.size() +" size findListEateryByMultiConditions");
		System.out.println("size: by NB : " + eateryService.findNbEateryByMultiConditions(requestDTO));
		long time2 = System.currentTimeMillis();
		System.out.println(time2-time1 +"--TIME---");
	}
	@Test
	void testRegionCity() {
		List<Region> allRegion = regionService.getAllRegion();
		allRegion.forEach(e->System.out.println(e));
		List<City> listCityByRegionID = cityService.getListCityByRegionID(1l);
		listCityByRegionID.forEach(e->System.out.println(e));
	}
	@Test
	void testCreateReview() {
		List<City> allCity = cityService.getAllCity();
		System.out.println(allCity.size());
		List<City> allCity2 = cityService.getAllCityHaveEateryOrderByName();
		System.out.println(allCity2.size());
	}
	@Test
	void testCreateImg() {
		ImageData oneImageByID = imageDataService.getOneImageByID(6l);
		System.out.println(oneImageByID.getSize());
		
		List<Long> listPhotoIDByEateryID = imageDataService.getListPhotoIDByEateryID(1l);
		System.out.println(listPhotoIDByEateryID);
	}
	@Test
	void testCreateuser() {
//		Customer custumer = new Customer();
		Customer custumer = new Customer("AAAAAaaa","dada","fsd","fsf","fsfs","dsfds");
		System.out.println(custumer);
//		custumer.setFirstName("test");
//		custumer.setLastName("test");
//		custumer.setUsername("testaaa");
//		custumer.setPassword("test");
//		custumer.setTitle("Mr");
//		custumer.setPhone("145678992");
		try {
			customerService.subscribe(custumer);
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
//			e.printStackTrace();
		}
		log.info(custumer.getTitle() + " " + custumer.getLastName() + " est bien enregistré");
	}
	@Test
	void testFindBooking() {
		Customer findByUsername = customerService.findByUsername("Ba.ZHOU@hotmail.com");
		System.out.println(findByUsername);
		List<Booking> previousBooking = bookingService.getPreviousBooking("Ba.ZHOU@hotmail.com");
		previousBooking.forEach(e->System.out.println(e.getDate_time()+"--"+e.getNb_people()+"--"+e.getCustomer().getLastName()+"--"+e.getEatery().getName()+e.getId()));
//		List<Booking> findByCustomerAndDateTimeAfterOrderByDateTimeAsc = bookingService.f(findByUsername, LocalDateTime.now());
		System.out.println(previousBooking.size());
	}
	@Test
	void creatBooking() {
		Booking b=new Booking();
		b.setCustomer(customerService.findByUsername("Yi.LIU@hotmail.com"));
		b.setDate_time(LocalDateTime.now());
		b.setEatery(eateryService.getOneEateryByEateryID(2l));
		b.setNb_people(5l);
		Booking saveBooking = bookingService.saveBooking(b);
		System.out.println(saveBooking);
	}
	@Test
	void getCookingStyle() {
//		List<CookingStyle> listCookingStyleByMultiConditions = eateryService.getListCookingStyleByMultiConditions(1l);
		List<CookingStyle> listCookingStyleByMultiConditions = eateryService.getListCookingStyleByMultiConditions(0l);
		System.out.println(listCookingStyleByMultiConditions.size());
	}
	@Test
	void getWeek() {
		LocalDateTime now = LocalDateTime.of(2133, 2, 12, 21, 22);
		String format = now.format(DateTimeFormatter.ofPattern("E", Locale.FRANCE));
		System.out.println(format);
	}
	@Test
	void testDay() {
		LocalDateTime now = LocalDateTime.now().plusDays(1l);
		LocalDateTime now2 = LocalDateTime.now().plusDays(2l);
		String format = now.format(DateTimeFormatter.ofPattern("dd/MM/yyy E hh:mm", Locale.FRANCE));
		if(now.isAfter(now2))System.out.println("OK");else {
			System.out.println("NO");
		}
		System.out.println(format);
	}
	@Test
	void aut() {
		try {
			customerService.authentification("aaaa", "bbb");
		} catch (PasswordWrongException e) {
			System.out.println(e.getLocalizedMessage());
		} catch (NoUserException e) {
			System.out.println(e.getLocalizedMessage());
		}
//		System.out.println(authentification);
	}
	@Test
	void auaat() {
			try {
				customerService.updatePassword("aaa", "bbb", "aaa");
			} catch (PasswordWrongException e) {
				System.out.println(e.getLocalizedMessage());
			}
//		System.out.println(authentification);
	}
	@Test
	void aaa() {
		List<Customer> injectionCustomer = injectionService.injectionCustomer();
		System.out.println(injectionCustomer.size());
	}
	@Test
	void TestTime() {
		LocalDateTime parse = LocalDateTime.parse("2019-07-06T13:00", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"));
		System.out.println(parse.getDayOfMonth());
	}
	@Test
	void bbb() {
		Boolean injectionBMC4j = injectionService.injectionBMC4j();
		System.out.println(injectionBMC4j);
	}
	@Test
	void cc () {
		injectionService.injectionCust();
	}
	@Test
	void findNbReviews () {
		Integer nbReviewsWithCommentByEateryID = reviewService.getNbReviewsWithCommentByEateryID(1l);
		System.out.println(nbReviewsWithCommentByEateryID);
	}
	@Test
	void TestImage () {
		Long oneSmallImageDataIDByEateryID=null;
			oneSmallImageDataIDByEateryID = imageDataService.getOneSmallImageDataIDByEateryID(31l);
		if(oneSmallImageDataIDByEateryID==null||oneSmallImageDataIDByEateryID==0l)
			System.out.println("NO photo!!");
		else {
			ImageData image = imageDataService.getOneImageByID(oneSmallImageDataIDByEateryID);
			System.out.println(image.getSize());
		}
	}

}
