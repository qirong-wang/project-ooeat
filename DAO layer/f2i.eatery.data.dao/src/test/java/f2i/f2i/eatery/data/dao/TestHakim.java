package f2i.f2i.eatery.data.dao;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import data.entities.ImageData;
import data.interfaces.ICustomer;
import data.interfaces.IImageData;

@Configuration
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@ContextConfiguration(locations = { "classpath*:app-context-service.xml" })
class TestHakim {

//	private static final Logger log = LogManager.getLogger(TestHakim.class);

	@Autowired
	IImageData imageDataService;

	@Autowired
	ICustomer service;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void findImageById() {
		ImageData imagefoud = imageDataService.getOneImageByID(356l);
		System.out.println(imagefoud.getContent().length);
	}

//	@Test
//	void testauthenticate() {
//		log.info("Authentication Test start");
//		String username = "aaaaaaaaaaaaaaaaiden11e@hotmail.fr";
//		String password = "zaaz11";
//		Customer customer = service.authentification(username, password);
//		log.info(customer);
////				assertTrue(customer!= null, "Username or password are incorrect");
//		log.info("welcome " + customer.getFirstName());
//	}

//	@Test
//	void findSmallImage() {
//		List<ImageData> imagefouds =  imageDataService.findSmallImageOfEatery("small");		
//		
//		System.out.println(imagefouds.size());
//	}

//	@Test
//	void testCreateCustumer() throws UserNameAlreadyExistsException {
//		Customer custumer = new Customer();
//
//		custumer.setFirstName("testaaaaa");
//		custumer.setLastName("test");
//		custumer.setUsername("testaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
//		custumer.setPassword("test");
//		custumer.setTitle("Mr");
//		custumer.setPhone("145678992");
//		try {
//			service.subscribe(custumer);
//		} catch (Exception e) {
//			System.out.println(e.getLocalizedMessage());
////			e.printStackTrace();
//		}
//		log.info(custumer.getTitle() + " " + custumer.getLastName() + " est bien enregistré");
//
//	}

}
