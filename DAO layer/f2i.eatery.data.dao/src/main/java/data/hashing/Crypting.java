package data.hashing;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Crypting {
	private static final Logger log = LogManager.getLogger(Crypting.class);

		public static String encryptingMessage(String password) {
//				String passwordTobeE = new String (passwordTobeEncrypted);
				String passwordSalted = PasswordHashed.passSalt( password);
				SecretKeySpec key = hashMachine.hashingPassword(passwordSalted);
				
				Cipher cipher = null;
				try {
					cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
				} catch (NoSuchAlgorithmException e) {
					log.info(" cipher instanse  Algo Error"+ e.getLocalizedMessage());
				} catch (NoSuchPaddingException e) {
					log.info(" cipher instanse Error"+ e.getLocalizedMessage());

				}
				try {
					cipher.init(Cipher.ENCRYPT_MODE, key);
				} catch (InvalidKeyException e) {
					log.info( e.getLocalizedMessage());

				}
				
				try {
					return Base64.getEncoder().encodeToString(cipher.doFinal(password.getBytes("UTF-8")));
			
				} catch (IllegalBlockSizeException e) {
					log.info( e.getLocalizedMessage());
				} catch (BadPaddingException e) {
					log.info( e.getLocalizedMessage());
				} catch (UnsupportedEncodingException e) {
					log.info( e.getLocalizedMessage());
				}
				return null;
			
			

		}

//		public static String decryptingMessage(String password,  String passwordEncrypted ) {
//
//			
//			String passwordSalted = PasswordHashed.passSalt( password);
//			SecretKeySpec key = hashMachine.hashingPassword(passwordSalted);
//				Cipher cipher = null;
//				try {
//					cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
//				} catch (NoSuchAlgorithmException e) {
//					log.info( e.getLocalizedMessage());
//				} catch (NoSuchPaddingException e) {
//					log.info( e.getLocalizedMessage());
//				}
//				try {
//					cipher.init(Cipher.DECRYPT_MODE, key);
//				} catch (InvalidKeyException e) {
//					log.info( e.getLocalizedMessage());
//				}
//
//				try {
//					return new String(cipher.doFinal(Base64.getDecoder().decode(passwordEncrypted)));
//				} catch (IllegalBlockSizeException e) {
//					log.info( e.getLocalizedMessage());
//				} catch (BadPaddingException e) {
//					log.info( e.getLocalizedMessage());
//				}
//return null;
//		
//
//		}
}

