package data.hashing;

import java.security.SecureRandom;

public class GenerateSalt {
	private static final SecureRandom sc = new SecureRandom();

	public static byte[] getSalt(byte[] salt, int length) {
		salt = new byte[length];
		sc.nextBytes(salt);

		return salt;
	}
}
