package data.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "eatery")
public class Eatery {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	private String name;
	private String description;
	@Column(name="menu_id")
	private Long menuID;
	@Embedded
	private Address address;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "city_id")
	private City city;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cooking_style_id")
	private CookingStyle cookingStyle;

	@OneToMany(mappedBy = "eatery")
	private List<Booking> bookings;


	@OneToOne(targetEntity = PracticalInfo.class, cascade = { CascadeType.ALL })
	@PrimaryKeyJoinColumn
	private PracticalInfo practicalInfo;
	
	@ManyToMany(targetEntity =EateryTag.class,fetch = FetchType.EAGER)
	@JoinTable(name="eatery_eatery_tag", joinColumns={@JoinColumn(name="eatery_id", referencedColumnName="id")}
     , inverseJoinColumns={@JoinColumn(name="tag_id", referencedColumnName="id")})
	private List<EateryTag> eateryTags;
	
	/**
	 * @return the eateryTags
	 */
	public List<EateryTag> getEateryTags() {
		return eateryTags;
	}

	/**
	 * @param eateryTags the eateryTags to set
	 */
	public void setEateryTags(List<EateryTag> eateryTags) {
		this.eateryTags = eateryTags;
	}

	public List<EateryTag> getEateryTag() {
		return eateryTags;
	}

	public void setEateryTag(List<EateryTag> eateryTag) {
		this.eateryTags = eateryTag;
	}

	public Eatery(Long id, String name, String description, Address address, City city, CookingStyle cookingStyle,
			List<Booking> bookings,  PracticalInfo practicalInfo) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.address = address;
		this.city = city;
		this.cookingStyle = cookingStyle;
		this.bookings = bookings;
		this.practicalInfo = practicalInfo;
	}

	/**
	 * @return the menuID
	 */
	public Long getMenuID() {
		return menuID;
	}

	/**
	 * @param menuID the menuID to set
	 */
	public void setMenuID(Long menuID) {
		this.menuID = menuID;
	}

	public PracticalInfo getPracticalInfo() {
		return practicalInfo;
	}

	public void setPracticalInfo(PracticalInfo practicalInfo) {
		this.practicalInfo = practicalInfo;
	}

	public CookingStyle getCookingStyle() {
		return cookingStyle;
	}

	public void setCookingStyle(CookingStyle cookingStyle) {
		this.cookingStyle = cookingStyle;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}


	public Eatery() {
		super();
	}

	public Eatery(Long id, String name, String description, Address address, City city, CookingStyle cookingStyle,
			List<Booking> bookings) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.address = address;
		this.city = city;
		this.cookingStyle = cookingStyle;
		this.bookings = bookings;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Eatery [id=" + id + ", name=" + name + ", description=" + description + ", address=" + address
				+ ", city=" + city + ", cookingStyle=" + cookingStyle + "]";
	}

}
