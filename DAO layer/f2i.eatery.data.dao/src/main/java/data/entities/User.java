package data.entities;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity 
@Table(name="user_data")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type", discriminatorType=DiscriminatorType.STRING)
public class User implements Serializable {
	private static final long serialVersionUID = -6471097991703823707L;
	
	@Id
	protected String username;
		
	protected String password;
	
	

	public User(String username, String passwordEncry) {
		super();
		this.username = username;
		this.password = passwordEncry;
		
	}

	public User() {
		super();
		
	}
	

	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasswordEncry() {
		return password;
	}

	public void setPasswordEncry(String passwordEncry) {
		this.password = passwordEncry;
	}

	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", passwordEncry=" + password +  "]";
	} 
	
	
	

}
