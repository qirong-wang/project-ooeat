package data.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class Review {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@OneToOne
	@JoinColumn(name="booking_id")
	private Booking booking;
	private Long rating;
	@Column (name= "comment", length =500)
	private String comment;
	
	public Review() {
		super();
	
	}

	public Review(Long id, Long rating, String comment) {
		super();
		this.id = id;
		this.rating = rating;
		this.comment = comment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Long getRating() {
		return rating;
	}

	public void setRating(Long rating) {
		this.rating = rating;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}

	public Review(Long id, Booking booking, Long rating, String comment) {
		super();
		this.id = id;
		this.booking = booking;
		this.rating = rating;
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "Review [id=" + id  + ", rating=" + rating + ", comment=" + comment + "]";
	}

	public Review(Booking booking, Long rating, String comment) {
		super();
		this.booking = booking;
		this.rating = rating;
		this.comment = comment;
	}
	

}
