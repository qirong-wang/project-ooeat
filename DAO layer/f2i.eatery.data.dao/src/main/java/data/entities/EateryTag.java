package data.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
@Entity 
@Table (name = "eatery_tag")
public class EateryTag {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Long id;
		private String name;
		@ManyToMany(mappedBy = "eateryTags")
		private List<Eatery> eateries;
		/**
		 * @return the id
		 */
		public Long getId() {
			return id;
		}
		/**
		 * @param id the id to set
		 */
		public void setId(Long id) {
			this.id = id;
		}
		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}
		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}
		/**
		 * @return the eatery
		 */
		public List<Eatery> getEatery() {
			return eateries;
		}
		/**
		 * @param eatery the eatery to set
		 */
		public void setEatery(List<Eatery> eatery) {
			this.eateries = eatery;
		}
		public EateryTag(Long id, String name, List<Eatery> eatery) {
			super();
			this.id = id;
			this.name = name;
			this.eateries = eatery;
		}
		public EateryTag() {
			super();
			// TODO Auto-generated constructor stub
		}
		@Override
		public String toString() {
			return "EateryTag [id=" + id + ", name=" + name + "]";
		}
		
}
