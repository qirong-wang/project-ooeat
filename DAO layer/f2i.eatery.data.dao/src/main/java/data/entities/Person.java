package data.entities;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Person extends User {
	private static final long serialVersionUID = 2691591896246229845L;
	
	
	@Column(name="first_name")
	protected String firstName;

	@Column(name="last_name")
	protected String lastName;

	

	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}
//	public Person(String username, String password, String firstName, String lastName) {
//		super(username, password);
//		this.firstName = firstName;
//		this.lastName = lastName;
//	}
	
	public Person(String username, String passwordEncry, String firstName, String lastName) {
		super(username, passwordEncry);
		this.firstName = firstName;
		this.lastName = lastName;
	}
	// getters et setters
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
