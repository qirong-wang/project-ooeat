package data.entities;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
	@Table(name = "city")
public class City {
	@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
			@Column(name = "id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="post_code")
	private String postCode;
	
	private Boolean foremost;
	
	@ManyToOne(fetch = FetchType.EAGER)
		@JoinColumn(name="region_id")
			private Region region;
	

	@OneToMany(mappedBy="city", fetch = FetchType.LAZY)
	List<Eatery> eateries;

	public City() {}
	
	public City(Long id, String name, String postCode,  Boolean foremost) {
		super();
		this.id = id;
		this.name = name;
		this.foremost = foremost;
		this.postCode = postCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Boolean getForemost() {
		return foremost;
	}

	public void setForemost(Boolean foremost) {
		this.foremost = foremost;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public List<Eatery> getEateries() {
		return eateries;
	}

	public void setEateries(List<Eatery> eateries) {
		this.eateries = eateries;
	}

	public List<Address> getAddresses(String postcode ) {
//	List<Address> addresses = new ArrayList<Address>();
//	for (Eatery e : eateries) {
//		if ( e.getAddress().getPostCode() != null &&  e.getAddress().getPostCode().startsWith(postcode))
//			addresses.add(e.getAddress());
//	}

// Jav8 - Lambda and stream Style
// Code equivalent donnant le même résultat que la boucle for
//
		List<Address> adr = eateries.stream().filter(e -> e.getAddress().getPostCode().startsWith(postcode)).map(e -> e.getAddress()).collect(Collectors.toList());
	return adr;
}
	@Override
	public String toString() {
		return "City [id=" + id + ", name=" + name + ", postCode=" + postCode + ", foremost=" + foremost + ", region="
				+ region + "]";
	}
}
