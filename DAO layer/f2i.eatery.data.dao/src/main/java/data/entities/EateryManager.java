package data.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;



@Entity 
	@DiscriminatorValue("E")
public class EateryManager extends Person {
	private static final long serialVersionUID = 1884710293248197489L;
	
	@Column(name="eatery_id")
	private Long eateryId;

	@OneToMany(mappedBy="eateryManager",fetch = FetchType.EAGER)
	List<BookingReport> bookingReports;

	

	public EateryManager(String username, String password,  String firstName, String lastName,
			Long eateryId, List<BookingReport> bookingReports) {
		super(username, password,  firstName, lastName);
		this.eateryId = eateryId;
		this.bookingReports = bookingReports;
	}


	public Long getEateryId() {
		return eateryId;
	}


	public void setEateryId(Long eateryId) {
		this.eateryId = eateryId;
	}


	public List<BookingReport> getBookingReports() {
		return bookingReports;
	}

	public void setBookingReports(List<BookingReport> bookingReports) {
		this.bookingReports = bookingReports;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "EateryManager [eateryId=" + eateryId + ", bookingReports=" + bookingReports + "]";
	}




	
}
