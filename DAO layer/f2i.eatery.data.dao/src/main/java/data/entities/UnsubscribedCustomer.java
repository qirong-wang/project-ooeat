package data.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity 
@DiscriminatorValue("U")
public class UnsubscribedCustomer extends Person {
	private static final long serialVersionUID = 3049458943934326713L;
	private String title;
	private String phone;
	@OneToMany(mappedBy = "customer", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<Booking> bookings;
	

	public UnsubscribedCustomer(String username, String passwordEncry,  String firstName, String lastName) {
		super(username, passwordEncry,  firstName, lastName);
		// TODO Auto-generated constructor stub
	}

	

	public UnsubscribedCustomer(String username, String passwordEncry, String firstName, String lastName,
			String title, String phone, List<Booking> bookings) {
		super(username, passwordEncry, firstName, lastName);
		this.title = title;
		this.phone = phone;
		this.bookings = bookings;
	}



	// getters and setters
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	@Override
	public String toString() {
		return "Customer [title=" + title + ", phone=" + phone + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", username=" + username + ", password=*****" + "]";
	}
	
	
}
