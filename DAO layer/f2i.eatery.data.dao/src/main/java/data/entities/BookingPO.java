package data.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "booking")
public class BookingPO {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	@Column(name = "eatery_id")
	private Long eatery_id;
	@Column(name = "customer_id")
	private String customer_id;
	private LocalDateTime date_time;
	private Integer nb_people;
	public BookingPO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BookingPO(Long id, Long eatery_id, String customer_id, LocalDateTime date_time, Integer nb_people) {
		super();
		this.id = id;
		this.eatery_id = eatery_id;
		this.customer_id = customer_id;
		this.date_time = date_time;
		this.nb_people = nb_people;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the eatery_id
	 */
	public Long getEatery_id() {
		return eatery_id;
	}
	/**
	 * @param eatery_id the eatery_id to set
	 */
	public void setEatery_id(Long eatery_id) {
		this.eatery_id = eatery_id;
	}
	/**
	 * @return the customer_id
	 */
	public String getCustomer_id() {
		return customer_id;
	}
	/**
	 * @param customer_id the customer_id to set
	 */
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	/**
	 * @return the date_time
	 */
	public LocalDateTime getDate_time() {
		return date_time;
	}
	/**
	 * @param date_time the date_time to set
	 */
	public void setDate_time(LocalDateTime date_time) {
		this.date_time = date_time;
	}
	/**
	 * @return the nb_people
	 */
	public Integer getNb_people() {
		return nb_people;
	}
	/**
	 * @param nb_people the nb_people to set
	 */
	public void setNb_people(Integer nb_people) {
		this.nb_people = nb_people;
	}
	@Override
	public String toString() {
		return "BookingPO [id=" + id + ", eatery_id=" + eatery_id + ", customer_id=" + customer_id + ", date_time="
				+ date_time + ", nb_people=" + nb_people + "]";
	}



}
