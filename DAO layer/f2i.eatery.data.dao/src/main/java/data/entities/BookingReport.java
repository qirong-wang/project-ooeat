package data.entities;


import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "booking_report")
public class BookingReport {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name="input_date")
	private LocalDateTime date;
	
	@Column(name="fulfilled")
	private Long fulfilled;
	
	@Column(name="comment")
	private String commentString;
	
	@Column(name="taking_amount")
	private Double takingAmount;
	
	@Column(name="due_amount")
	private Double dueAmount;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "manager_id")
	private EateryManager eateryManager;
	
	@OneToOne(optional = true,cascade = CascadeType.ALL)
	@JoinColumn(name="booking_id")
	private Booking booking;

	public BookingReport() {
	
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Long getFulfilled() {
		return fulfilled;
	}

	public void setFulfilled(Long fulfilled) {
		this.fulfilled = fulfilled;
	}

	public String getCommentString() {
		return commentString;
	}

	public void setCommentString(String commentString) {
		this.commentString = commentString;
	}

	public Double getTakingAmount() {
		return takingAmount;
	}

	public void setTakingAmount(Double takingAmount) {
		this.takingAmount = takingAmount;
	}

	public Double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public EateryManager getEateryManager() {
		return eateryManager;
	}

	public void setEateryManager(EateryManager eateryManager) {
		this.eateryManager = eateryManager;
	}

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}

	@Override
	public String toString() {
		return "BookingReport [id=" + id + ", date=" + date + ", fulfilled=" + fulfilled + ", commentString="
				+ commentString + ", takingAmount=" + takingAmount + ", dueAmount=" + dueAmount + ", eateryManager="
				 + ", booking="  + "]";
//		+ eateryManager + ", booking=" + booking + "]";

				
	}

	
	


	
	
}
