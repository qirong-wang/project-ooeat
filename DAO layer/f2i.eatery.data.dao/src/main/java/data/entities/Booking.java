package data.entities;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "booking")
public class Booking implements Cloneable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "eatery_id")
	private Eatery eatery;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "customer_id")
	private Customer customer;
	@Column(name = "date_time")
	private LocalDateTime dateTime;
	private Long nb_people;
	@OneToOne(mappedBy = "booking", orphanRemoval = true)
	private BookingReport bookingReport;
	@OneToOne(mappedBy = "booking", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Review review;

	public Booking(Long id, Eatery eatery, Customer customer, LocalDateTime date_time, Long nb_people,
			BookingReport bookingReport, Review review) {
		super();
		this.id = id;
		this.eatery = eatery;
		this.customer = customer;
		this.dateTime = date_time;
		this.nb_people = nb_people;
		this.bookingReport = bookingReport;
		this.review = review;
	}

	public Booking(Eatery eatery, Customer customer, LocalDateTime dateTime, Long nb_people) {
		super();
		this.eatery = eatery;
		this.customer = customer;
		this.dateTime = dateTime;
		this.nb_people = nb_people;
	}
//	public List<Object> clone() {
//		List<Booking> booking = null;
//	    try {
//	    	
//	    	booking = (List<Booking>) super.clone();
//	    } catch(CloneNotSupportedException cnse) {
//	      
//	      	cnse.printStackTrace(System.err);
//	    }
//	 
//	 
//	    return booking;
//	}
	public Review getReview() {
		return review;
	}

	public void setReview(Review review) {
		this.review = review;
	}

	public LocalDateTime getDate_time() {
		return dateTime;
	}

	public void setDate_time(LocalDateTime date_time) {
		this.dateTime = date_time;
	}

//	public Review getReview() {
//		return review;
//	}
//
//	public void setReview(Review review) {
//		this.review = review;
//	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Eatery getEatery() {
		return eatery;
	}

	public void setEatery(Eatery eatery2) {
		this.eatery = eatery2;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Long getNb_people() {
		return nb_people;
	}

	public void setNb_people(Long nb_people) {
		this.nb_people = nb_people;
	}

	public BookingReport getBookingReport() {
		return bookingReport;
	}

	public void setBookingReport(BookingReport bookingReport) {
		this.bookingReport = bookingReport;
	}

	public Booking(Long id, Eatery eatery, Customer customer, Long nb_people, BookingReport bookingReport) {
		super();
		this.id = id;
		this.eatery = eatery;
		this.customer = customer;
		this.nb_people = nb_people;
		this.bookingReport = bookingReport;
	}

	@Override
	public String toString() {
		return "Booking [id=" + id + ", eatery=" + eatery + ", customer=" + customer + ", date_time=" + dateTime
				+ ", nb_people=" + nb_people + ", bookingReport=" + bookingReport + ", \nreview=" + review + "]";
	}

	public Booking() {
		super();
	}

}
