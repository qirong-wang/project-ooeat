package data.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "practical_information")
public class PracticalInfo {
	@Id
	@GenericGenerator(name="foreignKey", strategy="foreign", parameters = @Parameter(name="property", value="eatery"))  
    @GeneratedValue(generator="foreignKey", strategy=GenerationType.IDENTITY)  
	private Long id;
	@Column(name = "hours_of_operation1")
	private String hoursOp1;
	@Column(name = "hours_of_operation2")
	private String hoursOp2;
	@Column(name = "payment_options")
	private String paymentOps;
	private String price;
	@Column(name = "getting_there")
	private String gettingThere;
	@Column(name = "parking")
	private String parking;
	 @OneToOne( cascade={CascadeType.ALL})  
	    @PrimaryKeyJoinColumn
	    private Eatery eatery;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the hoursOp1
	 */
	public String getHoursOp1() {
		return hoursOp1;
	}
	/**
	 * @param hoursOp1 the hoursOp1 to set
	 */
	public void setHoursOp1(String hoursOp1) {
		this.hoursOp1 = hoursOp1;
	}
	/**
	 * @return the hoursOp2
	 */
	public String getHoursOp2() {
		return hoursOp2;
	}
	/**
	 * @param hoursOp2 the hoursOp2 to set
	 */
	public void setHoursOp2(String hoursOp2) {
		this.hoursOp2 = hoursOp2;
	}
	/**
	 * @return the paymentOps
	 */
	public String getPaymentOps() {
		return paymentOps;
	}
	/**
	 * @param paymentOps the paymentOps to set
	 */
	public void setPaymentOps(String paymentOps) {
		this.paymentOps = paymentOps;
	}
	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * @return the gettingThere
	 */
	public String getGettingThere() {
		return gettingThere;
	}
	/**
	 * @param gettingThere the gettingThere to set
	 */
	public void setGettingThere(String gettingThere) {
		this.gettingThere = gettingThere;
	}
	/**
	 * @return the parking
	 */
	public String getParking() {
		return parking;
	}
	/**
	 * @param parking the parking to set
	 */
	public void setParking(String parking) {
		this.parking = parking;
	}
	/**
	 * @return the eatery
	 */
	public Eatery getEatery() {
		return eatery;
	}
	/**
	 * @param eatery the eatery to set
	 */
	public void setEatery(Eatery eatery) {
		this.eatery = eatery;
	}
	@Override
	public String toString() {
		return "PracticalInfo [id=" + id + ", hoursOp1=" + hoursOp1 + ", hoursOp2=" + hoursOp2 + ", paymentOps="
				+ paymentOps + ", price=" + price + ", gettingThere=" + gettingThere + ", parking=" + parking
				+ ", eatery=" + eatery + "]";
	}
	public PracticalInfo(Long id, String hoursOp1, String hoursOp2, String paymentOps, String price,
			String gettingThere, String parking, Eatery eatery) {
		super();
		this.id = id;
		this.hoursOp1 = hoursOp1;
		this.hoursOp2 = hoursOp2;
		this.paymentOps = paymentOps;
		this.price = price;
		this.gettingThere = gettingThere;
		this.parking = parking;
		this.eatery = eatery;
	}
	public PracticalInfo() {
		super();
		// TODO Auto-generated constructor stub
	} 
	
	 
	 
}
