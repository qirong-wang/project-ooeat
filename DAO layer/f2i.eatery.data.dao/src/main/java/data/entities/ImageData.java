package data.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "image_data")
public class ImageData {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private byte[] content;
	private String target;
	@Column(name = "target_id")
	private Long targetID;
	private String size;

	public ImageData(Long id, byte[] content, String target, Long targetID, String size) {
		super();
		this.id = id;
		this.content = content;
		this.target = target;
		this.targetID = targetID;
		this.size = size;
	}

	public ImageData(Long id, byte[] content) {
		super();
		this.id = id;
		this.content = content;
	}

	public ImageData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Long getTargetID() {
		return targetID;
	}

	public void setTargetID(Long targetID) {
		this.targetID = targetID;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public ImageData(Long id, String target, Long targetID, String size) {
		super();
		this.id = id;
		this.target = target;
		this.targetID = targetID;
		this.size = size;
	}

}
