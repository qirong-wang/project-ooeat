package data.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity 
	@Table(name="cooking_style")
public class CookingStyle {
	@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
			@Column(name = "id")
	private Long id;
	private String name;
	
	@OneToMany(mappedBy="cookingStyle", fetch = FetchType.LAZY)
	List<Eatery> eateries;
	
	@Override
	public String toString() {
		return "CookingStyle [id=" + id + ", name=" + name + "]";
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Eatery> getEateries() {
		return eateries;
	}
	
}
