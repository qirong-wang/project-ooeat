package data.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "region")
public class Region {
	@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
			@Column(name = "id")
	private Long id;
	
	private String name ;
	
	@OneToMany(mappedBy="region")
	List<City> cities;

	

	public Region() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Region(Long id, String name, List<City> cities) {
		super();
		this.id = id;
		this.name = name;
		this.cities = cities;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	@Override
	public String toString() {
		return "Region [id=" + id + ", name=" + name + ", cities= [...]"+ "]";
	}
	
}
