package data.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity 
	@DiscriminatorValue("C")
public class Customer extends Person {
	private static final long serialVersionUID = 3049458943934326713L;
	private String title;
	private String phone;
	@OneToMany(mappedBy = "customer", cascade=CascadeType.ALL, orphanRemoval=true)
//	@OneToMany(mappedBy = "customer")
	private List<Booking> bookings;
	
	

	
	  
	

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Customer(String username, String password,  String firstName, String lastName) {
		super(username, password, firstName, lastName);
		// TODO Auto-generated constructor stub
	}

//	public Customer(String username, String password,  String firstName, String lastName, String title,
//			String phone) {
//		super(username, password, firstName, lastName);
//		this.title = title;
//		this.phone = phone;
//	}

	public Customer(String username, String  passwordEncry,  String firstName, String lastName, String title,
			String phone, List<Booking> bookings) {
		super(username, passwordEncry, firstName, lastName);
		this.title = title;
		this.phone = phone;
		this.bookings = bookings;
	}
	public Customer(String username, String  passwordEncry, String firstName, String lastName, String title,
			String phone) {
		super(username, passwordEncry, firstName, lastName);
		this.title = title;
		this.phone = phone;
		
	}

	// getters and setters
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	public Customer(String title, String phone) {
		super();
		this.title = title;
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Customer [title=" + title + ", phone=" + phone + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", username=" + username + ", password=*****" + "]";
	}
	
	
}
