package data.interfaces;

import java.util.List;

import data.entities.ImageData;

public interface IImageData {

	public ImageData getOneImageByID(Long imageId);
//	List<ImageData> findSmallImageOfEatery (String size);

	public Long getOneSmallImageDataIDByEateryID(Long eateryID);

	public List<Long> getListPhotoIDByEateryID(Long eateryID);
}
