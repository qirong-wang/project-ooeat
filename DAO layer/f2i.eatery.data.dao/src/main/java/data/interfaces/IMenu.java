package data.interfaces;

import data.entities.Menu;

public interface IMenu {
	public Menu getOneMenuByID(Long menuID);
}
