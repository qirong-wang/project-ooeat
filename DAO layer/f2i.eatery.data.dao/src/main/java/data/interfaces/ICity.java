package data.interfaces;

import java.util.List;

import data.entities.City;

public interface ICity {
	public City getOneCityByID(Long cityID);
	public List<City> getAllCity();
	public List<City> getListCityByRegionID(Long regionID);
	public List<City> getListCityByNameLike(String cityNameLike);
	public List<City> getAllCityHaveEateryOrderByName();
	public List<City> getAllCityHaveEateryByRegionIdOrderByName(Long regionID);
}
