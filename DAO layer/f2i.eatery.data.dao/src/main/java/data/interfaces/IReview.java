package data.interfaces;

import java.util.List;
import data.entities.Review;
import data.po.ReviewInfo;

public interface IReview {

	Review saveReview(Long bookingId, Long rating, String comment);

	public List<ReviewInfo> getListReviewByEateryID(Long eateryID,Integer firstPage, Boolean withComment);

	public Integer getNbReviewsWithCommentByEateryID(Long eateryID);

	Review saveOneReview(Review review);
}
