package data.interfaces;

import java.time.LocalDateTime;
import java.util.List;
import data.entities.Booking;

public interface IBooking {

	Booking getBooking(String customerId, Long eateryId, Long np, LocalDateTime time);

	void deleteBooking(Long id);

	List<Booking> getFuturBooking(String username);

	List<Booking> getPreviousBooking(String username);

	Booking update(Long id, String username, LocalDateTime date, Long np);

	Booking saveBooking(Booking booking);

	Booking getBookingById(Long bookingId);

}
