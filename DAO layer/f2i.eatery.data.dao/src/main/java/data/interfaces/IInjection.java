package data.interfaces;

import java.io.File;
import java.util.List;

import data.entities.Customer;

public interface IInjection {

	List<Customer> injectionCustomer();

	List<Customer> injectionCustomer2(File xml, File xsd);

	Boolean injectionBMC4j();

	Boolean injectionCust();

}
