package data.interfaces;

import java.util.List;

import data.entities.CookingStyle;
import data.entities.Eatery;
import data.po.EateryInfo;
import data.po.EateryRequest;

public interface IEatery {
	public List<EateryInfo> findListEateryByMultiConditions(Long cityID, List<String> cookingStylesID, List<String> tagsID, String minPrice, String maxPrice, String minRating, Integer firstPage, Integer maxSize, String restrictionOrder); 
	public List<EateryInfo> findListEateryByMultiConditions(EateryRequest eateryRequest); 
	public Integer findNbEateryByMultiConditions(); 
	public List<String[]>  getNbCookingStyleByMultiConditions(); 
	public Integer findNbEateryByMultiConditions(EateryRequest eateryRequest);
	public Eatery getOneEateryByEateryID(Long eateryID);
	public List<CookingStyle> getListCookingStyleByMultiConditions(Long cityID);
}
