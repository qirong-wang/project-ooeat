package data.interfaces;

import data.entities.Customer;
import data.exceptions.NoUserException;
import data.exceptions.PasswordWrongException;
import data.exceptions.UserNameAlreadyExistsException;

public interface ICustomer {
	public Customer authentification(String username, String password) throws PasswordWrongException, NoUserException;

	public Customer findByUsername(String username);

	public Boolean subscribe(Customer custumer) throws UserNameAlreadyExistsException;
	public Customer updateCustomer(Customer customer);
	public void updatePassword(String username, String oldPassword, String newPassword) throws PasswordWrongException;
	public void unsubscribeCustomer(String username);

	public Customer updateUser(Customer customer);
	public Customer subscribeA(String username, String password, String firstname, String lastname, String title,
            String phone) throws UserNameAlreadyExistsException;

	public Customer save(Customer custumer) throws UserNameAlreadyExistsException;
}
