package data.interfaces;

import java.util.List;

import data.entities.Region;

public interface IRegion {
	public Region getOneRegionByID(Long regionID);
	public List<Region> getAllRegion();
	
}
