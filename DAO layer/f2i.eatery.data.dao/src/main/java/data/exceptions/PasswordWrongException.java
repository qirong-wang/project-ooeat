package data.exceptions;

public class PasswordWrongException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7906728902016906567L;

	public PasswordWrongException(String message) {
		super(message);
	}

}
