package data.exceptions;

public class NoUserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3371525823176226415L;

	public NoUserException(String message) {
		super(message);
	}

}
