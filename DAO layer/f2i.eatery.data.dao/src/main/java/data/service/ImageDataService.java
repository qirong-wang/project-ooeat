package data.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.ImageData;
import data.interfaces.IImageData;
import data.repos.ImageDataRepository;

@Service
public class ImageDataService implements IImageData {

	@Autowired
	ImageDataRepository imageDataRepository;

	@Override
	public ImageData getOneImageByID(Long imageId) {
		return imageDataRepository.findImageDataById(imageId);
	}

	@Override
	public Long getOneSmallImageDataIDByEateryID(Long eateryID) {
		return imageDataRepository.findOneSmallPhotoIDByEateryID(eateryID).get();
	}
	@Override
	public List<Long> getListPhotoIDByEateryID(Long eateryID) {
		return imageDataRepository.findListPhotoIDByEateryID(eateryID).get();
	}
}
