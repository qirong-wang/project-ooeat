package data.service;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.Booking;
import data.entities.Customer;
import data.entities.Eatery;
import data.interfaces.IBooking;
import data.interfaces.ICustomer;
import data.interfaces.IEatery;
import data.interfaces.IUnsubCustomer;
import data.repos.IbookingRepository;

@Service
public class BookingService implements IBooking {
	@Autowired
	ICustomer service;
	@Autowired
	IUnsubCustomer serviceUnsub;
	@Autowired
	IEatery serviceEatery;
	@Autowired
	IbookingRepository serviceBookingRepo;
	private static final Logger log = LogManager.getLogger(BookingService.class);

	@Override
	public void deleteBooking(Long id) {
		serviceBookingRepo.deleteById(id);
	}

	@Override
	public Booking getBooking(String customerId, Long eateryId, Long np, LocalDateTime time) {
		log.info("save Start .......");
		Customer customer = service.findByUsername(customerId);
		Eatery eatery = serviceEatery.getOneEateryByEateryID(eateryId);
		
		Booking booking = new Booking();
		booking.setCustomer(customer);
		booking.setEatery(eatery);
		booking.setNb_people(np);
		booking.setDate_time(time);
		
		Booking book = serviceBookingRepo.save(booking);

		return book;
	}

	@Override
	public List<Booking> getFuturBooking(String username) {
		log.info("Get All futur boking List.......");
		Customer customer = service.findByUsername(username);
		LocalDateTime time = LocalDateTime.now();
		List<Booking> book = serviceBookingRepo.findByCustomerAndDateTimeAfterOrderByDateTimeAsc(customer, time);
		log.info("List of your Next Booking" + book.size());
//		book.forEach(e -> log.info(
//				e.getId() + "    " + e.getCustomer().getUsername() + "  " + e.getNb_people() + " " + e.getDate_time()));
		return book;
	}

	@Override
	public List<Booking> getPreviousBooking(String username) {
		log.info("Get All previous boking List.......");
		Customer customer = service.findByUsername(username);
		LocalDateTime time = LocalDateTime.now();
		List<Booking> book = serviceBookingRepo.findByCustomerAndDateTimeBeforeOrderByDateTimeDesc(customer, time);
		log.info("List of your Next Booking" + book.size());
//		book.forEach(e -> log.info(
//				e.getId() + "    " +e.getEatery().getName() + "    "+ e.getCustomer().getUsername() + "  " + e.getNb_people() + " " + e.getDate_time()));
		return book;
	}

	@Override
	public Booking update(Long id, String username, LocalDateTime date, Long np) {
		log.info("update booking......");
		Customer customer = service.findByUsername(username);
		Booking book = serviceBookingRepo.findById(id).get();
		Eatery eatery = book.getEatery();
		book = new Booking(eatery, customer, date, np);
		Booking booking = serviceBookingRepo.save(book);
		return booking;
	}
	@Override
	public Booking saveBooking(Booking booking) {
		log.info("save booking......");
		Booking save =null;
		try {
			save = serviceBookingRepo.save(booking);
			System.out.println(save);
		} catch (Exception e) {
			log.error("ERROR while saving booking");
		}
		return save;
	}
	@Override
	public Booking getBookingById(Long bookingId) {
		log.info("getBookingById call......");
		return serviceBookingRepo.findById(bookingId).get();
	}

}
