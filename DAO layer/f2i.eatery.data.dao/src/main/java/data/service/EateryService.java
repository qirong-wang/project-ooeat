package data.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.CookingStyle;
import data.entities.Eatery;
import data.interfaces.IEatery;
import data.po.EateryInfo;
import data.po.EateryRequest;
import data.repos.EateryRepository;
import data.tools.StringTool;

@Service
public class EateryService implements IEatery {
//	private static final Logger log = LogManager.getLogger(EateryService.class);
	@Autowired
	EateryRepository eateryRepository;
//	private List<EateryInfo> list;



	@Override
	public List<EateryInfo> findListEateryByMultiConditions(EateryRequest eateryRequest) {
		return eateryRepository.findListEateryByMultiConditions(eateryRequest).get();
	}

	@Override
	public Integer findNbEateryByMultiConditions(EateryRequest eateryRequest) {
		return eateryRepository.findNbEateryByMultiConditions(eateryRequest).get();
	}

	@Override
	public List<EateryInfo> findListEateryByMultiConditions(Long cityID, List<String> cookingStylesID,
			List<String> tagsID, String minPrice, String maxPrice, String minRating, Integer firstResult,
			Integer maxSize, String orderBy) {

		String restrictionCityID = "", restrictionCookingStylesID = "", restrictionTagsID = "", restrictionPrice = "",
				restrictionRating = "", restrictionOrder = "";

		if (cityID != null)
			restrictionCityID = " And c.id = " + cityID;

		if (cookingStylesID != null && cookingStylesID.size() > 0)
			restrictionCookingStylesID = " And s.id in (" + StringTool.listConversion(cookingStylesID) + ")";

		if (minPrice != null && minPrice.length() > 0 && maxPrice != null && maxPrice.length() > 0)
			restrictionPrice = " And p.price between " + minPrice + " and " + maxPrice;
		if (tagsID != null && tagsID.size() > 0)
			restrictionTagsID = " And t.id in (" + StringTool.listConversion(tagsID) + ")";
		if (minRating != null && minRating.length() > 0)
			restrictionRating = " having avg(r.rating)> " + minRating;
		if (firstResult == null)
			firstResult = 0;

		if (maxSize == null)
			maxSize = 3000;
		if (orderBy != null && orderBy.length() > 0)
			restrictionOrder = " order by " + orderBy;
		List<EateryInfo> list = eateryRepository.findListEateryByMultiConditions(
				restrictionCityID + restrictionCookingStylesID + restrictionPrice + restrictionTagsID,
				restrictionRating, firstResult, maxSize, restrictionOrder).get();
		return list;
	}

	@Override
	public Integer findNbEateryByMultiConditions() {
		return eateryRepository.findNbEateryByMultiConditions().get();
	}

	@Override
	public List<String[]> getNbCookingStyleByMultiConditions() {
		return eateryRepository.findNbCookingStyleByMultiConditions().get();
	}

	@Override
	public Eatery getOneEateryByEateryID(Long eateryID) {
		return eateryRepository.findById(eateryID).get();
	}

	@Override
	public List<CookingStyle> getListCookingStyleByMultiConditions(Long cityID) {
		return eateryRepository.findListCookingStyleByMultiConditions(cityID).get();
	}


}
