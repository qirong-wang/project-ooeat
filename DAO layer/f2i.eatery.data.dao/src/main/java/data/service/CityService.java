package data.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.City;
import data.interfaces.ICity;
import data.repos.CityRepository;
@Service
public class CityService implements ICity {
	@Autowired
	CityRepository cityRepository;
	
	@Override
	public City getOneCityByID(Long cityID) {
		return cityRepository.findById(cityID).get();
	}

	@Override
	public List<City> getAllCity() {
		return cityRepository.findAllOrderByName();
	}
	@Override
	public List<City> getAllCityHaveEateryOrderByName() {
		return cityRepository.findAllCityHaveEateryOrderByName();
	}
	@Override
	public List<City> getAllCityHaveEateryByRegionIdOrderByName(Long regionID) {
		return cityRepository.findAllCityHaveEateryByRegionIdOrderByName(regionID);
	}

	@Override
	public List<City> getListCityByRegionID(Long regionID) {
		return cityRepository.findAllCityByRegionID(regionID);
	}

	@Override
	public List<City> getListCityByNameLike(String cityNameLike) {
		return cityRepository.findByNameLikeIgnoreCaseOrderByName("%"+cityNameLike+"%");
	}
}
