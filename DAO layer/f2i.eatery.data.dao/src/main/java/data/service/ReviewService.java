package data.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.Booking;
import data.entities.Review;
import data.interfaces.IReview;
import data.po.ReviewInfo;
import data.repos.IbookingRepository;
import data.repos.ReviewRepository;
@Service
public class ReviewService implements IReview {
	private final static Logger log = LogManager.getLogger(ReviewService.class);
	@Autowired
	ReviewRepository reviewRepository;
	@Autowired
	IbookingRepository serviceBookRepo;

	@Override
	public Integer getNbReviewsWithCommentByEateryID(Long eateryID){
		return reviewRepository.findNbReviewsWithCommentByEateryID(eateryID).get();
	}
	@Override
	public List<ReviewInfo> getListReviewByEateryID(Long eateryID,Integer firstPage, Boolean withComment) {
		return reviewRepository.findListReviewByEateryID(eateryID,firstPage,withComment).get();
	}
	@Override
	public Review saveReview(Long bookingId, Long rating, String comment) {
		log.info("save Review......");
		Booking booking = serviceBookRepo.findById(bookingId).get();
		Review review = reviewRepository.save(new Review(booking, rating, comment));
		return review;
	}
	@Override
	public Review saveOneReview(Review review) {
		log.info("save Review......");
		try {
			reviewRepository.save(review);
		} catch (Exception e) {
			log.error("ERROR while saving review...");
		}
		return review;
	}
}
