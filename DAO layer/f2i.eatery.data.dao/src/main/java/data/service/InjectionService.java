package data.service;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.Booking;
import data.entities.Customer;
import data.entities.Eatery;
import data.entities.Review;
import data.exceptions.UserNameAlreadyExistsException;
import data.interfaces.IBooking;
import data.interfaces.ICustomer;
import data.interfaces.IEatery;
import data.interfaces.IInjection;
import data.interfaces.IReview;
import data.repos.CustomerRepository;
import f2i.xml.injection.handler.CustomerSAXHandler;
import f2i.xml.injection.parser.ToolSAXParserXml;
@Service
public class InjectionService implements IInjection {
	private static final Logger log = LogManager.getLogger(InjectionService.class);

	@Autowired
	CustomerRepository customerService;
	@Autowired
	ICustomer service;
	@Autowired
	IEatery eateryService;
	@Autowired
	IBooking bookingService;
	@Autowired 
	IReview reviewService; 
	@Override
	public List<Customer> injectionCustomer() {
		ToolSAXParserXml<Customer, CustomerSAXHandler> parser =new ToolSAXParserXml<Customer, CustomerSAXHandler>();
//		SAXParserXml.parse("resources/customer.xml", "resources/customer.xsd");
//		StreamSource xml=new StreamSource(new File("src/main/resources/customer.xml"));
//		StreamSource xsd=new StreamSource(new File("src/main/resources/customer.xsd"));
//		StreamSource xml=new StreamSource(new File("src/main/resources/Customer.xml"));
//		StreamSource xsd=new StreamSource(new File("src/main/resources/Customer.xsd"));
//		StreamSource xml=new StreamSource(new File("resources/Customer.xml"));
//		StreamSource xsd=new StreamSource(new File("resources/Customer.xsd"));
		
		ClassLoader classLoader = getClass().getClassLoader();

        URL xml = classLoader.getResource("Customer.xml");
        URL xsd = classLoader.getResource("Customer.xml");
        if (xml == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            System.out.println("OK");
        }
		
//		File xml = new File(getClass().getResource("src/main/resources/Customer.xml").getFile());
//		File xsd = new File(getClass().getResource("src/main/resources/Customer.xsd").getFile());
//		StreamSource xml=new StreamSource(new File("Customer.xml"));
//		StreamSource xsd=new StreamSource(new File("Customer.xsd"));
//		new StreamSource(new File("resources/customer.xml");
//		List<Customer> listCustomers = parser.parse("Customer.xml", "Customer.xsd", CustomerSAXHandler.class);
		List<Customer> listCustomers = parser.parse(new File(xml.getFile()), new File(xsd.getFile()), CustomerSAXHandler.class);
//		List<Customer> listCustomers = parser.parse(xml, xsd, CustomerSAXHandler.class);
//		Booking booking=new Booking();
		Random r= new Random();
		Eatery eateryParis = eateryService.getOneEateryByEateryID(1l);
		
		String[] comments= {"Comentaire du Client...........",null,"Tres bon merci...","","Les plats étaient excellents....","","","","",null,null,null,"Vraiment super bon....","C'était super bon mon repas. je recommande","Bien...","Au top comme d'habitude...","Excellent, je recommande ce restaurant sans hésitation.","Bonne restaurant...","Excellente cuisine..","Un vrais délice..","au top :)","Super :)","Un peu juste en quantité...","Très contente de ma commande...","Très bien très bon" };
//		Review review = new Review();
//		listCustomers.forEach(e->System.out.println(e));
			for(Customer c: listCustomers) {
				try { 
					Customer save = service.save(c);
					for(int i =0; i<3;i++) {
						Booking booking=new Booking();
						booking.setDate_time(getTime());
						booking.setEatery(eateryParis);
						booking.setNb_people((long)r.nextInt(8)+1);
						booking.setCustomer(save);
						Booking saveBooking = bookingService.saveBooking(booking);
						Review review = new Review();
						review.setComment(comments[r.nextInt(comments.length)]);
						review.setRating(Long.valueOf(r.nextInt(5)+1));
						review.setBooking(saveBooking);
						reviewService.saveOneReview(review);
//						System.out.println(saveOneReview);
//						System.out.println("-----");
					}
					
				} catch (UserNameAlreadyExistsException e1) {
					System.out.println(e1.getLocalizedMessage());
				}
			}
		
		return listCustomers;
		
	}
	@Override
	public List<Customer> injectionCustomer2(File xml,File xsd) {
		ToolSAXParserXml<Customer, CustomerSAXHandler> parser =new ToolSAXParserXml<Customer, CustomerSAXHandler>();
		long[] eateryID = { 1l, 2l, 3l, 4l, 5l, 6l, 7l, 8l, 9l, 10l, 11l, 13l, 14l, 15l, 163l, 295l, 296l, 297l, 298l, 299l, 300l, 302l, 484l, 485l, 487l, 488l, 490l, 491l, 492l, 493l, 494l, 495l, 496l, 497l, 498l, 500l, 501l, 502l, 503l, 504l, 505l, 506l, 507l, 508l, 509l, 510l, 511l, 512l, 514l, 515l, 516l, 517l, 518l, 519l, 730l, 731l, 732l, 733l, 734l, 735l, 736l, 737l, 739l, 740l, 741l, 742l, 743l, 744l, 746l, 747l, 748l, 749l, 750l, 751l, 752l, 753l, 754l, 755l, 756l, 757l, 758l, 759l, 760l, 761l, 762l, 763l, 764l, 765l, 766l, 767l, 768l, 769l, 770l, 771l, 772l, 773l, 774l, 775l, 844l, 845l, 846l, 847l, 848l, 849l, 850l, 851l, 852l, 853l, 854l, 855l, 856l, 857l, 858l, 859l, 860l, 861l, 862l, 863l, 864l, 865l, 1150l, 1151l, 1152l, 1153l, 1154l, 1155l, 1156l, 1157l, 1158l, 1159l, 1160l, 1161l, 1162l, 1163l, 1164l, 1165l, 1166l, 1167l, 1168l, 1169l, 1170l, 1171l, 1173l, 1174l, 1175l, 1176l, 1177l, 1178l, 1179l, 1180l, 1181l, 1182l, 1183l, 1184l, 1185l, 1186l, 1187l, 1188l, 1189l, 1190l, 1587l, 1588l, 1589l, 1590l, 1591l, 1592l, 1593l, 1594l, 1595l, 1596l, 1597l, 1598l, 1599l, 1600l, 1601l, 1602l, 1603l, 1604l, 1605l, 1606l, 1607l, 1608l, 1609l, 1610l, 1611l, 1612l, 1613l, 1614l, 1615l, 1616l, 1617l, 1618l, 1619l, 1620l, 1621l, 1622l, 1623l, 1624l, 1625l, 1626l, 1627l, 1628l, 1629l, 1630l, 1631l, 1632l, 1633l, 1634l, 1635l, 1636l, 1637l, 1638l, 1639l, 1640l, 1641l, 1642l, 1643l, 1644l, 1645l, 1646l, 1647l, 1648l, 1649l, 1980l, 1981l, 1982l, 1983l, 1984l, 1985l, 1986l, 1987l, 1988l, 1989l, 1990l, 1991l, 1992l, 1993l, 1994l, 1995l, 1996l, 1997l, 1998l, 1999l, 2000l, 2001l, 2002l, 2003l, 2004l, 2005l, 2006l, 2008l, 2009l, 2010l, 2011l, 2012l, 2013l, 2014l, 2015l, 2016l, 2017l, 2018l, 2019l, 2020l, 2021l, 2022l, 2023l, 2024l, 2025l, 2026l, 2027l, 2028l, 2029l, 2030l, 2031l, 2032l, 2033l, 2034l, 2035l, 2036l, 2037l, 2038l, 2039l, 2040l, 2041l, 2042l, 2043l, 2044l, 2045l, 2046l, 2047l, 2048l, 2049l, 2050l, 2051l, 2052l, 2053l, 2054l, 2055l, 2056l, 2057l, 2058l, 2059l, 2060l, 2061l, 2062l, 2063l, 2358l, 2359l, 2360l, 2361l, 2362l, 2363l, 2364l, 2365l, 2366l, 2367l, 2368l, 2369l, 2370l, 2371l, 2372l, 2373l, 2374l, 2375l, 2376l, 2377l, 2378l, 2379l, 2380l, 2381l, 2382l, 2383l, 2384l, 2385l, 2386l, 2387l, 2388l, 2389l, 2390l, 2391l, 2392l, 2393l, 2394l, 2395l, 2396l, 2397l, 2398l, 2399l, 2400l, 2401l, 2402l, 2403l, 2404l, 2405l, 2406l, 2408l, 2409l, 2411l, 2412l, 2413l, 2414l, 2425l, 2426l, 2427l, 2428l, 2429l, 2430l, 2431l, 2432l, 2433l, 2434l, 2435l, 2436l, 2438l, 2439l, 2440l, 2441l, 2442l, 2443l, 2444l, 2445l, 2446l, 2447l, 2448l, 2449l, 2450l, 2451l, 2452l, 2453l, 2454l, 2455l, 2456l, 2457l, 2458l, 2459l, 2460l, 2461l, 2462l, 2463l, 2464l, 2465l, 2628l, 2629l, 2630l, 2631l, 2632l, 2633l, 2634l, 2635l, 2636l, 2637l, 2638l, 2639l, 2640l, 2641l, 2642l, 2643l, 2644l, 2645l, 2646l, 2647l, 2648l, 2649l, 2650l, 2651l, 2652l, 2653l, 2654l, 2655l, 2656l, 2657l, 2658l, 2659l, 2660l, 2661l, 2662l, 2663l, 2664l, 2665l, 2666l, 2667l, 2668l, 2669l, 2670l, 2671l, 2672l, 2673l, 2674l, 2675l, 2676l, 2677l, 2678l, 2679l, 2680l, 2681l, 2682l, 2683l, 2684l, 2685l, 2686l, 2687l, 2688l, 2689l, 2690l, 2691l, 2692l, 2693l, 2694l, 2695l, 2696l, 2697l, 2698l, 2699l, 2700l, 2701l, 2702l, 2703l, 2704l, 2705l, 2706l, 2707l, 2708l, 2709l, 2710l, 2711l, 2712l, 2713l, 2714l, 2715l, 2716l, 2717l, 2718l, 2719l, 2720l, 2721l, 2722l, 2723l, 2724l, 2725l, 2726l, 2727l, 2728l, 2729l, 2730l, 2731l, 2732l, 2733l, 2734l, 2735l, 2736l, 2737l, 2738l, 2739l, 2740l, 2741l, 2742l, 2743l, 2744l, 2745l, 2746l, 2747l, 2748l, 2749l, 2750l, 2751l, 2752l, 2753l, 2754l, 2755l, 2812l, 2813l, 2814l, 2815l, 2816l, 2817l, 2818l, 2819l, 2820l, 2821l, 2822l, 2823l, 2824l, 2825l, 2826l, 2827l, 2828l, 2829l, 2830l, 2831l, 2832l, 2833l, 2834l, 2835l, 2836l, 2837l, 2838l, 2839l, 2840l, 2841l, 2842l, 2843l, 2844l, 2845l, 2846l, 2847l, 2848l, 2849l, 2851l, 2852l, 2853l, 2854l, 2855l, 2856l, 2857l, 2858l, 2859l, 2860l, 2861l, 2862l, 2863l, 2864l, 2865l, 2866l, 2867l, 2868l, 2869l, 2870l, 2871l, 2892l, 2893l, 2894l, 2895l, 2896l, 2897l, 2898l, 2899l, 2900l, 2902l, 2903l, 2904l, 2905l, 2906l, 2907l, 2908l, 2909l, 2910l, 2911l, 2912l };
//		long[] eateryID = { 31l, 32l, 33l, 34l, 35l, 160l, 161l, 162l, 390l, 391l, 392l, 393l, 394l, 395l, 396l, 397l, 398l, 399l, 400l, 401l, 402l, 403l, 404l, 405l, 406l, 407l, 408l, 409l, 609l, 611l, 612l, 613l, 614l, 615l, 616l, 617l, 618l, 619l, 620l, 621l, 622l, 623l, 624l, 625l, 626l, 627l, 628l, 629l, 630l, 631l, 632l, 633l, 634l, 635l, 636l, 637l, 638l, 639l, 640l, 641l, 642l, 643l, 644l, 645l, 646l, 960l, 961l, 962l, 963l, 964l, 965l, 966l, 967l, 968l, 969l, 970l, 971l, 972l, 973l, 974l, 975l, 976l, 977l, 978l, 979l, 980l, 981l, 982l, 983l, 984l, 985l, 986l, 987l, 988l, 990l, 991l, 993l, 994l, 995l, 996l, 997l, 998l, 999l, 1000l, 1001l, 1002l, 1003l, 1004l, 1005l, 1006l, 1007l, 1008l, 1009l, 1010l, 1011l, 1012l, 1013l, 1014l, 1015l, 1016l, 1017l, 1018l, 1019l, 1020l, 1388l, 1389l, 1390l, 1391l, 1392l, 1393l, 1394l, 1395l, 1396l, 1398l, 1399l, 1400l, 1401l, 1402l, 1403l, 1404l, 1405l, 1406l, 1407l, 1408l, 1409l, 1410l, 1411l, 1412l, 1413l, 1414l, 1415l, 1416l, 1417l, 1418l, 1419l, 1420l, 1421l, 1422l, 1423l, 1424l, 1425l, 1426l, 1428l, 1429l, 1793l, 1794l, 1795l, 1796l, 1797l, 1798l, 1799l, 1800l, 1801l, 1802l, 1803l, 1804l, 1805l, 1806l, 1807l, 1808l, 1809l, 1810l, 1811l, 1812l, 1813l, 1814l, 1815l, 1816l, 1817l, 1818l, 1819l, 1820l, 1821l, 1822l, 1823l, 1824l, 1825l, 1826l, 1827l, 1828l, 1829l, 1830l, 1831l, 1832l, 1833l, 1834l, 1835l, 1836l, 1837l, 1839l, 1840l, 1841l, 1842l, 1843l, 1844l, 1845l, 1846l, 1847l, 1848l, 1849l, 1851l, 1852l, 1853l, 1854l, 1855l, 1856l, 2194l, 2195l, 2196l, 2197l, 2198l, 2199l, 2200l, 2201l, 2202l, 2203l, 2204l, 2205l, 2206l, 2207l, 2208l, 2209l, 2210l, 2211l, 2212l, 2213l, 2214l, 2215l, 2216l, 2217l, 2218l, 2219l, 2220l, 2221l, 2222l, 2223l, 2224l, 2225l, 2226l, 2227l, 2229l, 2230l, 2231l, 2232l, 2233l, 2234l, 2235l, 2236l, 2237l, 2238l, 2239l, 2240l, 2241l, 2242l, 2243l, 2244l, 2245l, 2246l, 2247l, 2248l, 2249l, 2250l, 2251l, 2252l, 2253l, 2254l, 2255l, 2256l, 2257l, 2258l, 2259l, 2260l, 2261l, 2262l, 2475l, 2476l, 2477l, 2478l, 2479l, 2480l, 2481l, 2482l, 2483l, 2484l, 2485l, 2486l, 2487l, 2488l, 2489l, 2490l, 2491l, 2492l, 2493l, 2494l, 2495l, 2496l, 2497l, 2498l, 2499l, 2500l, 2501l, 2502l, 2503l, 2504l, 2505l, 2506l, 2507l, 2508l, 2509l, 2510l, 2511l, 2512l, 2513l, 2514l, 2515l, 2516l, 2517l, 2518l, 2519l, 2520l, 2521l, 2522l, 2523l, 2524l, 2525l, 2526l, 2527l, 2528l, 2529l, 2530l, 2531l, 2532l, 2533l, 2534l, 2535l, 2536l, 2537l, 2538l, 2539l, 2540l, 2541l, 2542l, 2543l, 2544l, 2545l, 2546l, 2547l, 2548l, 2549l, 2550l, 2551l, 2552l, 2553l, 2554l, 2555l, 2556l, 2557l, 2558l, 2559l, 2560l, 2561l, 2756l, 2757l, 2758l, 2759l, 2760l, 2761l, 2762l, 2763l, 2764l, 2765l, 2766l, 2767l, 2768l, 2769l, 2770l, 2771l, 2772l, 2773l, 2774l, 2775l, 2776l, 2777l, 2778l, 2779l, 2780l, 2781l, 2782l, 2783l, 2784l, 2785l, 2786l, 2787l, 2788l, 2789l, 2790l, 2791l, 2792l, 2793l, 2794l, 2795l, 2796l, 2797l, 2798l, 2799l, 2800l, 2801l, 2802l, 2803l, 2804l, 2805l, 2806l, 2807l, 2872l, 2873l, 2874l, 2875l, 2876l, 2877l, 2878l, 2879l, 2880l, 2881l, 2882l, 2883l, 2884l, 2885l, 2886l, 2887l, 2888l, 2889l, 2890l, 2891l, 2913l, 2914l, 2915l, 2916l, 2917l, 2918l, 2919l, 2920l, 2921l, 2922l, 2923l, 2924l, 2925l, 2926l, 2927l, 2928l, 2929l, 2930l, 2931l, 2932l, 2933l, 2934l, 2935l, 2936l, 2937l, 2938l, 2939l, 2940l, 2941l, 2942l, 2943l, 2944l, 2945l, 2946l };
		List<Customer> listCustomers = parser.parse(xml, xsd, CustomerSAXHandler.class);
		Random r= new Random();
		
//		Eatery eateryParis = eateryService.getOneEateryByEateryID(1l); 
		
		String[] comments= {"Comentaire du Client...........",null,"Tres bon merci...","","Les plats étaient excellents....","","","","",null,null,null,"Vraiment super bon....","C'était super bon mon repas. je recommande","Bien...","Au top comme d'habitude...","Excellent, je recommande ce restaurant sans hésitation.","Bonne restaurant...","Excellente cuisine..","Un vrais délice..","au top :)","Super :)","Un peu juste en quantité...","Très contente de ma commande...","Très bien très bon" };
		for(Customer c: listCustomers) {
			try { 
				Customer save = service.save(c);
				for(int i =0; i<30;i++) {
					Booking booking=new Booking();
					booking.setDate_time(getTime());
					long eId =eateryID[r.nextInt(eateryID.length)];
					Eatery oneEateryByEateryID = eateryService.getOneEateryByEateryID(eId);
					booking.setEatery(oneEateryByEateryID);
					booking.setNb_people((long)r.nextInt(8)+1);
					booking.setCustomer(save);
					Booking saveBooking = bookingService.saveBooking(booking);
					Review review = new Review();
					review.setComment(comments[r.nextInt(comments.length)]);
					review.setRating(Long.valueOf(r.nextInt(5)+1));
					review.setBooking(saveBooking);
					reviewService.saveOneReview(review);
				}
				
			} catch (UserNameAlreadyExistsException e1) {
				System.out.println(e1.getLocalizedMessage());
			}
			
			
		}
		return listCustomers;
		
	}
	@Override
	public Boolean injectionBMC4j() {
//		ToolSAXParserXml<Customer, CustomerSAXHandler> parser =new ToolSAXParserXml<Customer, CustomerSAXHandler>();
		Random r= new Random();
		long[] eateryID = { 1l, 2l, 3l, 4l, 5l, 6l, 7l, 8l, 9l, 10l, 11l, 13l, 14l, 15l, 163l, 295l, 296l, 297l, 298l, 299l, 300l, 302l, 484l, 485l, 487l, 488l, 490l, 491l, 492l, 493l, 494l, 495l, 496l, 497l, 498l, 500l, 501l, 502l, 503l, 504l, 505l, 506l, 507l, 508l, 509l, 510l, 511l, 512l, 514l, 515l, 516l, 517l, 518l, 519l, 730l, 731l, 732l, 733l, 734l, 735l, 736l, 737l, 739l, 740l, 741l, 742l, 743l, 744l, 746l, 747l, 748l, 749l, 750l, 751l, 752l, 753l, 754l, 755l, 756l, 757l, 758l, 759l, 760l, 761l, 762l, 763l, 764l, 765l, 766l, 767l, 768l, 769l, 770l, 771l, 772l, 773l, 774l, 775l, 844l, 845l, 846l, 847l, 848l, 849l, 850l, 851l, 852l, 853l, 854l, 855l, 856l, 857l, 858l, 859l, 860l, 861l, 862l, 863l, 864l, 865l, 1150l, 1151l, 1152l, 1153l, 1154l, 1155l, 1156l, 1157l, 1158l, 1159l, 1160l, 1161l, 1162l, 1163l, 1164l, 1165l, 1166l, 1167l, 1168l, 1169l, 1170l, 1171l, 1173l, 1174l, 1175l, 1176l, 1177l, 1178l, 1179l, 1180l, 1181l, 1182l, 1183l, 1184l, 1185l, 1186l, 1187l, 1188l, 1189l, 1190l, 1587l, 1588l, 1589l, 1590l, 1591l, 1592l, 1593l, 1594l, 1595l, 1596l, 1597l, 1598l, 1599l, 1600l, 1601l, 1602l, 1603l, 1604l, 1605l, 1606l, 1607l, 1608l, 1609l, 1610l, 1611l, 1612l, 1613l, 1614l, 1615l, 1616l, 1617l, 1618l, 1619l, 1620l, 1621l, 1622l, 1623l, 1624l, 1625l, 1626l, 1627l, 1628l, 1629l, 1630l, 1631l, 1632l, 1633l, 1634l, 1635l, 1636l, 1637l, 1638l, 1639l, 1640l, 1641l, 1642l, 1643l, 1644l, 1645l, 1646l, 1647l, 1648l, 1649l, 1980l, 1981l, 1982l, 1983l, 1984l, 1985l, 1986l, 1987l, 1988l, 1989l, 1990l, 1991l, 1992l, 1993l, 1994l, 1995l, 1996l, 1997l, 1998l, 1999l, 2000l, 2001l, 2002l, 2003l, 2004l, 2005l, 2006l, 2008l, 2009l, 2010l, 2011l, 2012l, 2013l, 2014l, 2015l, 2016l, 2017l, 2018l, 2019l, 2020l, 2021l, 2022l, 2023l, 2024l, 2025l, 2026l, 2027l, 2028l, 2029l, 2030l, 2031l, 2032l, 2033l, 2034l, 2035l, 2036l, 2037l, 2038l, 2039l, 2040l, 2041l, 2042l, 2043l, 2044l, 2045l, 2046l, 2047l, 2048l, 2049l, 2050l, 2051l, 2052l, 2053l, 2054l, 2055l, 2056l, 2057l, 2058l, 2059l, 2060l, 2061l, 2062l, 2063l, 2358l, 2359l, 2360l, 2361l, 2362l, 2363l, 2364l, 2365l, 2366l, 2367l, 2368l, 2369l, 2370l, 2371l, 2372l, 2373l, 2374l, 2375l, 2376l, 2377l, 2378l, 2379l, 2380l, 2381l, 2382l, 2383l, 2384l, 2385l, 2386l, 2387l, 2388l, 2389l, 2390l, 2391l, 2392l, 2393l, 2394l, 2395l, 2396l, 2397l, 2398l, 2399l, 2400l, 2401l, 2402l, 2403l, 2404l, 2405l, 2406l, 2408l, 2409l, 2411l, 2412l, 2413l, 2414l, 2425l, 2426l, 2427l, 2428l, 2429l, 2430l, 2431l, 2432l, 2433l, 2434l, 2435l, 2436l, 2438l, 2439l, 2440l, 2441l, 2442l, 2443l, 2444l, 2445l, 2446l, 2447l, 2448l, 2449l, 2450l, 2451l, 2452l, 2453l, 2454l, 2455l, 2456l, 2457l, 2458l, 2459l, 2460l, 2461l, 2462l, 2463l, 2464l, 2465l, 2628l, 2629l, 2630l, 2631l, 2632l, 2633l, 2634l, 2635l, 2636l, 2637l, 2638l, 2639l, 2640l, 2641l, 2642l, 2643l, 2644l, 2645l, 2646l, 2647l, 2648l, 2649l, 2650l, 2651l, 2652l, 2653l, 2654l, 2655l, 2656l, 2657l, 2658l, 2659l, 2660l, 2661l, 2662l, 2663l, 2664l, 2665l, 2666l, 2667l, 2668l, 2669l, 2670l, 2671l, 2672l, 2673l, 2674l, 2675l, 2676l, 2677l, 2678l, 2679l, 2680l, 2681l, 2682l, 2683l, 2684l, 2685l, 2686l, 2687l, 2688l, 2689l, 2690l, 2691l, 2692l, 2693l, 2694l, 2695l, 2696l, 2697l, 2698l, 2699l, 2700l, 2701l, 2702l, 2703l, 2704l, 2705l, 2706l, 2707l, 2708l, 2709l, 2710l, 2711l, 2712l, 2713l, 2714l, 2715l, 2716l, 2717l, 2718l, 2719l, 2720l, 2721l, 2722l, 2723l, 2724l, 2725l, 2726l, 2727l, 2728l, 2729l, 2730l, 2731l, 2732l, 2733l, 2734l, 2735l, 2736l, 2737l, 2738l, 2739l, 2740l, 2741l, 2742l, 2743l, 2744l, 2745l, 2746l, 2747l, 2748l, 2749l, 2750l, 2751l, 2752l, 2753l, 2754l, 2755l, 2812l, 2813l, 2814l, 2815l, 2816l, 2817l, 2818l, 2819l, 2820l, 2821l, 2822l, 2823l, 2824l, 2825l, 2826l, 2827l, 2828l, 2829l, 2830l, 2831l, 2832l, 2833l, 2834l, 2835l, 2836l, 2837l, 2838l, 2839l, 2840l, 2841l, 2842l, 2843l, 2844l, 2845l, 2846l, 2847l, 2848l, 2849l, 2851l, 2852l, 2853l, 2854l, 2855l, 2856l, 2857l, 2858l, 2859l, 2860l, 2861l, 2862l, 2863l, 2864l, 2865l, 2866l, 2867l, 2868l, 2869l, 2870l, 2871l, 2892l, 2893l, 2894l, 2895l, 2896l, 2897l, 2898l, 2899l, 2900l, 2902l, 2903l, 2904l, 2905l, 2906l, 2907l, 2908l, 2909l, 2910l, 2911l, 2912l };
		String[] comments= {"Comentaire du Client...........",null,"Tres bon merci...","","Les plats étaient excellents....","","","","",null,null,null,"Vraiment super bon....","C'était super bon mon repas. je recommande","Bien...","Au top comme d'habitude...","Excellent, je recommande ce restaurant sans hésitation.","Bonne restaurant...","Excellente cuisine..","Un vrais délice..","au top :)","Super :)","Un peu juste en quantité...","Très contente de ma commande...","Très bien très bon" };
		Customer bmc =null;
		try {
			bmc = service.subscribeA("BMC4J@hotmail.com", "BMC4J@hotmail.com", "bmc", "BMC4J", "Mr.", "0123456789");
		} catch (UserNameAlreadyExistsException e) {
			log.error("Error while saving bmc4j");
			bmc=service.findByUsername("BMC4J@hotmail.com");
		}
		for(int i =0; i<10; i++) {
			Booking booking=new Booking();
			booking.setCustomer(bmc);
			long eId =eateryID[r.nextInt(eateryID.length)];
			Eatery oneEateryByEateryID = eateryService.getOneEateryByEateryID(eId);
			booking.setEatery(oneEateryByEateryID);
			booking.setDate_time(getTime());
			booking.setNb_people((long)r.nextInt(8)+1);
			Booking saveBooking = bookingService.saveBooking(booking);
			Review review = new Review();
			review.setComment(comments[r.nextInt(comments.length)]);
			review.setRating(Long.valueOf(r.nextInt(5)+1));
			review.setBooking(saveBooking);
			reviewService.saveOneReview(review);
		}
		
		for(int i =0; i<5; i++) {
		Booking booking=new Booking();
		booking.setCustomer(bmc);
		long eId =eateryID[r.nextInt(eateryID.length)];
		Eatery oneEateryByEateryID = eateryService.getOneEateryByEateryID(eId);
		booking.setEatery(oneEateryByEateryID);
		booking.setDate_time(getTime2());
		booking.setNb_people((long)r.nextInt(8)+1);
		Booking saveBooking = bookingService.saveBooking(booking);
		Review review = new Review();
		review.setComment(comments[r.nextInt(comments.length)]);
		review.setRating(Long.valueOf(r.nextInt(5)+1));
		review.setBooking(saveBooking);
		reviewService.saveOneReview(review);
		}
		return true;
		
	}
	@Override
	public Boolean injectionCust() {
//		ToolSAXParserXml<Customer, CustomerSAXHandler> parser =new ToolSAXParserXml<Customer, CustomerSAXHandler>();
		Random r= new Random();
//		long[] eateryID = { 1l, 2l, 3l, 4l, 5l, 6l, 7l, 8l, 9l, 10l, 11l, 13l, 14l, 15l, 163l, 295l, 296l, 297l, 298l, 299l, 300l, 302l, 484l, 485l, 487l, 488l, 490l, 491l, 492l, 493l, 494l, 495l, 496l, 497l, 498l, 500l, 501l, 502l, 503l, 504l, 505l, 506l, 507l, 508l, 509l, 510l, 511l, 512l, 514l, 515l, 516l, 517l, 518l, 519l, 730l, 731l, 732l, 733l, 734l, 735l, 736l, 737l, 739l, 740l, 741l, 742l, 743l, 744l, 746l, 747l, 748l, 749l, 750l, 751l, 752l, 753l, 754l, 755l, 756l, 757l, 758l, 759l, 760l, 761l, 762l, 763l, 764l, 765l, 766l, 767l, 768l, 769l, 770l, 771l, 772l, 773l, 774l, 775l, 844l, 845l, 846l, 847l, 848l, 849l, 850l, 851l, 852l, 853l, 854l, 855l, 856l, 857l, 858l, 859l, 860l, 861l, 862l, 863l, 864l, 865l, 1150l, 1151l, 1152l, 1153l, 1154l, 1155l, 1156l, 1157l, 1158l, 1159l, 1160l, 1161l, 1162l, 1163l, 1164l, 1165l, 1166l, 1167l, 1168l, 1169l, 1170l, 1171l, 1173l, 1174l, 1175l, 1176l, 1177l, 1178l, 1179l, 1180l, 1181l, 1182l, 1183l, 1184l, 1185l, 1186l, 1187l, 1188l, 1189l, 1190l, 1587l, 1588l, 1589l, 1590l, 1591l, 1592l, 1593l, 1594l, 1595l, 1596l, 1597l, 1598l, 1599l, 1600l, 1601l, 1602l, 1603l, 1604l, 1605l, 1606l, 1607l, 1608l, 1609l, 1610l, 1611l, 1612l, 1613l, 1614l, 1615l, 1616l, 1617l, 1618l, 1619l, 1620l, 1621l, 1622l, 1623l, 1624l, 1625l, 1626l, 1627l, 1628l, 1629l, 1630l, 1631l, 1632l, 1633l, 1634l, 1635l, 1636l, 1637l, 1638l, 1639l, 1640l, 1641l, 1642l, 1643l, 1644l, 1645l, 1646l, 1647l, 1648l, 1649l, 1980l, 1981l, 1982l, 1983l, 1984l, 1985l, 1986l, 1987l, 1988l, 1989l, 1990l, 1991l, 1992l, 1993l, 1994l, 1995l, 1996l, 1997l, 1998l, 1999l, 2000l, 2001l, 2002l, 2003l, 2004l, 2005l, 2006l, 2008l, 2009l, 2010l, 2011l, 2012l, 2013l, 2014l, 2015l, 2016l, 2017l, 2018l, 2019l, 2020l, 2021l, 2022l, 2023l, 2024l, 2025l, 2026l, 2027l, 2028l, 2029l, 2030l, 2031l, 2032l, 2033l, 2034l, 2035l, 2036l, 2037l, 2038l, 2039l, 2040l, 2041l, 2042l, 2043l, 2044l, 2045l, 2046l, 2047l, 2048l, 2049l, 2050l, 2051l, 2052l, 2053l, 2054l, 2055l, 2056l, 2057l, 2058l, 2059l, 2060l, 2061l, 2062l, 2063l, 2358l, 2359l, 2360l, 2361l, 2362l, 2363l, 2364l, 2365l, 2366l, 2367l, 2368l, 2369l, 2370l, 2371l, 2372l, 2373l, 2374l, 2375l, 2376l, 2377l, 2378l, 2379l, 2380l, 2381l, 2382l, 2383l, 2384l, 2385l, 2386l, 2387l, 2388l, 2389l, 2390l, 2391l, 2392l, 2393l, 2394l, 2395l, 2396l, 2397l, 2398l, 2399l, 2400l, 2401l, 2402l, 2403l, 2404l, 2405l, 2406l, 2408l, 2409l, 2411l, 2412l, 2413l, 2414l, 2425l, 2426l, 2427l, 2428l, 2429l, 2430l, 2431l, 2432l, 2433l, 2434l, 2435l, 2436l, 2438l, 2439l, 2440l, 2441l, 2442l, 2443l, 2444l, 2445l, 2446l, 2447l, 2448l, 2449l, 2450l, 2451l, 2452l, 2453l, 2454l, 2455l, 2456l, 2457l, 2458l, 2459l, 2460l, 2461l, 2462l, 2463l, 2464l, 2465l, 2628l, 2629l, 2630l, 2631l, 2632l, 2633l, 2634l, 2635l, 2636l, 2637l, 2638l, 2639l, 2640l, 2641l, 2642l, 2643l, 2644l, 2645l, 2646l, 2647l, 2648l, 2649l, 2650l, 2651l, 2652l, 2653l, 2654l, 2655l, 2656l, 2657l, 2658l, 2659l, 2660l, 2661l, 2662l, 2663l, 2664l, 2665l, 2666l, 2667l, 2668l, 2669l, 2670l, 2671l, 2672l, 2673l, 2674l, 2675l, 2676l, 2677l, 2678l, 2679l, 2680l, 2681l, 2682l, 2683l, 2684l, 2685l, 2686l, 2687l, 2688l, 2689l, 2690l, 2691l, 2692l, 2693l, 2694l, 2695l, 2696l, 2697l, 2698l, 2699l, 2700l, 2701l, 2702l, 2703l, 2704l, 2705l, 2706l, 2707l, 2708l, 2709l, 2710l, 2711l, 2712l, 2713l, 2714l, 2715l, 2716l, 2717l, 2718l, 2719l, 2720l, 2721l, 2722l, 2723l, 2724l, 2725l, 2726l, 2727l, 2728l, 2729l, 2730l, 2731l, 2732l, 2733l, 2734l, 2735l, 2736l, 2737l, 2738l, 2739l, 2740l, 2741l, 2742l, 2743l, 2744l, 2745l, 2746l, 2747l, 2748l, 2749l, 2750l, 2751l, 2752l, 2753l, 2754l, 2755l, 2812l, 2813l, 2814l, 2815l, 2816l, 2817l, 2818l, 2819l, 2820l, 2821l, 2822l, 2823l, 2824l, 2825l, 2826l, 2827l, 2828l, 2829l, 2830l, 2831l, 2832l, 2833l, 2834l, 2835l, 2836l, 2837l, 2838l, 2839l, 2840l, 2841l, 2842l, 2843l, 2844l, 2845l, 2846l, 2847l, 2848l, 2849l, 2851l, 2852l, 2853l, 2854l, 2855l, 2856l, 2857l, 2858l, 2859l, 2860l, 2861l, 2862l, 2863l, 2864l, 2865l, 2866l, 2867l, 2868l, 2869l, 2870l, 2871l, 2892l, 2893l, 2894l, 2895l, 2896l, 2897l, 2898l, 2899l, 2900l, 2902l, 2903l, 2904l, 2905l, 2906l, 2907l, 2908l, 2909l, 2910l, 2911l, 2912l };
		long[] eateryID = { 31l, 32l, 33l, 34l, 35l, 160l, 161l, 162l, 390l, 391l, 392l, 393l, 394l, 395l, 396l, 397l, 398l, 399l, 400l, 401l, 402l, 403l, 404l, 405l, 406l, 407l, 408l, 409l, 609l, 611l, 612l, 613l, 614l, 615l, 616l, 617l, 618l, 619l, 620l, 621l, 622l, 623l, 624l, 625l, 626l, 627l, 628l, 629l, 630l, 631l, 632l, 633l, 634l, 635l, 636l, 637l, 638l, 639l, 640l, 641l, 642l, 643l, 644l, 645l, 646l, 960l, 961l, 962l, 963l, 964l, 965l, 966l, 967l, 968l, 969l, 970l, 971l, 972l, 973l, 974l, 975l, 976l, 977l, 978l, 979l, 980l, 981l, 982l, 983l, 984l, 985l, 986l, 987l, 988l, 990l, 991l, 993l, 994l, 995l, 996l, 997l, 998l, 999l, 1000l, 1001l, 1002l, 1003l, 1004l, 1005l, 1006l, 1007l, 1008l, 1009l, 1010l, 1011l, 1012l, 1013l, 1014l, 1015l, 1016l, 1017l, 1018l, 1019l, 1020l, 1388l, 1389l, 1390l, 1391l, 1392l, 1393l, 1394l, 1395l, 1396l, 1398l, 1399l, 1400l, 1401l, 1402l, 1403l, 1404l, 1405l, 1406l, 1407l, 1408l, 1409l, 1410l, 1411l, 1412l, 1413l, 1414l, 1415l, 1416l, 1417l, 1418l, 1419l, 1420l, 1421l, 1422l, 1423l, 1424l, 1425l, 1426l, 1428l, 1429l, 1793l, 1794l, 1795l, 1796l, 1797l, 1798l, 1799l, 1800l, 1801l, 1802l, 1803l, 1804l, 1805l, 1806l, 1807l, 1808l, 1809l, 1810l, 1811l, 1812l, 1813l, 1814l, 1815l, 1816l, 1817l, 1818l, 1819l, 1820l, 1821l, 1822l, 1823l, 1824l, 1825l, 1826l, 1827l, 1828l, 1829l, 1830l, 1831l, 1832l, 1833l, 1834l, 1835l, 1836l, 1837l, 1839l, 1840l, 1841l, 1842l, 1843l, 1844l, 1845l, 1846l, 1847l, 1848l, 1849l, 1851l, 1852l, 1853l, 1854l, 1855l, 1856l, 2194l, 2195l, 2196l, 2197l, 2198l, 2199l, 2200l, 2201l, 2202l, 2203l, 2204l, 2205l, 2206l, 2207l, 2208l, 2209l, 2210l, 2211l, 2212l, 2213l, 2214l, 2215l, 2216l, 2217l, 2218l, 2219l, 2220l, 2221l, 2222l, 2223l, 2224l, 2225l, 2226l, 2227l, 2229l, 2230l, 2231l, 2232l, 2233l, 2234l, 2235l, 2236l, 2237l, 2238l, 2239l, 2240l, 2241l, 2242l, 2243l, 2244l, 2245l, 2246l, 2247l, 2248l, 2249l, 2250l, 2251l, 2252l, 2253l, 2254l, 2255l, 2256l, 2257l, 2258l, 2259l, 2260l, 2261l, 2262l, 2475l, 2476l, 2477l, 2478l, 2479l, 2480l, 2481l, 2482l, 2483l, 2484l, 2485l, 2486l, 2487l, 2488l, 2489l, 2490l, 2491l, 2492l, 2493l, 2494l, 2495l, 2496l, 2497l, 2498l, 2499l, 2500l, 2501l, 2502l, 2503l, 2504l, 2505l, 2506l, 2507l, 2508l, 2509l, 2510l, 2511l, 2512l, 2513l, 2514l, 2515l, 2516l, 2517l, 2518l, 2519l, 2520l, 2521l, 2522l, 2523l, 2524l, 2525l, 2526l, 2527l, 2528l, 2529l, 2530l, 2531l, 2532l, 2533l, 2534l, 2535l, 2536l, 2537l, 2538l, 2539l, 2540l, 2541l, 2542l, 2543l, 2544l, 2545l, 2546l, 2547l, 2548l, 2549l, 2550l, 2551l, 2552l, 2553l, 2554l, 2555l, 2556l, 2557l, 2558l, 2559l, 2560l, 2561l, 2756l, 2757l, 2758l, 2759l, 2760l, 2761l, 2762l, 2763l, 2764l, 2765l, 2766l, 2767l, 2768l, 2769l, 2770l, 2771l, 2772l, 2773l, 2774l, 2775l, 2776l, 2777l, 2778l, 2779l, 2780l, 2781l, 2782l, 2783l, 2784l, 2785l, 2786l, 2787l, 2788l, 2789l, 2790l, 2791l, 2792l, 2793l, 2794l, 2795l, 2796l, 2797l, 2798l, 2799l, 2800l, 2801l, 2802l, 2803l, 2804l, 2805l, 2806l, 2807l, 2872l, 2873l, 2874l, 2875l, 2876l, 2877l, 2878l, 2879l, 2880l, 2881l, 2882l, 2883l, 2884l, 2885l, 2886l, 2887l, 2888l, 2889l, 2890l, 2891l, 2913l, 2914l, 2915l, 2916l, 2917l, 2918l, 2919l, 2920l, 2921l, 2922l, 2923l, 2924l, 2925l, 2926l, 2927l, 2928l, 2929l, 2930l, 2931l, 2932l, 2933l, 2934l, 2935l, 2936l, 2937l, 2938l, 2939l, 2940l, 2941l, 2942l, 2943l, 2944l, 2945l, 2946l };
		
		String[] comments= {"Comentaire du Client...........",null,"Tres bon merci...","","Les plats étaient excellents....","","","","",null,null,null,"Vraiment super bon....","C'était super bon mon repas. je recommande","Bien...","Au top comme d'habitude...","Excellent, je recommande ce restaurant sans hésitation.","Bonne restaurant...","Excellente cuisine..","Un vrais délice..","au top :)","Super :)","Un peu juste en quantité...","Très contente de ma commande...","Très bien très bon" };
		Customer bmc=null;
		String rand=r.nextInt(99)+"";
		List<Customer> list=new ArrayList<Customer>();
		try {
			Customer	c01 = service.subscribeA("Tesa01@hot.com"+rand, "aaa", "bmc", "WANG", "Mr.", "0123456789");
			Customer	c02 = service.subscribeA("Tesa02@hot.com"+rand, "sirong", "bmc", "LIU", "Mlle.", "0123456789");
			Customer	c03 = service.subscribeA("Tesa03@hot.com"+rand, "sirong", "bmc", "ZHANG", "Mr.", "0123456789");
			Customer	c04 = service.subscribeA("Tesa04@hotl.com"+rand, "sirong", "bmc", "CHEN", "Mme.", "0123456789");
			Customer	c05 = service.subscribeA("Tesa05@hotl.com"+rand, "sirong", "bmc", "ZHAO", "Mr.", "0123456789");
			Customer	c06 = service.subscribeA("Tesa06@hotl.com"+rand, "sirong", "bmc", "LI", "Mr.", "0123456789");
			Customer	c07 = service.subscribeA("Tesa07@hotl.com"+rand, "aaa", "r", "ZHENG", "Mr.", "0123456789");
			Customer	c08 = service.subscribeA("Tesa08@hotl.com"+rand, "aaar", "rong", "XU", "Mlle.", "0123456789");
			Customer	c09 = service.subscribeA("Tesa09@hotl.com"+rand, "aaar", "r", "ZHAO", "Mr.", "0123456789");
			Customer	c10 = service.subscribeA("Tes110@hotl.com"+rand, "aaar", "r", "ZHANG", "Mr.", "0123456789");
			Customer	c11 = service.subscribeA("Tes111@hotl.com"+rand, "aaar", "bmc", "CHEN", "Mr.", "0123456789");
			Customer	c12 = service.subscribeA("Tes121@hotl.com"+rand, "aaar", "rong", "LIN", "Mlle.", "0123456789");
			Customer	c13 = service.subscribeA("Tes1311@hoil.com"+rand, "aaar", "bmc", "XU", "Mr.", "0123456789");
			Customer	c14 = service.subscribeA("Tes1411@hoil.com"+rand, "aaar", "bmc", "MING", "Mr.", "0123456789");
			Customer	c15 = service.subscribeA("Tes1511@hoil.com"+rand, "aaar", "bmc", "XU", "Mr.", "0123456789");
			list.add(c01);
			list.add(c02);
			list.add(c03);
			list.add(c04);
			list.add(c05);
			list.add(c06);
			list.add(c07);
			list.add(c08);
			list.add(c09);
			list.add(c10);
			list.add(c11);
			list.add(c12);
			list.add(c13);
			list.add(c14);
			list.add(c15);
			
		} catch (UserNameAlreadyExistsException e) {
			
		}
		for(int i =0; i<315; i++) {
			Booking booking=new Booking();
			booking.setCustomer(list.get(i%15));
			long eId =eateryID[r.nextInt(eateryID.length)];
			Eatery oneEateryByEateryID = eateryService.getOneEateryByEateryID(eId);
			booking.setEatery(oneEateryByEateryID);
			booking.setDate_time(getTime());
			booking.setNb_people((long)r.nextInt(8)+1);
			Booking saveBooking = bookingService.saveBooking(booking);
			Review review = new Review();
			review.setComment(comments[r.nextInt(comments.length)]);
			review.setRating(Long.valueOf(r.nextInt(5)+1));
			review.setBooking(saveBooking);
			reviewService.saveOneReview(review);
		}
		try {
			bmc = service.subscribeA("BMC4J@hotmail.fr", "BMC4J@hotmail.fr", "bmc", "BMC4J","Mr.", "0123456789");
		} catch (UserNameAlreadyExistsException e) {
			log.error("Error while saving bmc4j");
			bmc=service.findByUsername("BMC4J@hotmail.com");
		}
		for(int i =0; i<5; i++) {
			Booking booking=new Booking();
			booking.setCustomer(bmc);
			long eId =eateryID[r.nextInt(eateryID.length)];
			Eatery oneEateryByEateryID = eateryService.getOneEateryByEateryID(eId);
			booking.setEatery(oneEateryByEateryID);
			booking.setDate_time(getTime2());
			booking.setNb_people((long)r.nextInt(8)+1);
			bookingService.saveBooking(booking);
		}
		for(int i =0; i<3; i++) {
			Booking booking=new Booking();
			booking.setCustomer(bmc);
			long eId =eateryID[r.nextInt(eateryID.length)];
			Eatery oneEateryByEateryID = eateryService.getOneEateryByEateryID(eId);
			booking.setEatery(oneEateryByEateryID);
			booking.setDate_time(getTime());
			booking.setNb_people((long)r.nextInt(8)+1);
			bookingService.saveBooking(booking);
			
		}
		for(int i =0; i<3; i++) {
			Booking booking=new Booking();
			booking.setCustomer(bmc);
			long eId =eateryID[r.nextInt(eateryID.length)];
			Eatery oneEateryByEateryID = eateryService.getOneEateryByEateryID(eId);
			booking.setEatery(oneEateryByEateryID);
			booking.setDate_time(getTime());
			booking.setNb_people((long)r.nextInt(8)+1);
			Booking saveBooking = bookingService.saveBooking(booking);
			Review review = new Review();
			review.setComment(comments[r.nextInt(comments.length)]);
			review.setRating(Long.valueOf(r.nextInt(5)+1));
			review.setBooking(saveBooking);
			reviewService.saveOneReview(review);
			booking.setDate_time(getTime());
			booking.setNb_people((long)r.nextInt(8)+1);
			bookingService.saveBooking(booking);
			review.setComment(comments[r.nextInt(comments.length)]);
			review.setRating(Long.valueOf(r.nextInt(5)+1));
			review.setBooking(saveBooking);
			reviewService.saveOneReview(review);
		}
		
		for(int i =0; i<95; i++) {
			Booking booking=new Booking();
			booking.setCustomer(list.get(i%15));
			Eatery oneEateryByEateryID = eateryService.getOneEateryByEateryID(1l);
			booking.setEatery(oneEateryByEateryID);
			
			booking.setDate_time(getTime());
			booking.setNb_people((long)r.nextInt(8)+1);
			Booking saveBooking = bookingService.saveBooking(booking);
			Review review = new Review();
			review.setComment(comments[r.nextInt(comments.length)]);
			review.setRating(Long.valueOf(r.nextInt(5)+1));
			review.setBooking(saveBooking);
			reviewService.saveOneReview(review);
		}
		
		
		return true;
		
	}
	private LocalDateTime getTime() {
		Random r=new Random();
//		int  plusMinus=1;
		int days=90;
		LocalDate day = LocalDate.now().minusDays(r.nextInt(days));
		int hour = r.nextInt(24);
		int minute = r.nextInt(60);
		int second = r.nextInt(60);
		LocalTime time = LocalTime.of(hour, minute, second);
		LocalDateTime of = LocalDateTime.of(day, time);
//		String dayTime = LocalDateTime.of(day, time).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'hh:mm:ss"));
		return of;
		}
	private LocalDateTime getTime2() {
		Random r=new Random();
//		int  plusMinus=1;
		int days=4;
		LocalDate day = LocalDate.now().plusDays(r.nextInt(days));
		int hour = r.nextInt(24);
		int minute = r.nextInt(60);
		int second = r.nextInt(60);
		LocalTime time = LocalTime.of(hour, minute, second);
		LocalDateTime of = LocalDateTime.of(day, time);
//		String dayTime = LocalDateTime.of(day, time).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'hh:mm:ss"));
		return of;
	}

}
