package data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.Menu;
import data.interfaces.IMenu;
import data.repos.MenuRepository;
@Service
public class MenuService implements IMenu {
	@Autowired
	MenuRepository menuRepository;

	@Override
	public Menu getOneMenuByID(Long menuID) {
		return menuRepository.findById(menuID).get();
	}
	
	
}
