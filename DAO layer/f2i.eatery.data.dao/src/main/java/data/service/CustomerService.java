package data.service;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.Customer;
import data.exceptions.NoUserException;
import data.exceptions.PasswordWrongException;
import data.exceptions.UserNameAlreadyExistsException;
import data.hashing.Crypting;
import data.interfaces.ICustomer;
import data.repos.CustomerRepository;

@Service
public class CustomerService implements ICustomer {
	private static final Logger log = LogManager.getLogger(CustomerService.class);

	@Autowired
	CustomerRepository customerService;

	@Override
	public Customer authentification(String username, String password) throws PasswordWrongException, NoUserException {
		Customer findByUsername = customerService.findByUsername(username);
		if (findByUsername == null) {
			throw new NoUserException("Cet identifiant n'existe pas!!");
		} else {
			String encrypPass = Crypting.encryptingMessage(password);
			Customer findByUsernameAndPassword = customerService.findByUsernameAndPassword(username, encrypPass);
			if (findByUsernameAndPassword == null)
				throw new PasswordWrongException("Le mot de pass n'est pas correct!!");
			return customerService.findByUsernameAndPassword(username, encrypPass);
		}
	}

	@Override
	public Customer findByUsername(String username) {

		return customerService.findByUsername(username);
	}

	@Override
	public Boolean subscribe(Customer custumer) throws UserNameAlreadyExistsException {

		Customer custo = customerService.findByUsername(custumer.getUsername());

		if (custo == null) {
			Customer customerSaved = customerService.save(custumer);
			log.info(customerSaved);
			return true;
		} else {
			throw new UserNameAlreadyExistsException(
					"Adresse mail déjà utilisée : Veuillez choisir une autre adresse email!");
		}
	}

	@Override
	public Customer save(Customer custumer) throws UserNameAlreadyExistsException {
		Customer save = null;
		try {
			save = customerService.save(custumer);
		} catch (Exception e) {
			log.error("Error while saving Customer...");
			save = customerService.findByUsername(custumer.getUsername());
			return save;
		}
		return save;
	}

	@Override
	public Customer updateCustomer(Customer customer) {

		return customerService.save(customer);
	}

	@Override
	@Transactional
	public void updatePassword(String username, String oldPassword, String newPassword) throws PasswordWrongException {
		Customer customer = customerService.findByUsername(username);
		String encrypPass = Crypting.encryptingMessage(oldPassword);
		Customer findByUsernameAndPassword = customerService.findByUsernameAndPassword(username, encrypPass);

		if (findByUsernameAndPassword == null)
			throw new PasswordWrongException("Le mot de pass n'est pas correct!!");
		else {
			String newencrypPass = Crypting.encryptingMessage(newPassword);
			customer.setPasswordEncry(newencrypPass);
			customerService.save(customer);
		}
	}

	@Transactional
	public void unsubscribeCustomer(String username) {
		customerService.unsubscribe(username);
	}

	@Override
	public Customer updateUser(Customer customer) {
		return null;
	}

	@Override
	public Customer subscribeA(String username, String password, String firstname, String lastname, String title,
			String phone) throws UserNameAlreadyExistsException {
		Customer findByUsername = customerService.findByUsername(username);
		if (findByUsername != null)
			throw new UserNameAlreadyExistsException("Cet identifiant est déjà utilisé!!");
		String encrypPass = Crypting.encryptingMessage(password);
		Customer customer = new Customer(username, encrypPass, firstname, lastname, title, phone);
		customerService.save(customer);
		return customer;
	}
}
