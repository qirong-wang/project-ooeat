package data.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.entities.Region;
import data.interfaces.IRegion;
import data.repos.RegionRepository;
@Service
public class RegionService implements IRegion {
//private static final Logger log = LogManager.getLogger(RegionService.class);
@Autowired
RegionRepository regionRepository;
	@Override
	public Region getOneRegionByID(Long regionID) {
		return regionRepository.findById(regionID).get();
	}

	@Override
	public List<Region> getAllRegion() {
		return regionRepository.findAll();
	}

}
