package data.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import data.entities.UnsubscribedCustomer;


@Repository
public interface UnsubscribedRepository extends JpaRepository<UnsubscribedCustomer, Long> {

//	void add(Object customer);

//	UnsubscribedCustomer save(Customer customer);

}
