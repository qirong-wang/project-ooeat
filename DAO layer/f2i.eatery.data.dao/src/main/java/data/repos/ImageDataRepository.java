package data.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import data.entities.ImageData;

public interface ImageDataRepository extends JpaRepository<ImageData, Long>,CustomizedImageDataRepository {

	ImageData findImageDataById(Long id);
//	ImageData findOneSmallImageData();

//	@Query("select u from ImageData u where u.size = ?small")
//	List<ImageData> findBySize(String size);
}
