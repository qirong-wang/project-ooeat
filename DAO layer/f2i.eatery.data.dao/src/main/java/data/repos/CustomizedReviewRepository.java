package data.repos;

import java.util.List;
import java.util.Optional;

import data.po.ReviewInfo;

public interface CustomizedReviewRepository {

	Optional<List<ReviewInfo>> findListReviewByEateryID(Long eateryID, Integer firstPage, Boolean withComment);

	Optional<Integer> findNbReviewsWithCommentByEateryID(Long eateryID);
}
