package data.repos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import data.entities.CookingStyle;
import data.entities.EateryTag;
import data.po.EateryInfo;
import data.po.EateryRequest;
import data.tools.StringTool;

@Repository
public class CustomizedEateryRepositoryImpl implements CustomizedEateryRepository {
	private static final Logger log = LogManager.getLogger(CustomizedEateryRepositoryImpl.class);
	@PersistenceContext
	private EntityManager entityManager;
	private Integer nbEatery;
	private List<Long> listEateryID;
	private String selectAll = "select e.id, e.name, e.address.street, e.address.postCode, c.name, p.hoursOp1, p.hoursOp2, p.price, s.id, s.name, count(r.booking), avg(r.rating)  ";
	private String selectAllID = "select e.id,  p.price ";
	private String fromAll = " From Review r right join r.booking b right join b.eatery e inner join e.city c inner join e.practicalInfo p inner join e.cookingStyle s where 1=1 ";
//	private String fromEatery = " From Eatery e inner join e.city c inner join e.practicalInfo p inner join e.cookingStyle s where 1=1 ";
	private String restrictionWhere = "";
	private String restrictionRating = "";

	private List<Map.Entry<Long, Integer>> listEateryIdPrice;

	private Long cityID;
	private String eateryNameLike;
	private List<String> cookingStyleIDs;
	private List<String> tagIDs;
	private String minPrice;
	private String maxPrice;
	private String minNote;
	private Integer firstPage;
	private Integer maxSize;
	private String order;
	private String restrictionCityID = "",restrictionNameLike="", restrictionCookingStylesID = "", restrictionTagsID = "",
			restrictionPrice = "", restrictionOrder = "";

	@Override
	public Optional<List<EateryInfo>> findListEateryByMultiConditions(EateryRequest eateryRequest) {
		System.out.println(" findListEateryByMultiConditions(EateryRequest eateryRequest)  ---call....");
		setRestrictions(eateryRequest);
//		if (firstPage != null && firstPage > 0 &&order!=null&& order.contains("price")) {
//			return getNextPageEateryInfo();
//		}
		if (order != null && order.length() > 0) {
			if (order.contains("price")) {
				return getListEateryByMultiConditionsByOrderPrice();
			}
			restrictionOrder = " order by " + order;
		}

		String restrictions = restrictionWhere + "   group by e.id,b.eatery " + restrictionRating + restrictionOrder;
		String hql = selectAll + fromAll + restrictions;
//		System.out.println(hql);
//		log.debug(hql);
		Query query = entityManager.createQuery(hql);
		query.setFirstResult(firstPage);
		query.setMaxResults(maxSize);

		@SuppressWarnings("unchecked")
		List<Object[]> resultList = query.getResultList();

		List<EateryInfo> listEateryInfo = new ArrayList<EateryInfo>();

		listEateryID = new ArrayList<Long>();
		if (resultList != null)
			for (Object[] o : resultList) {
//				Long eatertyID = (long) o[0];
				EateryInfo eateryInfo = getEateryInfoFromObject(o);
//				listEateryID.add(eatertyID);
				listEateryInfo.add(eateryInfo);
			}
		nbEatery = listEateryInfo.size();
		return Optional.ofNullable(listEateryInfo);
	}

	private void setRestrictions(EateryRequest eateryRequest) {
		firstPage = eateryRequest.getFirstPage();
		maxSize = eateryRequest.getMaxSize();
		order = eateryRequest.getOrder();
		cityID = eateryRequest.getCityID();
		eateryNameLike=eateryRequest.getEateryNameLike();
		cookingStyleIDs = eateryRequest.getCookingStyleIDs();
		tagIDs = eateryRequest.getTagsIDs();
		minPrice = eateryRequest.getMinPrice();
		maxPrice = eateryRequest.getMaxPrice();
		minNote = eateryRequest.getMinNote();

		if (cityID != null)
			restrictionCityID = " And c.id = " + cityID;
		else 
			restrictionCityID=" ";
		
		if (eateryNameLike != null&&eateryNameLike.length()>0)
			restrictionNameLike = " And e.name like '%" + eateryNameLike+"%' ";
		else 
			restrictionNameLike=" ";
 
		if (cookingStyleIDs != null && cookingStyleIDs.size() > 0)
			restrictionCookingStylesID = " And s.id in "
					+   StringTool.listConversion(cookingStyleIDs);
		else restrictionCookingStylesID=" ";

		if (minPrice != null && maxPrice != null)
			restrictionPrice = " And p.price between " + minPrice + " and " + maxPrice;
		else restrictionPrice=" ";

		if (tagIDs != null && tagIDs.size() > 0)
			restrictionTagsID = " And t.id in " + StringTool.listConversion(tagIDs);
		else restrictionTagsID= " "; 

		if (minNote != null && !minNote.equals("0"))
			restrictionRating = " having avg(r.rating)> " + minNote;
		else restrictionRating=" ";
		firstPage = firstPage == null ? 0 : firstPage;
		maxSize = maxSize == null ? 3000 : maxSize;

		restrictionWhere = restrictionCityID +restrictionNameLike+ restrictionCookingStylesID + restrictionPrice + restrictionTagsID;
	}

	private Optional<List<EateryInfo>> getListEateryByMultiConditionsByOrderPrice() {
		String hql = selectAllID + fromAll + restrictionWhere + " group by e.id,b.eatery " + restrictionRating;
//		System.out.println(hql);
		Query query = entityManager.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Object[]> resultList = query.getResultList();
		Map<Long, Integer> eateryIdPriceMap = null;
		if(firstPage==0)listEateryID = new ArrayList<Long>();
		if (resultList != null && resultList.size() > 0) {
			eateryIdPriceMap = new TreeMap<Long, Integer>();
			for (Object[] o : resultList) {
				String price = (String) o[1];
				Long eateryId=(long) o[0];
				if(firstPage==0)listEateryID.add(eateryId);
				if (price != null) {
					price = price.substring(0, price.length() - 1).trim();
					eateryIdPriceMap.put(eateryId, Integer.valueOf(price));
				} else
					eateryIdPriceMap.put(eateryId, 0);
			}
		} else {
			nbEatery = 0;
			return Optional.ofNullable(new ArrayList<EateryInfo>());
		}

		listEateryIdPrice = new ArrayList<Map.Entry<Long, Integer>>(eateryIdPriceMap.entrySet());
		Collections.sort(listEateryIdPrice, new Comparator<Map.Entry<Long, Integer>>() {
			@Override
			public int compare(Entry<Long, Integer> o1, Entry<Long, Integer> o2) {
				return (order.contains("desc") ? (o2.getValue() - (o1.getValue())) : (o1.getValue() - (o2.getValue())));
			}
		});
		nbEatery = listEateryIdPrice.size();
		maxSize = nbEatery > maxSize ? maxSize : nbEatery;
		return getNextPageEateryInfo();
	}

//to delete
	@Override
	public Optional<List<EateryInfo>> findListEateryByMultiConditions(String restrictionWhere, String restrictionHaving,
			Integer firstResult, Integer maxSize, String restrictionOrderBy) {
		log.debug("CustomizedEateryRepositoryImpl findListEateryByMultiConditions called.......");
		String select = "select e.id, e.name, e.address.street, e.address.postCode, c.name, p.hoursOp1, p.hoursOp2, p.price, s.id, s.name, count(r.booking), avg(r.rating)  ";
		String from = " From Review r right join r.booking b right join b.eatery e inner join e.city c inner join e.practicalInfo p inner join e.cookingStyle s where 1=1";
		String restrictions = restrictionWhere + "   group by e.id,b.eatery " + restrictionHaving + restrictionOrderBy;
		String hql = select + from + restrictions;
//		log.debug(hql);
		Query query = entityManager.createQuery(hql);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxSize);
		@SuppressWarnings("unchecked")
		List<Object[]> resultList = query.getResultList();
		List<EateryInfo> listEateryInfo = new ArrayList<EateryInfo>();
		listEateryID = new ArrayList<Long>();
		if (resultList != null)
			for (Object[] o : resultList) {
				EateryInfo eateryInfo = getEateryInfoFromObject(o);
				listEateryInfo.add(eateryInfo);
			}
		nbEatery = entityManager.createQuery(hql).getResultList().size();

		return Optional.ofNullable(listEateryInfo);
	}

	@Override
	public Optional<Integer> findNbEateryByMultiConditions(EateryRequest eateryRequest) {
		log.debug("findNbEateryByMultiConditions(EateryRequest eateryRequest)    call....");
		if (nbEatery == null || nbEatery == 0)
			return Optional.ofNullable(0);
		
		System.out.println("--------------");
		if (order!=null&&order.contains("price")) {
			return Optional.ofNullable(nbEatery);
		}
		String restrictions = restrictionWhere + "   group by e.id" + restrictionRating;
		String hql = selectAllID + fromAll + restrictions;
		System.err.println(hql);
		listEateryID=new ArrayList<Long>();
		@SuppressWarnings("unchecked")
		List<Object[]> resultList = entityManager.createQuery(hql).getResultList();
		if (resultList != null)System.out.println("=====findNbEateryByMultiConditions size : "+resultList.size()+"=====");
		if (resultList != null) {
			for (Object[] o : resultList) {
				listEateryID.add((long)o[0]);
//				for (int i = 0; i < o.length; i++) {
//					System.out.print(o[i] + "\t");
//				}
//				System.out.println((o[1].toString().length()));
			}
		}

		return Optional.ofNullable(resultList.size());
	}
	@Override
	public Optional<List<CookingStyle>> findListCookingStyleByMultiConditions(Long cityID) {
		log.debug("findListCookingStyleByMultiConditions   call....");
		String hql;
		if (cityID == null || cityID == 0)
			hql = "select s from Eatery e inner join e.cookingStyle s group by s.id order by s.name";
		else
			hql = "select s from Eatery e inner join e.city c inner join e.cookingStyle s where c.id = " + cityID
					+ " group by s.id order by s.name";
		@SuppressWarnings("unchecked")
		List<CookingStyle> resultList = entityManager.createQuery(hql).getResultList();
		return Optional.ofNullable(resultList);
	}
//to delete
	@Override
	public Optional<Integer> findNbEateryByMultiConditions() {
		log.debug("CustomizedEateryRepositoryImpl findNbEateryByMultiConditions called.......");

		return Optional.ofNullable(nbEatery);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Optional<List<String[]>> findNbCookingStyleByMultiConditions() {
		System.out.println("----------Nb CookingStyle-- call-------");
		List<Object[]> resultList = null;
		List<String[]> cookingStyleList = new ArrayList<String[]>();
		if (listEateryID != null&&listEateryID.size()>0) {
			String hql = "select count(s.id), s.id ,s.name  "
					+ "From Eatery e inner join e.cookingStyle s where e.id in "
					+ listEateryID.toString().replace('[', '(').replace(']', ')') + " group by s.id Order by count(s.id) desc " + "   ";
			System.out.println(hql);
			Query query = entityManager.createQuery(hql);
			try {
				resultList = query.getResultList();
				cookingStyleList = new ArrayList<String[]>();
				for (Object[] o : resultList) {
					cookingStyleList.add(new String[] { o[0] + "", "" + o[1], o[2] + "" });
				}
			} catch (Exception e) {
				log.error(" ERROR while getting result " + e.getLocalizedMessage());
			}
		}
		return Optional.ofNullable(cookingStyleList);

	}

	private Optional<List<EateryInfo>> getNextPageEateryInfo() {
		List<Entry<Long, Integer>> nextListEateryInfo = listEateryIdPrice.subList(firstPage, maxSize+firstPage);
		List<EateryInfo> nextListeateryInfo = new ArrayList<EateryInfo>();
		for (Map.Entry<Long, Integer> mapping : nextListEateryInfo) {
			nextListeateryInfo.add(getOneEateryInfoByEateryID(mapping.getKey()));
		}
		return Optional.ofNullable(nextListeateryInfo);
	}

	private Map<String, String> getTagsByEateryId(Long eateryID) {
		Query query = entityManager.createQuery("Select e.eateryTags FROM Eatery e where e.id = " + eateryID);
		@SuppressWarnings("unchecked")
		List<EateryTag> resultList = query.getResultList();
		if (resultList != null && resultList.size() > 0) {
			Map<String, String> map = new TreeMap<String, String>();
			resultList.forEach(t -> {
				map.put("tag"+t.getId(), t.getName());
			});
			return map;
		} else
			return null;
	}

	private EateryInfo getOneEateryInfoByEateryID(Long eateryID) {
		Query query = entityManager.createQuery(selectAll + fromAll + " And e.id = " + eateryID);
		Object[] object = (Object[]) query.getResultList().get(0);
		return getEateryInfoFromObject(object);
	}

	private EateryInfo getEateryInfoFromObject(Object[] o) {
		Long eatertyID = (long) o[0];
		EateryInfo eateryInfo = new EateryInfo(eatertyID, (String) o[1], (String) (String) o[2], (String) o[3],
				(String) o[4], (String) o[5], (String) o[6], (String) o[7], (Long) o[8], (String) o[9], (Long) o[10],
				(Double) o[11]);
		eateryInfo.setTags(getTagsByEateryId(eatertyID));
		return eateryInfo;
	}

	// to delete demo test
	@Override
	public Optional<EateryInfo> getOneEateryInfoByID(Long eateryID) {
		long time1 = System.currentTimeMillis();
		String eateryNameLike= "PARIS";
//		EateryRequest requestDTO=new EateryRequest(95l, new String[] {"1","20"}, null, "p.price", null, null, null, null, null);
//		EateryRequest requestDTO=new EateryRequest(eateryID, cookingStyleIDs, tagsIDs, order, minNote, maxPrice, minPrice, firstPage, maxSize)
//		EateryRequest requestDTO = new EateryRequest(1l, null, null, null, null,10 + "", 33 + "", 0, 10);
//		List<String> cooks=new ArrayList<String>();
//		cooks.add("2");
//		cooks.add("5");
//		 new EateryRequest(eateryID, cookingStyleIDs, tagsIDs, order, minNote, minPrice, maxPrice, firstPage, maxSize)
//		EateryRequest requestDTO = new EateryRequest(1l, null, null, null, null,null, null, 120, 10);
//		EateryRequest requestDTO = new EateryRequest(1l, eateryNameLike ,null, null, "s.price", null,null, null, null, 10);
		EateryRequest requestDTO = new EateryRequest(null, eateryNameLike ,null, null, null, null,null, null, null, null);
//		EateryRequest requestDTO2 = new EateryRequest(1l, null, null, "s.price", null,null, null, 10, 10);
//		EateryRequest requestDTO = new EateryRequest(1l, null, null, null, "0.2",null, null, null, 10);
		List<EateryInfo> list = findListEateryByMultiConditions(requestDTO).get();
//		List<EateryInfo> list1 = findListEateryByMultiConditions(requestDTO2).get();
		System.out.println("----------------list size: "+list.size()+"-------------------------");
		if (list != null) {
			list.forEach(e -> System.out.println(e.getName() + "\t" + e.getPrice() + "\t" + e.getTags()));
			System.out.println("size in this page: "+list.size() + "++++");
		}
//		getListEateryByMultiConditionsByOrderPrice();
		long time2 = System.currentTimeMillis();
		System.out.println(time2 - time1 + "--TIME---");
		Integer integer = findNbEateryByMultiConditions(requestDTO).get();
		List<String[]> list2 = findNbCookingStyleByMultiConditions().get();
		System.out.println("coooking style size:" + list2.size());
//		list2.forEach(e->System.out.println(e[0]+"-"+ e[1] +"--"+e[2]));
		
		System.out.println("all items founds: "+integer + "::::");
		System.out.println(nbEatery + "!!!!!!!!!!!!");

//		Query query = entityManager.createQuery(selectAll+ fromAll +" And e.id = "+eateryID);
//		Object[] object = (Object[]) query.getResultList().get(0);
//		System.out.println(object[0]+"--"+object[2]);
		return Optional.ofNullable(null);
	}

}
