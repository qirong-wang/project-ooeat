package data.repos;

import java.util.List;
import java.util.Optional;

import data.entities.ImageData;

public interface CustomizedImageDataRepository {
	Optional<ImageData> findOneSmallPhotoByEateryID(Long eateryID);

	Optional<List<Long>> findListPhotoIDByEateryID(Long eateryID);

	Optional<Long> findOneSmallPhotoIDByEateryID(Long eateryID);
}
