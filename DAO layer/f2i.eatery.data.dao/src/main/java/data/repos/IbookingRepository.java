package data.repos;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import data.entities.Booking;
import data.entities.Customer;

@Repository
public interface IbookingRepository extends JpaRepository<Booking, Long> {
	
	
	List <Booking> findByCustomerAndDateTimeAfterOrderByDateTimeAsc(Customer customer, LocalDateTime time);
	List <Booking> findByCustomerAndDateTimeBeforeOrderByDateTimeDesc(Customer customer, LocalDateTime time);
	Optional<Booking> findById(Long id);
	void deleteById(Long id);
}
