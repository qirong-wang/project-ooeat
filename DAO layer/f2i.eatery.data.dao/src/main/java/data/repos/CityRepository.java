package data.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import data.entities.City;

public interface CityRepository extends JpaRepository<City, Long> {
	@Query("select c from City c order by c.name")
	List<City> findAllOrderByName();
	@Query("select c from Eatery e inner join e.city c group by c.id order by c.name")
	List<City> findAllCityHaveEateryOrderByName();
	@Query("select c from Eatery e inner join e.city c inner join c.region r where r.id = :regionID group by c.id order by c.name")
	List<City> findAllCityHaveEateryByRegionIdOrderByName(@Param("regionID") Long regionID);
	List<City> findByName(String cityName);
	List<City> findByNameLikeIgnoreCaseOrderByName(String cityNameLike);
	@Query("select c from City c inner join c.region r where r.id = :regionID order by c.name")
	List<City> findAllCityByRegionID(@Param("regionID") Long regionID);
}
