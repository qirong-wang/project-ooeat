package data.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import data.entities.Menu;

public interface MenuRepository extends JpaRepository<Menu, Long> {
}
