package data.repos;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import data.entities.ImageData;

@Repository
public class CustomizedImageDataRepositoryImpl implements CustomizedImageDataRepository {
	private static final Logger log = LogManager.getLogger(CustomizedImageDataRepositoryImpl.class);
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Optional<Long> findOneSmallPhotoIDByEateryID(Long eateryID) {
		Long imageId =null;
		try {
			imageId = (Long)entityManager.createQuery("select i.id From ImageData i where i.target='eatery' AND i.size='small' AND i.targetID = " +eateryID).setMaxResults(1).getResultList().get(0);
		} catch (Exception e) {
			log.error("Error while loading image for eatery ID : "+eateryID);
			return Optional.ofNullable(0l);
		}
		return Optional.ofNullable(imageId);
//		return Optional.ofNullable((ImageData)entityManager.createQuery("From ImageData i where i.target='eatery' AND i.size='small' AND i.targetID = " +eateryID).setMaxResults(1).getResultList().get(0));
	}
	@SuppressWarnings("unchecked")
	@Override
	public Optional<List<Long>> findListPhotoIDByEateryID(Long eateryID) {
		
//		Long imageID = (Long) entityManager.createQuery("select i.id From ImageData i where i.target='eatery' AND i.size='small' AND i.targetID = " +eateryID).setMaxResults(1).getResultList().get(0);
		return Optional.ofNullable((List<Long>)entityManager.createQuery("select i.id From ImageData i where i.target='eatery' AND i.size='big' AND i.targetID = " +eateryID).getResultList());
//		return Optional.ofNullable((ImageData)entityManager.createQuery("From ImageData i where i.target='eatery' AND i.size='small' AND i.targetID = " +eateryID).setMaxResults(1).getResultList().get(0));
	}
	@Override
	public Optional<ImageData> findOneSmallPhotoByEateryID(Long eateryID) {
		// TODO Auto-generated method stub
		return null;
	}

}
