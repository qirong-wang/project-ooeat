package data.repos;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import data.po.ReviewInfo;

@Repository
public class CustomizedReviewRepositoryImpl implements CustomizedReviewRepository {
	private static final Logger log = LogManager.getLogger(CustomizedReviewRepositoryImpl.class);
	@PersistenceContext
	private EntityManager entityManager;
	@Override
	public Optional<List<ReviewInfo>> findListReviewByEateryID(Long eateryID, Integer firstPage, Boolean withComment) {
		log.debug("findListReviewByEateryID  call.....");
		Query createQuery = entityManager.createQuery("select r.id, r.comment, r.rating,c.username,c.firstName,c.lastName,b.dateTime from Review r left join r.booking b inner join b.customer c left join b.eatery e where e.id="+eateryID+  (withComment?" And length(r.comment)>0":"")+" order by b.dateTime DESC");
		createQuery.setFirstResult(firstPage);
		createQuery.setMaxResults(10);
		@SuppressWarnings("unchecked")
		List<Object[]> resultList = createQuery.getResultList();
		List<ReviewInfo> listReviewInfos= new ArrayList<ReviewInfo>();
		resultList.forEach(o->{
			listReviewInfos.add(getReviewInfoByObject(o));
		});
		return Optional.ofNullable(listReviewInfos);
	}
	@Override
	public Optional<Integer> findNbReviewsWithCommentByEateryID(Long eateryID){
		Query createQuery = entityManager.createQuery("select count(r) from Review r left join r.booking b inner join b.customer c left join b.eatery e where e.id="+eateryID+  " And length(r.comment)>0" );
		return Optional.ofNullable( Integer.valueOf(createQuery.getResultList().get(0)+""));
	}
	
	private ReviewInfo getReviewInfoByObject(Object[] o) {
		ReviewInfo reviewInfo=new ReviewInfo();
		reviewInfo.setId((long)o[0]);
		reviewInfo.setComment((String)o[1]);
		reviewInfo.setRating((long)o[2]);
		String username=(String)o[3];
		reviewInfo.setNbNotes(getNbReviewsByCumstomerID(username));
		reviewInfo.setFirstName((String)o[4]);
		reviewInfo.setLastName((String)o[5]);
		reviewInfo.setDate_time((LocalDateTime)o[6]);
		return reviewInfo;
	}
	
	
	private Integer getNbReviewsByCumstomerID(String userName) {
		Query createQuery = entityManager.createQuery("select count(r) from Review r left join r.booking b inner join b.customer c where c.username=:username" );
		createQuery.setParameter("username", userName);
		return Integer.valueOf(createQuery.getResultList().get(0)+"");
	}
}
