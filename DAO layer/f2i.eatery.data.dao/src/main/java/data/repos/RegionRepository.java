package data.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import data.entities.Region;

public interface RegionRepository extends JpaRepository<Region, Long> {
	
}
