package data.repos;

import java.util.List;
import java.util.Optional;

import data.entities.CookingStyle;
import data.po.EateryInfo;
import data.po.EateryRequest;

public interface CustomizedEateryRepository {
	Optional<List<EateryInfo>> findListEateryByMultiConditions(String restrictionWhere,String restrictionHaving,Integer firstPage, Integer maxSize,  String restrictionOrderBy );
	Optional<Integer> findNbEateryByMultiConditions();
	Optional<List<String[]>> findNbCookingStyleByMultiConditions();
//	Optional<Long> findOneSmallPhotoByEateryID();
	Optional<List<EateryInfo>> findListEateryByMultiConditions(EateryRequest eateryRequest);
	Optional<Integer> findNbEateryByMultiConditions(EateryRequest eateryRequest);
	Optional<EateryInfo> getOneEateryInfoByID(Long eateryID);
	Optional<List<CookingStyle>> findListCookingStyleByMultiConditions(Long cityID);
}
