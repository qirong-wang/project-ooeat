package data.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import data.entities.Review;

public interface ReviewRepository extends JpaRepository<Review, Long>,CustomizedReviewRepository {

}
