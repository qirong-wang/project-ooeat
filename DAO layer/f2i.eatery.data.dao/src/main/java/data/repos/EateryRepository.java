package data.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import data.entities.Eatery;

public interface EateryRepository extends JpaRepository<Eatery, Long>,CustomizedEateryRepository {
	@Query("select count(b.eatery), e.id, e.name From Booking b right join b.eatery e  group by e.id,b.eatery order by count(b.eatery)")
	List<Object[]> findListEatery();
}
