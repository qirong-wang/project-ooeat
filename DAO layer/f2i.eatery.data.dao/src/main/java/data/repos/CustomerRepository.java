package data.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import data.entities.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	Customer findByUsername(String username);
	Customer findByPassword(String password);
	@Query("select u from User u where u.username = :username And u.password = :password")
	Customer findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
	


//	@Modifying(clearAutomatically = true)

//	@Query("UPDATE Customer u SET u.title=:title AND u.phone =:phone WHERE u.username = :username AND u.password = :password")
//	void updateCustomer(@Param("username") String username, @Param("password") String password,
//			 @Param("title") String title,
//			@Param("phone") String phone);

	@Modifying(clearAutomatically = true)
//	@Modifying
	@Query("UPDATE User u SET u.password = :passt  WHERE u.username = :username AND u.password = :password")
	void updatePassword(@Param("username") String username, @Param("password") String password,
			@Param("passt") String pass);

	@Modifying(clearAutomatically = true)
	@Query("UPDATE Customer c SET c.username='Tarik'  WHERE c.username=:username")
	void unsubscribe(@Param("username") String username);

//	@Modifying(clearAutomatically = true)
//	@Query("DELETE Customer WHERE c.username=:username")
//	void unsub(@Param("username") String username);
}
