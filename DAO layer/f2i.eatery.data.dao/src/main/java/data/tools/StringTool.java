package data.tools;

import java.util.List;

public class StringTool {
	public static String listConversion(List<String> list) {
		String s="";
		for(String i:list) {
			s+=i+",";
		}
		return " (" +s.substring(0, s.length()-1)+") ";
	}
}
