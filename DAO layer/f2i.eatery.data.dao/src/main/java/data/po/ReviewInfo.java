package data.po;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ReviewInfo {
	private Long id;
	private String comment;
	private Integer nbNotes;
	private Long rating;
	private String firstName;
	private String lastName;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
	private LocalDateTime date_time;
	public ReviewInfo(Long id, String comment, Integer nbNotes, Long rating, String firstName, String lastName,
			LocalDateTime date_time) {
		super();
		this.id = id;
		this.comment = comment;
		this.nbNotes = nbNotes;
		this.rating = rating;
		this.firstName = firstName;
		this.lastName = lastName;
		this.date_time = date_time;
	}
	public ReviewInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Integer getNbNotes() {
		return nbNotes;
	}
	public void setNbNotes(Integer nbNotes) {
		this.nbNotes = nbNotes;
	}
	public Long getRating() {
		return rating;
	}
	public void setRating(Long rating) {
		this.rating = rating;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public LocalDateTime getDate_time() {
		return date_time;
	}
	public void setDate_time(LocalDateTime date_time) {
		this.date_time = date_time;
	}
	@Override
	public String toString() {
		return "ReviewInfo [id=" + id + ", comment=" + comment + ", nbNotes=" + nbNotes + ", rating=" + rating
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", date_time=" + date_time + "]";
	}
	
}
