package data.po;

import java.util.List;

public class EateryRequest {
	private Long cityID;
	private String eateryNameLike;
	private List<String> cookingStyleIDs;
	private List<String> tagsIDs;
	private String order;
	private String minNote;
	private String minPrice;
	private String maxPrice;
	private Integer firstPage;
	private Integer maxSize;
	
	


	public EateryRequest(Long cityID, String eateryNameLike, List<String> cookingStyleIDs, List<String> tagsIDs,
			String order, String minNote, String minPrice, String maxPrice, Integer firstPage, Integer maxSize) {
		super();
		this.cityID = cityID;
		this.eateryNameLike = eateryNameLike;
		this.cookingStyleIDs = cookingStyleIDs;
		this.tagsIDs = tagsIDs;
		this.order = order;
		this.minNote = minNote;
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
		this.firstPage = firstPage;
		this.maxSize = maxSize;
	}

	public EateryRequest(Long cityID, List<String> cookingStyleIDs, List<String> tagsIDs, String order, String minNote,
			String minPrice, String maxPrice, Integer firstPage, Integer maxSize) {
		super();
		this.cityID = cityID;
		this.cookingStyleIDs = cookingStyleIDs;
		this.tagsIDs = tagsIDs;
		this.order = order;
		this.minNote = minNote;
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
		this.firstPage = firstPage;
		this.maxSize = maxSize;
	}

	public EateryRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getCityID() {
		return cityID;
	}

	public void setCityID(Long cityID) {
		this.cityID = cityID;
	}



	public String getEateryNameLike() {
		return eateryNameLike;
	}

	public void setEateryNameLike(String eateryNameLike) {
		this.eateryNameLike = eateryNameLike;
	}

	public String getMinNote() {
		return minNote;
	}

	public void setMinNote(String minNote) {
		this.minNote = minNote;
	}

	public String getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(String maxPrice) {
		this.maxPrice = maxPrice;
	}

	public String getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}

	public Integer getFirstPage() {
		return firstPage;
	}

	public void setFirstPage(Integer firstPage) {
		this.firstPage = firstPage;
	}

	public Integer getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(Integer maxSize) {
		this.maxSize = maxSize;
	}







	public List<String> getCookingStyleIDs() {
		return cookingStyleIDs;
	}

	public void setCookingStyleIDs(List<String> cookingStyleIDs) {
		this.cookingStyleIDs = cookingStyleIDs;
	}

	public List<String> getTagsIDs() {
		return tagsIDs;
	}

	public void setTagsIDs(List<String> tagsIDs) {
		this.tagsIDs = tagsIDs;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}


	@Override
	public String toString() {
		return "EateryRequest [cityID=" + cityID + ", cookingStyleIDs=" + cookingStyleIDs + ", tagsIDs=" + tagsIDs
				+ ", order=" + order + ", minNote=" + minNote + ", minPrice=" + minPrice + ", maxPrice=" + maxPrice
				+ ", firstPage=" + firstPage + ", maxSize=" + maxSize + "]";
	}


	
	
}
