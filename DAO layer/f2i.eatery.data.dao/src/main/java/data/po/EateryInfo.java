package data.po;

import java.util.Map;
public class EateryInfo {
	private Long id;
	private String name;
	private String street;
	private String postCode;
	private String city;
	private Long photoID;
	private String hoursOp1;
	private String hoursOp2;
	private String price;
	private Long cookingStyleID;
	private String cookingStyle;
	private Long reviews;
	private Double rating;
	private Map<String,String> tags;

public EateryInfo(Long id, String name, String street, String postCode, String city, String hoursOp1,
			String hoursOp2, String price, Long cookingStyleID, String cookingStyle, Long reviews, Double rating) {
		super();
		this.id = id;
		this.name = name;
		this.street = street;
		this.postCode = postCode;
		this.city = city;
		this.hoursOp1 = hoursOp1;
		this.hoursOp2 = hoursOp2;
		this.price = price;
		this.cookingStyleID = cookingStyleID;
		this.cookingStyle = cookingStyle;
		this.reviews = reviews;
		this.rating = rating;
	}
public EateryInfo(Long id, String name, String street, String postCode, String city, Long photoID, String hoursOp1,
		String hoursOp2, String price, Long cookingStyleID, String cookingStyle, Long reviews, Double rating,
		Map<String, String> tags) {
	super();
	this.id = id;
	this.name = name;
	this.street = street;
	this.postCode = postCode;
	this.city = city;
	this.photoID = photoID;
	this.hoursOp1 = hoursOp1;
	this.hoursOp2 = hoursOp2;
	this.price = price;
	this.cookingStyleID = cookingStyleID;
	this.cookingStyle = cookingStyle;
	this.reviews = reviews;
	this.rating = rating;
	this.tags = tags;
}
public EateryInfo() {
	super();
}
public Long getCookingStyleID() {
	return cookingStyleID;
}
public void setCookingStyleID(Long cookingStyleID) {
	this.cookingStyleID = cookingStyleID;
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getStreet() {
	return street;
}
public void setStreet(String street) {
	this.street = street;
}
public String getPostCode() {
	return postCode;
}
public void setPostCode(String postCode) {
	this.postCode = postCode;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}

public Long getPhotoID() {
	return photoID;
}
public void setPhotoID(Long photoID) {
	this.photoID = photoID;
}
public String getHoursOp1() {
	return hoursOp1;
}
public void setHoursOp1(String hoursOp1) {
	this.hoursOp1 = hoursOp1;
}
public String getHoursOp2() {
	return hoursOp2;
}
public void setHoursOp2(String hoursOp2) {
	this.hoursOp2 = hoursOp2;
}
public String getPrice() {
	return price;
}
public void setPrice(String price) {
	this.price = price;
}
public String getCookingStyle() {
	return cookingStyle;
}
public void setCookingStyle(String cookingStyle) {
	this.cookingStyle = cookingStyle;
}
public Long getReviews() {
	return reviews;
}
public void setReviews(Long reviews) {
	this.reviews = reviews;
}
public Double getRating() {
	return rating;
}
public void setRating(Double rating) {
	this.rating = rating;
}
public Map<String, String> getTags() {
	return tags;
}
public void setTags(Map<String, String> tags) {
	this.tags = tags;
}
@Override
public String toString() {
	return "EateryDTO [id=" + id + ", name=" + name + ", street=" + street + ", postCode=" + postCode + ", city=" + city
			+ ", photoID=" + photoID + ", hoursOp1=" + hoursOp1 + ", hoursOp2=" + hoursOp2 + ", price=" + price
			+ ", cookingStyle=" + cookingStyle + ", reviews=" + reviews + ", rating=" + rating + ", tags=" + tags + "]";
}
	
}
