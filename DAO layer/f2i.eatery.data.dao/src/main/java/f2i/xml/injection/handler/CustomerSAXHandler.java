package f2i.xml.injection.handler;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import data.entities.Customer;


public class CustomerSAXHandler extends MySAXHandler<Customer> {
	@Override
	// Triggered when the start of tag is found.
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
//	logger.log(Level.INFO,"endElement for qName:"+qName+" LocalName:"+localName);
		switch (qName) {
		// Create a new Employee object when the start tag is found
		case "Customer":
			entity=new Customer();
			break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		switch (qName) {
		// Add the employee to list once end tag is found
		case "Customer":
			listEnitities.add(entity);
			break;
		case "password":
			entity.setPasswordEncry(content);
			break;
		case "last_name":
			entity.setLastName(content);
			break;
		case "first_name":
			entity.setFirstName(content);
			break;
		case "userName":
			entity.setUsername(content);
			break;
		case "phone":
			entity.setPhone(content);
			break;
		case "title":
			entity.setTitle(content);
			break;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		content = String.copyValueOf(ch, start, length).trim();
	}
	
}
