package f2i.xml.injection.parser;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import data.entities.Customer;


public class SAXParserXml {

	private static Logger logger = Logger.getLogger("SAXParserXml.class") ; // src\main\java\f2i\xml\injection\xml\customer.xsd
	
	public static List<Customer> parse(String xmlFilePath, String xsdFilePath){
		validateFile(xmlFilePath, xsdFilePath);
		SAXParserXml p = new SAXParserXml();		
		return p.initParser(xmlFilePath);
	}
	
	public static Boolean validateFile(String xmlFilePath, String xsdFilePath) {
		Boolean status = true;
		try {
			Schema schema =  SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new File(xsdFilePath));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new File(xmlFilePath)));
			
		} catch (SAXException | IOException e) {
			logger.severe("ERROR SAXException While Validating XML Against XSD File"+e.getLocalizedMessage());
			status = false;
		} 
		
		logger.info("File is Valide ...");
		return status;
	}

	private List<Customer> initParser(String xmlFilePath) {
		logger.log(Level.INFO, "Start XML Parser ...");
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		SAXHandler handler = null;
		try {
			SAXParser parser = parserFactory.newSAXParser();
			handler = new SAXHandler();
			parser.parse(new FileInputStream(xmlFilePath), handler);
			
		} catch (ParserConfigurationException e) {
			logger.log(Level.SEVERE, "Error XML Configuration : "+e.getLocalizedMessage());
		} catch (SAXException e) {
			logger.log(Level.SEVERE, "Error XML SAXException : "+e.getLocalizedMessage());
			logger.log(Level.SEVERE, " Ligne :"+handler.getDocumentLocator().getLineNumber());
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error XML IOException : "+e.getLocalizedMessage());
		}
		return handler.getCustomers();
	}
	
}
