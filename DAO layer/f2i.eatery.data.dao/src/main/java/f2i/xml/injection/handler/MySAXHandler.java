package f2i.xml.injection.handler;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Locator;
import org.xml.sax.helpers.DefaultHandler;

/**
 * The Handler for SAX Events.
 */
public class MySAXHandler<T> extends DefaultHandler {

	protected Locator locator;

	protected List<T> listEnitities;
	protected T entity;
	protected String content;

	public MySAXHandler() {
		listEnitities = new ArrayList<>();
	}

	/**
	 * @return the listEnitities
	 */
	public List<T> getListEnitities() {
		return listEnitities;
	}

	/**
	 * @param listEnitities the listEnitities to set
	 */
	public void setListEnitities(List<T> listEnitities) {
		this.listEnitities = listEnitities;
	}

	public void setDocumentLocator(Locator locator) {
		this.locator = locator;
	}

	public Locator getDocumentLocator() {
		return this.locator;
	}

	
}
