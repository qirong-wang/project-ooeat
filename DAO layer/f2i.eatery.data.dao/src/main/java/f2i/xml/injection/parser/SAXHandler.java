package f2i.xml.injection.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import data.entities.Customer;




/**
 * The Handler for SAX Events.
 */
public class SAXHandler extends DefaultHandler {

	private Locator locator;

	private List<Customer> customers;
	private Customer customer;
	private String content ;

	public SAXHandler() {
		customers = new ArrayList<>();
	}

	
	public List<Customer> getCustomers() {
		return customers;
	}



	public void setDocumentLocator(Locator locator) {
		this.locator = locator;
	}

	public Locator getDocumentLocator() {
		return this.locator;
	}

	@Override
	// Triggered when the start of tag is found.
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
//	logger.log(Level.INFO,"endElement for qName:"+qName+" LocalName:"+localName);

		switch (qName) {
		// Create a new Employee object when the start tag is found
		case "customer":
			customer = new Customer();
			break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		switch (qName) {
		// Add the employee to list once end tag is found
		case "customer":
			customers.add(customer);
			break;
		case "password":
			customer.setPasswordEncry(content);
			break;
		case "lastname":
			customer.setLastName(content);
			break;
		case "firstname":
			customer.setFirstName(content);
			break;
		case "username":
			customer.setUsername(content);
			break;
		case "phone":
			customer.setPhone(content);
			break;
		case "title":
			customer.setTitle(content);
			break;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		content = String.copyValueOf(ch, start, length).trim();
	}
}
