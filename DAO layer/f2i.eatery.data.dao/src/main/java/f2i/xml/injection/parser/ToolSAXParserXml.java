package f2i.xml.injection.parser;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import f2i.xml.injection.handler.MySAXHandler;

public class ToolSAXParserXml<T,M extends MySAXHandler<T>> {

	private static Logger logger = Logger.getLogger("ToolSAXParserXml.class") ; // src\main\java\f2i\xml\injection\xml\customer.xsd
	
//	public List<T> parse(StreamSource xml, StreamSource xsd,Class<M> handlerClass){
		public List<T> parse(File xml, File xsd,Class<M> handlerClass){
//	public List<T> parse(String xmlFilePath, String xsdFilePath,Class<M> handlerClass){
		validateFile(xml, xsd);
//		ToolSAXParserXml p = new ToolSAXParserXml();		
		return initParser(xml,handlerClass);
//		return initParser(xmlFilePath,handlerClass);
	}
	
//		private  Boolean validateFile(StreamSource xml, StreamSource xsd) {
	private  Boolean validateFile(File xml, File xsd) {
//	private  Boolean validateFile(String xmlFilePath, String xsdFilePath) {
		Boolean status = true;
		try {
//			Schema schema =  SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new File(xsdFilePath));
			Schema schema =  SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(xsd);
			Validator validator = schema.newValidator();
//			validator.validate(new StreamSource(new File(xmlFilePath)));
			validator.validate(new StreamSource(xml));
//			validator.validate(xml);
			
		} catch (SAXException | IOException e) {
			logger.severe("ERROR SAXException While Validating XML Against XSD File "+e.getLocalizedMessage());
			status = false;
		} 
		
		logger.info("File is Valide ...");
		return status;
	}

//	private List<T> initParser(StreamSource xml, Class<M> handlerClass) {
		@SuppressWarnings("deprecation")
		private List<T> initParser(File xml, Class<M> handlerClass) {
//	private List<T> initParser(String xmlFilePath, Class<M> handlerClass) {
		logger.log(Level.INFO, "Start XML Parser ...");
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		M handler=null;
		try {
			SAXParser parser = parserFactory.newSAXParser();
			handler = handlerClass.newInstance();
//			parser.parse(xml.getInputStream(), handler);
			parser.parse(new FileInputStream(xml), handler);
			
		} catch (ParserConfigurationException e) {
			logger.log(Level.SEVERE, "Error XML Configuration : "+e.getLocalizedMessage());
		} catch (SAXException e) {
			logger.log(Level.SEVERE, "Error XML SAXException : "+e.getLocalizedMessage());
			logger.log(Level.SEVERE, " Ligne :"+handler.getDocumentLocator().getLineNumber());
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error XML IOException : "+e.getLocalizedMessage());
		} catch (InstantiationException e) {
			logger.log(Level.SEVERE, "Error XML InstantiationException : "+e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			logger.log(Level.SEVERE, "Error XML IllegalAccessException : "+e.getLocalizedMessage());
		}
		return (List<T>) handler.getListEnitities();
	}

	
}
